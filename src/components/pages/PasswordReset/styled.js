import styled from "styled-components";

export const MdpWrapper = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  min-width: 30%;
  justify-content: center;
  align-items: center;
  margin: auto;
`;
