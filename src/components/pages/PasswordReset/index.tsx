import React, { Dispatch, useState } from "react";
import ProfilContent from "../../organisms/ProfilContent";
import Input from "../../atoms/Input";
import Button from "../../atoms/Button";
import { theme } from "../../_settings/theme";
import Title from "../../atoms/Title";
import { MdpWrapper } from "./styled";
import { connect } from "react-redux";
import { ResetPasswordAction } from "../../../store/resetPassword/types";
import { resetPassword } from "../../../store/resetPassword/actions";

import { ReactComponent as LockIcon } from "../.././../assets/icons/lock.svg";
import { toast } from "react-toastify";

const PasswordReset = (props: any) => {
  const [credentials, setCredentials] = useState({
    password: "",
    confirm: ""
  });
  const handleChange = (data: any) => {
    return setCredentials({ ...credentials, ...data });
  };
  const onSubmit = () => {
    if (credentials.password !== credentials.confirm) {
      toast.error("Les deux mots de passe ne sont pas identiques");
      return;
    }
    props.resetPasswordFunction(props.jwt, credentials.password);
  };
  return (
    <MdpWrapper>
      <Input
        name="password"
        type="password"
        placeholder="nouveau mot de passe"
        {...theme.light}
        value={credentials.password}
        IconComponent={LockIcon}
        onChange={handleChange}
      ></Input>
      <Input
        name="confirm"
        type="password"
        placeholder="nouveau mot de passe"
        {...theme.light}
        value={credentials.confirm}
        IconComponent={LockIcon}
        onChange={handleChange}
      ></Input>
      <Button size="big" {...theme.dark} onClick={onSubmit}>
        changer mot de passe
      </Button>
    </MdpWrapper>
  );
};

interface IPropsDispatch {
  resetPasswordFunction: (jwt: string, password: string) => void;
}

const mapDispatchToProps = (
  dispatch: Dispatch<ResetPasswordAction>
): IPropsDispatch => ({
  resetPasswordFunction: (jwt: string, password: string) =>
    dispatch(resetPassword(jwt, password))
});

export default connect(
  null,
  mapDispatchToProps
)(PasswordReset);
