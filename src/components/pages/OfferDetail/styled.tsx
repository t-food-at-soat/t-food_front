import styled from "styled-components";

export const OfferDetailContainer = styled.section`
  display: flex;
  justify-content: center;
  align-items: center;  
  flex: 1;
`;