import React,{useState} from 'react';
import {OfferDetailContainer} from './styled';
import OfferDetailCard from '../../organisms/OfferDetailCard';
import {IRootState} from "../../../store/combiners";
import {connect} from "react-redux";
import Offer from "../../../ts/interfaces/offer.interfaces";

export interface IOfferDetailProps {
    id: number
}

const OfferDetail = ({id, offerList}: IOfferDetailProps & IPropsState) => {

    const [currentOffer, setCurrentOffer] =  useState(offerList.find(x => x.id === Number(id)));
    const [currentIndex, setCurrentIndex] = useState(currentOffer ? offerList.indexOf(currentOffer) : 0);

    console.log(currentOffer ? offerList.indexOf(currentOffer) : 0);
    const handleCtrl = (index : number) => {
        setCurrentOffer(offerList[index]);
        setCurrentIndex(index);
    };

    return (
        <OfferDetailContainer>
            {currentOffer
                ? <OfferDetailCard
                    offer={currentOffer}
                    currentIndex={currentIndex}
                    maxIndex={offerList.length - 1}
                    handleCtrl={handleCtrl}
                />
                : "NOTHING"}
        </OfferDetailContainer>
    );
};

interface IPropsState {
    offerList: Array<Offer>;
}

const mapStateToProps = ({offer}: IRootState, ownProps: IOfferDetailProps): IPropsState => ({
    offerList: offer.offerList
});

export default connect(
    mapStateToProps
)(OfferDetail);

