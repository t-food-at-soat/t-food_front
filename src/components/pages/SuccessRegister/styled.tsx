import styled from "styled-components";

export const SuccessCardWrapper = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  
    @media (max-width: 768px) {
     text-align: center;
     flex-direction: column-reverse;
   }
`;