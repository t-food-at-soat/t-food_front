import React from 'react';
import SuccessCard from "../../organisms/SuccessCard";
import {SuccessCardWrapper} from './styled'

const SuccessRegister = () => {
    return (
        <SuccessCardWrapper>
          <SuccessCard message={"Votre inscription à bien été prise en compte !"}/>
        </SuccessCardWrapper>
    );
};

export default SuccessRegister;
