import React, { useState } from "react";
import { RegisterPageStyled } from "./styled";
import UserType from "../../molecules/Forms/typeUser/index";
import Mascot from "../../atoms/Mascot";
import RegisterForm from "../../organisms/RegisterForm/index";
import Roles from "../../../ts/types/roles.types";

interface IRegisterState {
  roles: Roles;
}

const RegisterUser = () => {
  const [state, setState] = useState<IRegisterState>({
    roles: Roles.PUBLIC
  });

  const handleClick = (roles: Roles) => {
    setState({ ...state, roles: roles });
  };

  const onBack = () => {
    setState({ ...state, roles: Roles.PUBLIC });
  };

  return (
    <RegisterPageStyled>
      <Mascot type={state.roles} />
      {state.roles === Roles.PUBLIC && <UserType onClick={handleClick} />}
      {state.roles !== Roles.PUBLIC && (
        <RegisterForm
          roles={state.roles}
          onSubmit={data => console.log(data)}
          onBack={onBack}
        />
      )}
    </RegisterPageStyled>
  );
};

export default RegisterUser;
