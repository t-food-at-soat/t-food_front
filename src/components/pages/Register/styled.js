import styled from "styled-components";

const RegisterPageStyled = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  user-select: none;
  min-width: 350px;
  background: url("/static/media/bgImage.b54a47c5.jpg");
  flex: 1;

 
  
  svg {  
    padding: 1rem;
  }
  
  @media (max-width: 768px) {
    svg {
      display: none;
    }
  }
`;

export { RegisterPageStyled };
