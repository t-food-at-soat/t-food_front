import React from "react";
import { connect } from "react-redux";
import { IRootState } from "../../../store/combiners";
import Organism401 from "../../organisms/organism401";
import { UpdateOfferWrapper } from "./styled";
import Roles from "../../../ts/types/roles.types";
import OfferForm from "../../organisms/OfferForm";
import Offer from "../../../ts/interfaces/offer.interfaces";

interface IUpdateOffer {
  offerId: number;
}

const whatPageDoIDisplay = (isConnected: boolean, offer: Offer) => {
  if (!isConnected) return <Organism401 />;
  return <OfferForm offerToUpdate={offer} />;
};
const UpdateOffer = ({
  isConnected,
  offerId,
  entrepriseOffers
}: IPropsState & IUpdateOffer) => {
  const offerToUpdate: Offer = entrepriseOffers.find(
    item => item.id == offerId
  ) as Offer;
  return (
    <UpdateOfferWrapper>
      {whatPageDoIDisplay(isConnected, offerToUpdate)}
    </UpdateOfferWrapper>
  );
};

interface IPropsState {
  entrepriseOffers: Array<Offer>;
  isConnected: boolean;
  roles: Roles;
}

const mapStateToProps = ({
  authentication,
  offer
}: IRootState): IPropsState => ({
  entrepriseOffers: offer.offerList,
  isConnected: authentication.isConnected,
  roles: authentication.userConnected
    ? authentication.userConnected.roles
    : Roles.INDIVIDUAL
});

export default connect(mapStateToProps)(UpdateOffer);
