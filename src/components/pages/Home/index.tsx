import React from 'react';
import HeroHeader from "../../organisms/HeroHeader";
import HomeContent from "../../organisms/HomeContent";

const Home = () => {
    return (
        <>
            <HeroHeader/>
            <HomeContent/>
        </>
    );
};

export default Home;
