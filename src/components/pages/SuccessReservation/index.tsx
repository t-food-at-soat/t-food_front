import React from 'react';
import SuccessCard from "../../organisms/SuccessCard";
import {SuccessCardWrapper} from './styled'

const SuccessReservation= () => {
    return (
        <SuccessCardWrapper>
          <SuccessCard message={"Votre réservation à bien été prise en compte !"}/>
        </SuccessCardWrapper>
    );
};

export default SuccessReservation;
