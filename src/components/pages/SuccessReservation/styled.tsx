import styled from "styled-components";

export const SuccessCardWrapper = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;  
  text-align: center; 
`;