import React from "react";
import {connect} from "react-redux";
import {IRootState} from "../../../store/combiners";
import Organism401 from "../../organisms/organism401";
import {OfferWrapper} from './styled'
import Roles from "../../../ts/types/roles.types";
import EnterpriseHome from "../../organisms/enterpriseHome";
import UserHome from "../../organisms/userHome";

const whatPageDoIDisplay = (isConnected: boolean, roles: Roles) => {
    if (!isConnected) return <Organism401/>;

    if (roles === Roles.CORPORATION) return <EnterpriseHome/>;
    return <UserHome/>;
};
const Offer = ({isConnected, roles}: IPropsState) => {
    return <OfferWrapper>
        {whatPageDoIDisplay(isConnected, roles)}
    </OfferWrapper>;
};

interface IPropsState {
    isConnected: boolean;
    roles: Roles;
}

const mapStateToProps = ({authentication}: IRootState): IPropsState => ({
    isConnected: authentication.isConnected,
    roles: authentication.userConnected
        ? authentication.userConnected.roles
        : Roles.INDIVIDUAL
});

export default connect(
    mapStateToProps
)(Offer);
