import styled from "styled-components";

export const OfferWrapper = styled.section`
    flex: 1;
    display: flex;
    justify-content: center;
`;