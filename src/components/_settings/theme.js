import { createGlobalStyle } from "styled-components";
import milkshake from "./fonts/Milkshake.ttf";
import thisFont from "./fonts/This-Regular.otf";
import hiragino from "./fonts/hiragino-mincho-pro-w3.otf";

const getPadding = size => {
  switch (size) {
    case "small":
      return ".25rem 0";
    case "big":
      return "1rem 0rem";
    default:
      return ".5rem 0rem";
  }
};

const colors = {
  green: "#98B92E",
  white: "#FFF",
  blue: "#52A3DD",
  lightgrey: "#E3E4E4",
  darkgrey: "#4F4F4F",
  black: "#000",
  darkgreen: "#688900",
  mediumgrey: "#606060",
  darkorange: "#E54513",
  yellow: "#F1B54E"
};

export const theme = {
  colors,
  light: {
    backgroundColor: colors.white,
    color: colors.darkgrey,
    boxShadow: `.1rem .1rem ${colors.green}`,
    border: "none",
    hoverBackgroundColor: colors.lightgrey,
    hoverBoxShadow: ".1rem .1rem " + colors.darkgreen
  },
  dark: {
    backgroundColor: colors.darkgrey,
    color: colors.white,
    boxShadow: ".1rem .1rem " + colors.white,
    border: ".05rem solid " + colors.white,
    hoverBackgroundColor: colors.mediumgrey,
    hoverBoxShadow: ".1rem .1rem " + colors.lightgrey
  },
  roundBox: props => `
    width:100%;
    padding: ${getPadding(props.size)};
    border-radius: .6rem;
    border: ${props.border};
    font-family: Milkshake,serif;
    background-color: ${props.backgroundColor};
    
    font-size: 1rem;
    color: ${props.color};
    cursor: ${props.isNotClickable ? "unset" : "pointer"};
    outline: none;
    display:flex;
  `
};

export const GlobalStyle = createGlobalStyle`
  @font-face {
    font-family: milkshake;
    src: url(${milkshake}) format('truetype');
    font-weight: normal;
    font-style: normal;
  }
  
  @font-face {
    font-family: thisFont;
    src: url(${thisFont}) format('truetype');
    font-weight: normal;
    font-style: normal;
  }
  
  @font-face {
    font-family: hiragino;
    src: url(${hiragino}) format('truetype');
    font-weight: normal;
    font-style: normal;
  }
  
  html, body {
    margin: 0;
    overflow-x: hidden;
    padding: 0;
    font-size:62.5%;
  }
  
  * {
    box-sizing: border-box;
  }
  
  #root{
    display: flex;
    flex-direction: column;
    min-height: 100vh;
  }
  
  .Toastify {
    font-size: 2rem;
    font-family: 'Hiragino',serif;
  }
  
  a{
    text-decoration: none;
  }
`;
