import React from "react";
import { ThemeProvider } from "styled-components";
import { GlobalStyle, theme } from "../../_settings/theme";
import NavigateFormButtons from "./index";

export default {
  title: "Molecules|NavigateFormButtons",
  includeStories: ["standard"]
};

export const standard = () => (
  <ThemeProvider theme={theme}>
    <div>
      <GlobalStyle />
      <div style={{ width: "32.4rem" }}>
        <NavigateFormButtons
          left={"précedent"}
          right={"suivant"}
          onClickL={() => console.log("Gauche")}
          onClickR={() => console.log("Droite")}
        ></NavigateFormButtons>
      </div>
    </div>
  </ThemeProvider>
);
