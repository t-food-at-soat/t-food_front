import styled from 'styled-components';

const StyledNavigateFormButtons = styled.div`
  width:100%;
  display:flex;
  justify-content:space-between;
`;
const ButtonWrapper = styled.div`
 
  width:40%;
`;

export {StyledNavigateFormButtons, ButtonWrapper};

