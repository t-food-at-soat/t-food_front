import React, {ReactElement} from "react";
import { StyledNavigateFormButtons, ButtonWrapper } from "./styled";
import Button from "../../atoms/Button";
import { theme } from "../../_settings/theme";

interface INavigateFormButtonsProps {
  onClickL: () => void;
  onClickR: () => void;
  left: ReactElement;
  right: ReactElement;
}

const NavigateFormButtons = ({
  onClickL,
  onClickR,
  left,
  right,
  ...props
}: INavigateFormButtonsProps) => {
  return (
    <StyledNavigateFormButtons>
      <ButtonWrapper>
        <Button {...theme.light} size="big" onClick={() => onClickL()}>
          {left}
        </Button>
      </ButtonWrapper>
      <ButtonWrapper>
        <Button {...theme.light} size="big" onClick={() => onClickR()}>
          {right}
        </Button>
      </ButtonWrapper>
    </StyledNavigateFormButtons>
  );
};

export default NavigateFormButtons;
