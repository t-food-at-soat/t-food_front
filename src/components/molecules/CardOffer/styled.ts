import styled from 'styled-components';

interface IStyledCardOfferMapProps {

}

const ImageBoxWrapper = styled.div`
  overflow: hidden;
  width: 100%;
`;

const StyledCardOffer = styled.div<IStyledCardOfferMapProps>`
  perspective: 1500px;
  position: relative;
  transition: all .3s ease-out;
  div:nth-of-type(1) {    
      transition: all 0.3s;
  } 
    
  &:hover {
    cursor: pointer;
    transform: translate(0, -0.5rem);
    
    .imageBox {  
      transform: translateX(30%);
    }
    .hoverCardMap{
        transform: rotateY(0deg);
        opacity: 1;
    }
     
    
  }
  &:active {
    transform: translate(0, 0.5rem);
  }
`;

const HoverCardMap = styled.div`
  display: none;  
  //display: flex;
  background: ${props => props.theme.colors.green};
  box-shadow: 3px 0 14px 0 rgba(0,0,0,0.75);
  position: absolute;
  top: 0;
  transform: rotateY(-90deg);
  width: 80%;
  height: 17rem;
  transition: transform 0.6s ,opacity 0.3s ease;
  transform-origin: 0 0;
  opacity: 0;
  justify-content: center;
  align-items: center;
  
  img {
    width: 100%;
    height: 100%;
    object-fit: cover;
  }
`;


const BadgeContainer = styled.section`
  display:flex;
  justify-content:space-between;
  margin-top: .8rem;
`;
export {StyledCardOffer, BadgeContainer, HoverCardMap, ImageBoxWrapper};

