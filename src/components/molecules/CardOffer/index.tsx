import React, { useState } from "react";
import {
  StyledCardOffer,
  BadgeContainer,
  HoverCardMap,
  ImageBoxWrapper
} from "./styled";
import Badge from "../../atoms/Badge";
import ImageBox from "../../atoms/ImageBox";
import CartTitle from "../../atoms/CardTitle";
import OfferType from "../../../ts/interfaces/offer.interfaces";
import Map from "../../atoms/Map";
import moment from "moment-timezone";
import ReservationStatus from "../../../ts/types/reservationStatus.types";

interface ICardOfferProps {
  offer: OfferType & any;
  onClick: (offer: OfferType) => void;
}

const CardOffer = ({ offer, onClick }: ICardOfferProps) => {
  const handleClick = (
    _event: React.MouseEvent<HTMLDivElement, MouseEvent>
  ) => {
    onClick(offer);
  };
  const [travelDuration, setTravelDuration] = useState("Calcul ...");

  const handleDirectionDuration = (duration: any) => {
    offer.duration = duration.value;
    setTravelDuration(`${duration.text ? duration.value > 3600 ? "+ 1 heure" : duration.text : "Bon voyage !"}`);
  };

  return (
    <StyledCardOffer onClick={event => handleClick(event)}>
      <ImageBoxWrapper>
        <ImageBox
          shadowOff={true}
          sourceImage={offer.picture}
          title={offer.description}
          className={"imageBox"}
        />
      </ImageBoxWrapper>
      <HoverCardMap className="hoverCardMap">
        <Map
          handleDirectionDuration={handleDirectionDuration}
          destination={offer.location}
        />
      </HoverCardMap>
      <CartTitle
        isPassed={moment(offer.datetime).isBefore(Date.now())}
        isCanceled={offer.ReservationStatus === ReservationStatus.Canceled}
      >
        <span>{offer.description}</span>
      </CartTitle>

      <BadgeContainer>
        <Badge>
          <span>{offer.type}</span>
        </Badge>
        <Badge>
          <span>{moment(offer.datetime).format("DD MMMM")}</span>
        </Badge>
        <Badge>
          <span>{travelDuration}</span>
        </Badge>
      </BadgeContainer>

      <BadgeContainer>
        <Badge>
          <span>{offer.quantity} personnes</span>
        </Badge>

        <Badge>
          <span>{moment(offer.datetime).format("HH:mm")}</span>
        </Badge>
        <Badge isOutlined imgSrc={offer.author && offer.author.picture} />
      </BadgeContainer>
    </StyledCardOffer>
  );
};

export default CardOffer;
