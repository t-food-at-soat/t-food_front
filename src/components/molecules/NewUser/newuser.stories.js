import React from "react";
import NewUser from "./index";
import { ThemeProvider } from "styled-components";
import { GlobalStyle, theme } from "../../_settings/theme";

export default { title: "Molecules|NewUser" };

export const withText = () => {
  return (
    <ThemeProvider theme={theme}>
      <>
        <GlobalStyle />
        <NewUser />
      </>
    </ThemeProvider>
  );
};
