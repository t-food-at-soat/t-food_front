import React, { useState } from "react";
import { NewUserStyled, ButtonWrapper } from "./style";
import Button from "../../atoms/Button";
import Title from "../../atoms/Title";
import { theme } from "../../_settings/theme";
import { navigate } from "hookrouter";

import { ConnexionWrapper } from "././style";
import ConnexionForm from "../../molecules/Forms/Connexion";
import Logo from "../../atoms/Logo";

const NewUser = () => {
  const [miniConnexionVisiable, setMiniConnexionVisiable] = useState<boolean>(
    false
  );
  return (
    <>
      <NewUserStyled className={"new-user"}>
        <ButtonWrapper>
          <Button
            size="small"
            {...theme.light}
            onClick={() => navigate("/register")}
          >
            <Title font={"Milkshake"} fontSize={"1.4rem"}>
              <p>Je suis nouveau</p>
            </Title>
          </Button>
          <Button
            size="small"
            {...theme.light}
            onClick={() => setMiniConnexionVisiable(true)}
          >
            <Title font={"Milkshake"} fontSize={"1.4rem"}>
              <p>Connexion</p>
            </Title>
          </Button>
        </ButtonWrapper>
      </NewUserStyled>
      {miniConnexionVisiable && (
        <ConnexionWrapper
          onClick={e => {
            setMiniConnexionVisiable(false);
          }}
        >
          <div style={{ marginBottom: "4rem" }}>
            <Logo dark={true} content={"food"}></Logo>
          </div>

          <ConnexionForm></ConnexionForm>
        </ConnexionWrapper>
      )}
    </>
  );
};

export default NewUser;
