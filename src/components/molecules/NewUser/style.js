import styled from "styled-components";
import ImgBG from "../../organisms/HeroHeader/bgImage.jpg";

const NewUserStyled = styled.div`
  display: flex;
  justify-content: space-around;
  align-items: center;

  button:nth-child(2) {
    display: none;
  }

  @media (max-width: 758px) {
    button:nth-child(2) {
      display: block;
    }
  }
`;

const ButtonWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex-basis: 40%;

  button:first-of-type {
    margin-bottom: 0.5rem;
  }
`;

const ConnexionWrapper = styled.div`
  background: url(${ImgBG});
  top: 0;
  left: 0;
  width: 100vw !important;
  height: 100vh;
  margin: 0 !important;
  position: fixed;
  z-index: 1000;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  @media (min-width: 758px) {
    display: none;
  }
`;

export { NewUserStyled, ButtonWrapper, ConnexionWrapper };
