import React from "react";
import Profile from "./index";
import { ThemeProvider } from "styled-components";
import { GlobalStyle, theme } from "../../_settings/theme";

export default { title: "Molecules|Profile" };

export const withText = () => {
  return (
    <ThemeProvider theme={theme}>
      <>
        <GlobalStyle />
        <Profile
          imageUrl={"https://sonet.soat.fr/Trombinoscope/1671"}
          username={"Romain"}
          onClick={() => console.log(`Hello from Romain`)}
        />
      </>
    </ThemeProvider>
  );
};
