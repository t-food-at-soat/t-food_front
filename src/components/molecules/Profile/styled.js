import styled from "styled-components";

const ProfileContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  cursor: pointer;
  user-select: none;
  
  span {
    padding-left: 1.5rem;
  }
  
  svg {
    height: 4.5rem;
    width: 4.5rem;
    padding-left: 1.5rem;
  }
`;

const ProfileWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: row;
  cursor: pointer;
  user-select: none;
`;



export { ProfileContainer, ProfileWrapper };
