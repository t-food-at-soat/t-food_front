import React from "react";
import {ProfileContainer, ProfileWrapper} from "./styled";
import Avatar from "../../atoms/Avatar";
import Title from "../../atoms/Title";
import {ReactComponent as LogoutIcon} from './logout.svg'

interface IProfile {
    imageUrl: string;
    username: string;
    onClick: () => void;
    onLogoutClick: () => void;
}

const Profile = ({imageUrl, username, onClick, onLogoutClick}: IProfile) => {
    return (
        <>
            <ProfileContainer>
                <ProfileWrapper onClick={onClick}>
                    <Avatar avatarUrl={imageUrl} size={"4rem"}/>
                    <Title>
                        <p>{username}</p>
                    </Title>
                </ProfileWrapper>
                <LogoutIcon onClick={onLogoutClick}/>
            </ProfileContainer>
        </>
    );
};

export default Profile;
