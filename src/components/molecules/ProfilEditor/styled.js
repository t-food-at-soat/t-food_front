import styled from 'styled-components';

const ProfilEditorWrapper = styled.div`
  transition: all .3s ease-out;
  flex-basis: 30%;
  margin: 2rem 0;
  
  @media (max-width: 768px) {
    flex-basis: auto;
  }
 
  &:hover {
    
  }
  &:active {
  
  }
`;

const ProfilEditorButtonWrapper = styled.div`
  margin-top: .8rem;
`;

const ProfilEditorInputsWrapper = styled.div`
    margin-top:0.5rem;
    
    div {    
      width: auto !important;
    }
`;


export {ProfilEditorWrapper, ProfilEditorInputsWrapper, ProfilEditorButtonWrapper};

