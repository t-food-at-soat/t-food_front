import React, {useEffect, useState} from "react";
import {
  ProfilEditorWrapper,
  ProfilEditorInputsWrapper,
  ProfilEditorButtonWrapper
} from "./styled";
import Input from "../../atoms/Input";
import Button from "../../atoms/Button";
import Title from "../../atoms/Title";
import {theme} from "../../_settings/theme";
import {ReactComponent as MailIcon} from "../../../assets/icons/email.svg";
import {ReactComponent as PwdIcon} from "../../../assets/icons/lock.svg";
import {ReactComponent as PhoneIcon} from "../../../assets/icons/smartphone.svg";
import {IRootState} from "../../../store/combiners";
import {Dispatch} from "redux";
import {connect} from "react-redux";
import User from "../../../ts/interfaces/user.interfaces";
import {ProfileAction} from "../../../store/profile/types";
import {getProfileById, setProfileIsLoad, updateProfile} from "../../../store/profile/actions";
import {ILoggedInPayload} from "../../../store/authentication/types";
import {updateProfileEpic} from "../../../store/profile/epics/updateProfile";
import Autocomplete from "../../atoms/Autocomplete";
import {toast} from "react-toastify";
import ImageBox from "../../atoms/ImageBox";
import {AddImageStyled, ImageContainerStyled} from "../../organisms/OfferForm/styled";


const ProfilEditor = ({isProfileLoad, currentProfile, setProfileLoad, getProfileById,connectedUser,updateProfile}: IPropsState & IPropsDispatch) => {
  const [profile, setProfile] = useState({...currentProfile, repeatPassword: ""});

  const handleChange = (data : any) => {
    setProfile({
      ...profile,
      ...data
    });
  };

  const handleSubmit = () => {
    if (profile.password !== profile.repeatPassword){
      toast.error("Les mots de passe ne correspondent pas !")
    }else {
      updateProfile(profile)
    }

  };

  useEffect(()=> {
    if (connectedUser){
      getProfileById(Number(connectedUser.id));
    }

    return () => {
      setProfileLoad(false);
    };
  }, [connectedUser]);

  useEffect(()=> {
    setProfile({
      ...currentProfile,
      password : "",
      repeatPassword: ""
    });
    return;
  }, [currentProfile]);



  return (
    <>
      {isProfileLoad
        ? <ProfilEditorWrapper>
          <ImageContainerStyled>
            <ImageBox
              shadowOff={true}
              sourceImage={profile.picture}
              title={"Profile picture"}
            />
            <AddImageStyled>
              <Input
                placeholder={"Photo"}
                name={"picture"}
                value={""}
                type={"file"}
                onChange={handleChange}
              />
              <p>+</p>
            </AddImageStyled>
          </ImageContainerStyled>
          <ProfilEditorInputsWrapper>
            <Input
              {...theme.light}
              IconComponent={MailIcon}
              placeholder={"email"}
              value={profile.email}
              name={"email"}
              type={"email"}
              onChange={handleChange}
            />
          </ProfilEditorInputsWrapper>
          <ProfilEditorInputsWrapper>
            <Input
              {...theme.light}
              type="password"
              IconComponent={PwdIcon}
              value={profile.password}
              placeholder={"Nouveau mot de passe"}
              name={"password"}
              onChange={handleChange}
            />
          </ProfilEditorInputsWrapper>
          <ProfilEditorInputsWrapper>
            <Input
              {...theme.light}
              type="password"
              IconComponent={PwdIcon}
              value={profile.repeatPassword}
              placeholder={"Répéter le mot de passe "}
              name={"repeatPassword"}
              onChange={handleChange}
            />
          </ProfilEditorInputsWrapper>
          <ProfilEditorInputsWrapper>
            <Input
              {...theme.light}
              IconComponent={PhoneIcon}
              placeholder={"Mobile"}
              value={profile.phone}
              name={"phone"}
              type={"number"}
              onChange={handleChange}
            />
          </ProfilEditorInputsWrapper>
          <ProfilEditorInputsWrapper>
            <Autocomplete
              onChange={handleChange}
              isRequired={false}
              placeholder={"Nouvelle adresse"}/>
          </ProfilEditorInputsWrapper>

          <ProfilEditorButtonWrapper>
            <Button size="big" {...theme.light} onClick={handleSubmit}>
              <Title font={"Milkshake"} fontSize={"1.5rem"}>
                <span>Modifier</span>
              </Title>
            </Button>
          </ProfilEditorButtonWrapper>
        </ProfilEditorWrapper>
        : "Chargement"
      }
    </>
  );
};

interface IPropsState {
  currentProfile: User;
  isProfileLoad: boolean;
  connectedUser: ILoggedInPayload | undefined;
}

interface IPropsDispatch {
  getProfileById: (id: number) => void;
  setProfileLoad: (state: boolean) => void;
  updateProfile: (state: User) => void;
}

const mapStateToProps = ({authentication, profile}: IRootState): IPropsState => ({
  currentProfile: profile.currentProfile,
  connectedUser : authentication.userConnected,
  isProfileLoad: profile.isLoad
});

const mapDispatchToProps = (
  dispatch: Dispatch<ProfileAction>
): IPropsDispatch => ({
  getProfileById: (id: number) => dispatch(getProfileById(id)),
  setProfileLoad: (payload: boolean) => dispatch(setProfileIsLoad(payload)),
  updateProfile: (payload: User) => dispatch(updateProfile(payload)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfilEditor);
