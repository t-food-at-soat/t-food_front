import React from "react";
import { ThemeProvider } from "styled-components";
import { GlobalStyle, theme } from "../../_settings/theme";
import ProfilEditor from "./index";

export default {
  title: "Molecules|ProfilEditor",
  includeStories: ["standard"]
};

export const standard = () => (
  <ThemeProvider theme={theme}>
    <div>
      <GlobalStyle />
      <ProfilEditor
        onClick={profile => {
          console.log(profile);
        }}
      ></ProfilEditor>
    </div>
  </ThemeProvider>
);
