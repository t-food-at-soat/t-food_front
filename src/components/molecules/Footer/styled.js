import styled from "styled-components";

const FooterWrapper = styled.div`
  padding: 2rem 0;
  display: flex;
  justify-content: space-around;
  align-items: center;
  flex-direction: row;
  width: 100%;
  background: ${props => props.theme.colors.darkgrey};

  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

const ButtonGroup = styled.div`
  display: flex;
  flex-basis: 50%;
  justify-content: space-between;
  
  button {
    width:30%
  }
  
  @media (max-width: 768px) {
    flex-direction: column;
    
    button {
      width:100%;
      padding: 0 2.5rem;
      margin: 1rem 0;
    }
  }
  
  
`;

export {FooterWrapper, ButtonGroup};
