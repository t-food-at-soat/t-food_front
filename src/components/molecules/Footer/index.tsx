import React from "react";
import { FooterWrapper, ButtonGroup } from "./styled";
import Logo from "../../atoms/Logo";
import Button from "../../atoms/Button";
import Title from "../../atoms/Title";
import { theme } from "../../_settings/theme";

const Footer = ({ ...props }) => {
  return (
    <FooterWrapper>
      <Logo dark={true} content={"food"} />
      <ButtonGroup>
        <Button {...theme.dark} onClick={() => console.log("pipou")}>
          <Title dark={true}>
            <a href="/t-chart.pdf" target="_blank" style={{ color: "white" }}>
              La<span>T</span>
              <span>chart</span>
            </a>
          </Title>
        </Button>
        <Button {...theme.dark} onClick={() => console.log("pipou")}>
          <Title dark={true}>
            Nous<span> </span>
            <span>contacter</span>
          </Title>
        </Button>
        <Button {...theme.dark} onClick={() => console.log("pipou")}>
          <Title dark={true}>
            Faire<span> </span>
            <span>un don</span>
          </Title>
        </Button>
      </ButtonGroup>
    </FooterWrapper>
  );
};

export default Footer;
