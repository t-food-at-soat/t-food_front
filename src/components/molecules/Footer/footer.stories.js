import React from "react";
import Footer from "./index";
import { ThemeProvider } from "styled-components";
import { GlobalStyle, theme } from "../../_settings/theme";

export default { title: "Molecules|Footer" };

export const FooterStyled = () => {
  return (
    <ThemeProvider theme={theme}>
      <>
        <GlobalStyle />
        <Footer />
      </>
    </ThemeProvider>
  );
};
