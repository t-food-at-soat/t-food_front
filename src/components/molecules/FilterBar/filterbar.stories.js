import React from "react";
import { ThemeProvider } from "styled-components";
import { GlobalStyle, theme } from "../../_settings/theme";
import FilterBar from "./index";
export default { title: "Molecules|FilterBar" };

export const FilterBarStyled = () => {
  return (
    <ThemeProvider theme={theme}>
      <>
        <GlobalStyle />
        <FilterBar
          {...theme.light}
          onFilterChange={filterResult => console.log(filterResult)}
        />
      </>
    </ThemeProvider>
  );
};
