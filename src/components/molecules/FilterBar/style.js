import styled from "styled-components";

const FilterBarWrapper = styled.div`
  ${props => props.theme.roundBox(props)};
  flex-direction: row;
  height: 70px;

  @media (max-width: 768px) {
    flex-direction: column;
    height: 170px;
    justify-content: space-between;
  }
`;

const FilterWrapper = styled.div`
  width: 100%;
  margin: 1rem;
  position:relative;
  
  @media (max-width: 768px) {
    width: auto;
  }

  div svg {
    width: 3.5rem !important;
    height: 3.5rem !important;
  }
`;

export { FilterBarWrapper, FilterWrapper };
