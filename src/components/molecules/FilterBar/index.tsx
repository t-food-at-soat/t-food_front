import React, { useState } from "react";
import { FilterBarWrapper, FilterWrapper } from "./style";
import Filter from "../Filter";
import { ReactComponent as OrderIcon } from "../../../assets/icons/order.svg";
import { theme } from "../../_settings/theme";
import Title from "../../atoms/Title/index";
import OfferType from "../../../ts/types/offerType.types";

export interface IFilterData {
  types: OfferType[];
  quantities: string[];
  distances: string[];
}
interface IFilterBarProps {
  filter: IFilterData;
  onFilterChange: (filterElements: any) => void;
}

const dataA = [
  { id: 1, title: OfferType.Pizza, value: OfferType.Pizza },
  {
    id: 2,
    title: OfferType.BuffetChaud,
    value: OfferType.BuffetChaud
  },
  {
    id: 3,
    title: OfferType.BuffetFroid,
    value: OfferType.BuffetFroid
  },
  { id: 4, title: OfferType.Salades, value: OfferType.Salades }
];

const dataB = [
  { id: 1, title: "1 personne", value: 1 },
  { id: 2, title: "2 personnes", value: 2 },
  { id: 3, title: "3 personnes", value: 3 },
  { id: 4, title: "plus", value: 4 }
];

const dataC = [{ id: 1, title: "Plus proche au plus loin", value: 1 }];

const FilterBar = ({
  filter,
  onFilterChange = (filterElements: any) => {}
}: IFilterBarProps) => {
  const handleFilterChange = (filterResult: any) => {
    filter = {
      ...filterResult,
      ...filter
    };
    onFilterChange(filter);
  };
  return (
    <FilterBarWrapper>
      <FilterWrapper>
        <Filter
          filterTitle={
            <Title font={"Milkshake"}>
              <span>Filtrer par type</span>
            </Title>
          }
          data={dataA}
          selectedItems={filter.types}
          {...theme.light}
          icon={OrderIcon}
          onFilterChange={data => {
            handleFilterChange({ types: data });
          }}
        />
      </FilterWrapper>
      <FilterWrapper>
        <Filter
          filterTitle={
            <Title font={"Milkshake"}>
              <span>Filtrer par quantité</span>
            </Title>
          }
          data={dataB}
          selectedItems={filter.quantities}
          {...theme.light}
          icon={OrderIcon}
          onFilterChange={data => handleFilterChange({ quantities: data })}
        />
      </FilterWrapper>
      <FilterWrapper>
        <Filter
          filterTitle={
            <Title font={"Milkshake"}>
              <span>Filtrer par distance</span>
            </Title>
          }
          data={dataC}
          selectedItems={filter.distances}
          {...theme.light}
          icon={OrderIcon}
          onFilterChange={data => handleFilterChange({ distances: data })}
        />
      </FilterWrapper>
    </FilterBarWrapper>
  );
};

export default FilterBar;
