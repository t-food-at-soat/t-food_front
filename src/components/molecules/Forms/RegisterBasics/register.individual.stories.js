import React from "react";
import RegisterBasis from "./index";
import { ThemeProvider } from "styled-components";
import { GlobalStyle, theme } from "../../../_settings/theme";
import { Provider } from "react-redux";
import store from "../../../../store/configureStore";

export default { title: "Molecules|Register Basis" };

export const RegisterBasisStyled = () => {
  const basis = {
    username: "",
    email: "",
    password: "",
    confirmPassword: "",
    imageUrl: ""
  };
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <>
          <GlobalStyle />
          <RegisterBasis
            onChange={basis => {
              console.log(basis);
            }}
            basis={basis}
          />
        </>
      </ThemeProvider>
    </Provider>
  );
};
