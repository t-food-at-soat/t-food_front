import styled from "styled-components";

const RegisterBasicsStyled = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: column; 
`;

export { RegisterBasicsStyled };
