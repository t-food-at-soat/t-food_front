import React from "react";
import { RegisterBasicsStyled } from "./styled";
import { ReactComponent as MailIcon } from "../../../../assets/icons/email.svg";
import { ReactComponent as LockIcon } from "../../../../assets/icons/lock.svg";
import { ReactComponent as CameraIcon } from "../../../../assets/icons/camera.svg";
import { ReactComponent as UserIcon } from "../../../../assets/icons/user.svg";
import Input from "../../../atoms/Input";
import { theme } from "../../../_settings/theme";

interface IRegisterBasisProps {
  basis: any;
  onChange: (basis: any) => void;
}

const RegisterBasis = ({ basis, onChange }: IRegisterBasisProps) => {
  return (
    <RegisterBasicsStyled>
      <Input
        {...theme.light}
        IconComponent={UserIcon}
        placeholder={"Nom"}
        name={"name"}
        type={"text"}
        value={basis.name}
        onChange={onChange}
        isRequired={true}
      />
      <Input
        {...theme.light}
        IconComponent={MailIcon}
        placeholder={"Email"}
        name={"email"}
        type={"text"}
        value={basis.email}
        onChange={onChange}
        validation={"email"}
        isRequired={true}
      />
      <Input
        {...theme.light}
        IconComponent={LockIcon}
        placeholder={"Mot de passe"}
        type={"password"}
        name={"password"}
        value={basis.password}
        onChange={onChange}
        isRequired={true}
      />
      <Input
        {...theme.light}
        IconComponent={CameraIcon}
        placeholder={"Photo"}
        name={"picture"}
        value={basis.pictureLabel}
        type={"file"}
        onChange={onChange}
      />
    </RegisterBasicsStyled>
  );
};

export default RegisterBasis;
