import styled from 'styled-components';


interface iConnexionFormWrapperProps {
    dark ?: boolean
    theme: {
        colors : any
    }
}

const ConnexionFormWrapper = styled.div<iConnexionFormWrapperProps>`
   text-align: center;
  font-family: This,serif;  
  font-size : 4rem;
  color: ${props => props.dark ? props.theme.colors.white : props.theme.colors.green};
  span {
    color: ${props => props.dark ? props.theme.colors.white : props.theme.colors.darkgrey};
    font-size: 1.5rem;
    margin: 0;
    font-family: "Hiragino",serif;
    cursor: pointer;
    &:hover{
    text-decoration: underline;
    }
  }
  div div  {
    margin-bottom: 1.7rem;
  }
  
  svg {
    height: 1.5rem !important;
  }
`;



export { ConnexionFormWrapper };