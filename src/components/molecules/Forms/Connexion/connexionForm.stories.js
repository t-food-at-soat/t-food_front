import React from "react";
import { Provider } from "react-redux";
import ConnexionForm from "./index";
import { ThemeProvider } from "styled-components";
import { GlobalStyle, theme } from "../../../_settings/theme";
import store from "../../../../store/configureStore";

export default { title: "Molecules|Connexion Form" };

export const ConnexionFormDefault = () => (
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <>
        <GlobalStyle />
        <ConnexionForm />
      </>
    </ThemeProvider>
  </Provider>
);
