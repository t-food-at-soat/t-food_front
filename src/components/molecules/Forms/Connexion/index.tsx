import React, { useState } from "react";
import { ConnexionFormWrapper } from "./styled";
import Input from "../../../atoms/Input";
import { theme } from "../../../_settings/theme";
import { ReactComponent as MailIcon } from "../../../../assets/icons/email.svg";
import { ReactComponent as LockIcon } from "../../../../assets/icons/lock.svg";
import Title from "../../../atoms/Title";
import Button from "../../../atoms/Button";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { AuthenticationAction } from "../../../../store/authentication/types";
import { login } from "../../../../store/authentication/actions";
import { getResetURL } from "../../../../store/resetPassword/actions";
import { IRootState } from "../../../../store/combiners";
import { navigate } from "hookrouter";
import { ResetPasswordAction } from "../../../../store/resetPassword/types";
import {toast} from "react-toastify";

interface IConnexionFormProps {
  classNameConnexion?: string;
}

const ConnexionForm = ({
  login,
  isInvalid,
  isConnected,
  errorMessage,
  getResetURL,
  classNameConnexion
}: IConnexionFormProps & IPropsDispatch & IPropsState) => {
  const [credentials, setCredentials] = useState({
    email: "",
    password: ""
  });
  const handleChange = (data: any) => {
    return setCredentials({ ...credentials, ...data });
  };
  const onSubmit = () => {
    login(credentials.email, credentials.password);
  };
  const forgotMdp = () => {
    getResetURL(credentials.email);
  };

  const handleKeyPress = (event: any) => {
    if (event.key === "Enter") {
      login(credentials.email, credentials.password);
    }
  };

  const handleMdpForget = () => {
    if (!credentials.email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)){
      toast.error("Email requis")
    } else {
      forgotMdp()
    }
  };

  return (
    <ConnexionFormWrapper
      className={classNameConnexion}
      onClick={e => e.stopPropagation()}
    >
      <div>
        <Input
          {...theme.light}
          value={credentials.email}
          IconComponent={MailIcon}
          type={"email"}
          name="email"
          placeholder={"email"}
          onChange={handleChange}
          validation={"email"}
          isInvalid={isInvalid}
        />
        <Input
          {...theme.light}
          type={"password"}
          value={credentials.password}
          IconComponent={LockIcon}
          name="password"
          placeholder={"mot de passe"}
          onChange={handleChange}
          isInvalid={isInvalid}
          onKeyPress={handleKeyPress}
        />
        <Button size="big" {...theme.light} onClick={onSubmit}>
          <Title font={"Milkshake"} fontSize={"1.5rem"}>
            <span>Connexion</span>
          </Title>
        </Button>
        <span onClick={handleMdpForget} >mot de passe oublié ?</span>
      </div>
      {isConnected && navigate("/offer")}
    </ConnexionFormWrapper>
  );
};

interface IPropsDispatch {
  login: (email: string, password: string) => void;
  getResetURL: (email: string) => void;
}

interface IPropsState {
  isInvalid: boolean;
  isConnected: boolean;
  errorMessage: string;
}

const mapStateToProps = ({ authentication }: IRootState): IPropsState => ({
  isInvalid: authentication.isInvalid,
  isConnected: authentication.isConnected,
  errorMessage: authentication.errorMessage
});

const mapDispatchToProps = (
  dispatch: Dispatch<AuthenticationAction> & Dispatch<ResetPasswordAction>
): IPropsDispatch => ({
  login: (email: string, password: string) => dispatch(login(email, password)),
  getResetURL: (email: string) => dispatch(getResetURL(email))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ConnexionForm);
