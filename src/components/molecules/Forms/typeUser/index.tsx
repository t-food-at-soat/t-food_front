import React from "react";
// import Button from "../../../atoms/Button";
import Title from "../../../atoms/Title";
import { TypeUserFormStyled } from "./styled";
import { theme } from "../../../_settings/theme";
import Button from "../../../atoms/Button";
import { ReactComponent as AssociationIcon } from "../../../../assets/icons/association.svg";
import { ReactComponent as EntrepriseIcon } from "../../../../assets/icons/entreprise.svg";
import { ReactComponent as ParticulierIcon } from "../../../../assets/icons/particulier.svg";
import Roles from "../../../../ts/types/roles.types";

interface ITypeUserProps {
  onClick: (data: Roles) => void;
}

const TypeUser = ({ onClick }: ITypeUserProps) => {
  const handleClick = (data: Roles) => onClick(data);
  return (
    <TypeUserFormStyled>
      <Button
        icon={AssociationIcon}
        iconColor="#efc364"
        {...theme.light}
        onClick={() => handleClick(Roles.ASSOCIATION)}
      >
        <Title font={"Milkshake"}>
          <span style={{ color: "#efc364" }}>association</span>
        </Title>
      </Button>
      <Button
        icon={EntrepriseIcon}
        iconColor="#52a3dd"
        {...theme.light}
        onClick={() => handleClick(Roles.CORPORATION)}
      >
        <Title font={"Milkshake"}>
          <span style={{ color: "#52a3dd" }}>entreprise</span>
        </Title>
      </Button>
      <Button
        icon={ParticulierIcon}
        iconColor="#f15e1b"
        {...theme.light}
        onClick={() => handleClick(Roles.INDIVIDUAL)}
      >
        <Title font={"Milkshake"}>
          <span style={{ color: "#f15e1b" }}>particulier</span>
        </Title>
      </Button>
    </TypeUserFormStyled>
  );
};

export default TypeUser;
