import styled from "styled-components";

const TypeUserFormStyled = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  height: 100%;

  button {
    margin: 0.5rem 0;
    flex-direction: row-reverse;

    svg {
      width: 5rem !important;
      height: 5rem !important;
      padding-right: 1.5rem;
    }
  }
  
  min-width: 30%;
  
   @media (max-width: 768px) {
     min-width: 80%;
  }
`;

export {TypeUserFormStyled};
