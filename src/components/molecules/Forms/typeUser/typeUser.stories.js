import React from "react";
import UserType from "./index";
import { ThemeProvider } from "styled-components";
import { GlobalStyle, theme } from "../../../_settings/theme";
import store from "../../../../store/configureStore";
import { Provider } from "react-redux";

export default { title: "Molecules|UserType" };

export const UserTypeStyled = () => {
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <>
          <GlobalStyle />
          <UserType />
        </>
      </ThemeProvider>
    </Provider>
  );
};
