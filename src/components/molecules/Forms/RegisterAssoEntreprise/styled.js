import styled from "styled-components";

const RegisterAssoEntrepriseStyled = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: column;
  min-width: 100% !important;
`;

export { RegisterAssoEntrepriseStyled };
