import React from "react";
import { RegisterAssoEntrepriseStyled } from "./styled";
import Input from "../../../atoms/Input";
import { theme } from "../../../_settings/theme";
import {ReactComponent as SiretIcon} from '../../../../assets/icons/barcode.svg'

interface IRegisterAssoEntrepriseProps {
  infoAssoEntreprise: any;
  onChange: (infoAssoEntreprise: any) => void;
}

const RegisterAssoEntreprise = ({
  infoAssoEntreprise,
  onChange
}: IRegisterAssoEntrepriseProps) => {
  return (
    <RegisterAssoEntrepriseStyled>
      <Input
        {...theme.light}
        IconComponent={SiretIcon}
        placeholder={"N° Siret"}
        name={"numSiret"}
        type={"number"}
        value={infoAssoEntreprise.numSiret}
        isRequired={true}
        onChange={onChange}
      />
    </RegisterAssoEntrepriseStyled>
  );
};

export default RegisterAssoEntreprise;
