import React from "react";
import { Provider } from "react-redux";
import RegisterAssoEntreprise from "./index";
import { ThemeProvider } from "styled-components";
import { GlobalStyle, theme } from "../../../_settings/theme";
import store from "../../../../store/configureStore";

export default { title: "Molecules|Register EntrepriseAssociation Info" };

export const RegisterAssociationEntreprise = () => {
  const infoAssoEntreprise = {
    numSiret: ""
  };

  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <>
          <GlobalStyle />
          <RegisterAssoEntreprise
            infoAssoEntreprise={infoAssoEntreprise}
            onChange={info => console.log(info)}
          />
        </>
      </ThemeProvider>
    </Provider>
  );
};
