import React from "react";
import { RegisterAddressStyled } from "./styled";
import Autocomplete from "../../../atoms/Autocomplete";
interface IRegisterAddressProps {
  address: any;
  onChange: (basis: any) => void;
}

const RegisterAddress = ({ address, onChange }: IRegisterAddressProps) => {
  return (
    <RegisterAddressStyled>
      <Autocomplete onChange={onChange} />
    </RegisterAddressStyled>
  );
};

export default RegisterAddress;
