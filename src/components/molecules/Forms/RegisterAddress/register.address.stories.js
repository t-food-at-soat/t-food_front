import { Provider } from "react-redux";
import React from "react";
import RegisterAddress from "./index";
import { ThemeProvider } from "styled-components";
import { GlobalStyle, theme } from "../../../_settings/theme";
import store from "../../../../store/configureStore";

export default { title: "Molecules|Register Address" };

export const RegisterAddressStyled = () => {
  const address = {
    username: "",
    email: "",
    password: "",
    confirmPassword: "",
    imageUrl: ""
  };
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <>
          <GlobalStyle />
          <RegisterAddress
            onChange={address => {
              console.log(address);
            }}
            address={address}
          />
        </>
      </ThemeProvider>
    </Provider>
  );
};
