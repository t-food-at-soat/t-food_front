import styled from "styled-components";

const RegisterAddressStyled = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: column;
  
  div {
    width: 100% !important;
  }
  
`;

export { RegisterAddressStyled };
