import React from 'react';
import ReservationBox from './index';
import {ThemeProvider} from 'styled-components';
import {GlobalStyle, theme} from '../../_settings/theme';

export default {title: 'Molecules|ReservationBox'};

export const reservationBox = () => <ThemeProvider theme={theme}>
  <>
    <GlobalStyle/>
    <div style={{width:"600px",height:"300px"}}>
      <ReservationBox />
    </div>
  </>
</ThemeProvider>;
