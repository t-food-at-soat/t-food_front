import React from "react";
import { ReservationBoxWrapper, ButtonWrapper } from "./styled";
import { theme } from "../../_settings/theme";
import Title from "../../atoms/Title";
import Button from "../../atoms/Button";
import Mascot from "../../atoms/Mascot";
import { Dispatch } from "redux";
import { OfferAction } from "../../../store/offer/types";
import { reserveOffer } from "../../../store/offer/actions";
import { connect } from "react-redux";
import { ILoggedInPayload } from "../../../store/authentication/types";
import { IRootState } from "../../../store/combiners";
import Offer from "../../../ts/interfaces/offer.interfaces";
import { ReactComponent as FearMascot } from "./fear.svg";
import { navigate } from "hookrouter";
import { cancelReservation } from "../../../store/reservation/actions";
import { ReservationActions } from "../../../store/reservation/types";

interface IReservationBoxProps {
  className?: string;
  offer: Offer & any;
  isPassed: boolean;
}

const ReservationBox = ({
  className,
  offer,
  reserveOffer,
  cancelReservation,
  userConnected,
  isPassed = false
}: IReservationBoxProps & IPropsDispatch & IPropsState) => {
  console.log(offer);
  return (
    <ReservationBoxWrapper className={className}>
      {userConnected && userConnected.id === offer.ReservationUserId ? (
        <>
          {!isPassed ? (
            <>
              <ButtonWrapper>
                <Button
                  size="big"
                  {...theme.light}
                  onClick={() => cancelReservation(offer.ReservationId)}
                >
                  <Title font={"Milkshake"}>
                    <span>
                      <span>J’annule !</span>
                    </span>
                  </Title>
                </Button>
              </ButtonWrapper>
              <FearMascot />
            </>
          ) : (
            <Mascot />
          )}
        </>
      ) : (
        <>
          <ButtonWrapper>
            {!isPassed ? (
              userConnected && userConnected.id === offer.author.id ? (
                <>
                  <Button
                    size="big"
                    {...theme.light}
                    onClick={() => {
                      console.log(offer.id);
                      navigate(`/offer/update/${offer.id}`);
                    }}
                  >
                    <Title font={"Milkshake"}>
                      <span>
                        <span>Je modifie !</span>
                      </span>
                    </Title>
                  </Button>
                  <Button
                    size="big"
                    {...theme.light}
                    onClick={() => reserveOffer(Number(offer.id))}
                  >
                    <Title font={"Milkshake"}>
                      <span>
                        <span>J'annule !</span>
                      </span>
                    </Title>
                  </Button>
                </>
              ) : (
                <Button
                  size="big"
                  {...theme.light}
                  onClick={() => reserveOffer(Number(offer.id))}
                >
                  <Title font={"Milkshake"}>
                    <span>
                      <span>Je réserve</span>
                    </span>
                  </Title>
                </Button>
              )
            ) : null}
          </ButtonWrapper>
          <Mascot />
        </>
      )}
    </ReservationBoxWrapper>
  );
};

interface IPropsDispatch {
  reserveOffer: (id: number) => void;
  cancelReservation: (id: number) => void;
}

interface IPropsState {
  userConnected: ILoggedInPayload | undefined;
}

const mapStateToProps = ({ authentication }: IRootState): IPropsState => ({
  userConnected: authentication.userConnected
});

const mapDispatchToProps = (
  dispatch: Dispatch<OfferAction | ReservationActions>
): IPropsDispatch => ({
  reserveOffer: (id: number) => dispatch(reserveOffer(id)),
  cancelReservation: (id: number) => dispatch(cancelReservation(id))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReservationBox);
