import styled from 'styled-components';


interface IReservationBoxWrapperProps {

}

const ReservationBoxWrapper = styled.div<IReservationBoxWrapperProps>`
  background-color: ${props => props.theme.colors.darkgrey};
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: space-around;
  align-items: center;
  
  svg {
    transform: scaleX(-1);
    height: 238px;
  }
`;

const ButtonWrapper = styled.div`
  flex-basis: 40%;
`;



export { ReservationBoxWrapper, ButtonWrapper };