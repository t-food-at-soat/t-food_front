import React, { useState, FunctionComponent } from "react";
import { FilterWrapper } from "./styled";
import { theme } from "../../_settings/theme";
import Input from "../../atoms/Input";
import Title from "../../atoms/Title";

interface Item {
  id: number;
  title: string;
  value: any;
}

interface IFilterProps {
  data: Item[];
  selectedItems: any[];
  filterTitle: JSX.Element;
  onFilterChange: (selectedItems: string[]) => void;
  icon: React.FunctionComponent<React.SVGProps<SVGSVGElement>>;
}

const Filter = ({
  data,
  selectedItems,
  icon,
  onFilterChange,
  filterTitle,
  ...props
}: IFilterProps) => {
  const [visibility, setVisibility] = useState<boolean | void>();

  const Icon = icon;

  const handleFilterChange = (item: any) => {
    let index = selectedItems.indexOf(item.value);
    item.checked
      ? selectedItems.push(item.value)
      : selectedItems.splice(index, 1);

    onFilterChange(selectedItems);
  };

  const handleChildClick = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>
  ) => {
    event.stopPropagation();
  };

  return (
    <FilterWrapper {...props} onClick={() => setVisibility(!visibility)}>
      <Title>
        <span>{filterTitle}</span>
        <Icon
          style={{
            width: "1rem",
            paddingLeft: "1rem",
            verticalAlign: "middle"
          }}
        />
      </Title>
      {visibility && (
        <div>
          {data.map((item: Item) => {
            return (
              <div key={item.id}>
                <Input
                  isBoxShadowDisplayed={true}
                  {...theme.light}
                  checked={selectedItems.includes(item.value.toString())}
                  type={"checkbox"}
                  label={item.title}
                  name={"value"}
                  value={item.value}
                  onClick={handleChildClick}
                  onChange={handleFilterChange}
                />
              </div>
            );
          })}
        </div>
      )}
    </FilterWrapper>
  );
};

export default Filter;
