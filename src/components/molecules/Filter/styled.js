import styled from "styled-components";

const FilterWrapper = styled.div`
  ${props => props.theme.roundBox(props)};
  justify-content: center;
  align-items: center;
  flex-direction: column;
  border: 1px solid #aba8ab;
  width: 100%;
  position: absolute;
  z-index: 10;
  
  @media (max-width: 768px) {
    position: inherit;
  }
`;

export { FilterWrapper };
