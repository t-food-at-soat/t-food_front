import React from "react";
import Filter from "./index";
import { ThemeProvider } from "styled-components";
import { GlobalStyle, theme } from "../../_settings/theme";
import { ReactComponent as FilterIcon } from "../../../assets/icons/filter.svg";

export default { title: "Molecules|Filter" };

export const FilterStyled = () => {
  const data = [
    { id: 1457, title: "Pizza" },
    { id: 2544, title: "Salade" },
    { id: 378987, title: "Boisson" }
  ];

  const handleFilterChange = list => {
    console.log(list);
  };

  return (
    <ThemeProvider theme={theme}>
      <>
        <GlobalStyle />
        <Filter
          filterTitle="Categorie 1"
          data={data}
          {...theme.light}
          icon={FilterIcon}
          onFilterChange={handleFilterChange}
        />
      </>
    </ThemeProvider>
  );
};
