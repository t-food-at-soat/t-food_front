import React from 'react';
import ImageBox from './index';
import {ThemeProvider} from 'styled-components';
import {GlobalStyle, theme} from '../../_settings/theme';

export default {title: 'Atoms|ImageSwapMap'};

export const ImageBoxWithShadow = () => <ThemeProvider theme={theme}>
  <>
    <GlobalStyle/>
    <div style={{width:"600px",height:"300px"}}>
      <ImageBox
          sourceImage={"https://images.unsplash.com/photo-1543882570-022581e299e7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2797&q=80"}
          title={"Test"}/>
    </div>
  </>
</ThemeProvider>;
