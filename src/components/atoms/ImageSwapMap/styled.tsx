import styled from 'styled-components';


interface ImageSwapMapWrapperProps {
    sourceImage?: string,
    swap: boolean
}

const ImageSwapMapWrapper = styled.div<ImageSwapMapWrapperProps>`
  background: url(${props => props.sourceImage}) no-repeat;
  background-size : cover;
  height: 100%;
  width: 100%;
  position: absolute;
  perspective: 1000px;
  transition: transform 0.8s;
  transform-style: preserve-3d;
  backface-visibility: hidden;
  ${props => (props.swap && "transform: rotateX(180deg)")};
  grid-area: img;
  #direction-example {
    transform-style: preserve-3d;
    backface-visibility: hidden;
    position:absolute;
    top:0;
  }
`;

interface IMapSwapButtonProps {
    swap: boolean
}

const MapSwapButton = styled.div<IMapSwapButtonProps>`
    cursor: pointer;
    transition: all 300ms ease-in-out;
    width: 0;
    height: 0;
    border-bottom: 100px solid transparent;
    border-left: 100px solid ${props => (props.swap ? props.theme.colors.green : props.theme.colors.blue)};
    
    svg {
      transform-style: preserve-3d;
      backface-visibility: hidden;
      transform-origin: top left;
      transition: all 300ms ease-in-out;
      position: absolute;
      left: 7px;
      top: 0px;
      width: 3.5rem;
      transform: scale(1);   
    }
    
    &:hover{      
      border-bottom: 130px solid transparent;
      border-left: 130px solid ${props => (props.swap ? props.theme.colors.green : props.theme.colors.blue)};
      svg {
          transform: scale(1.5);
      }
    }
`;

const ImageSwapMapContainer = styled.div`
  position: relative;
  height: 100%;
  width: 100%;
`;


export {ImageSwapMapWrapper, MapSwapButton, ImageSwapMapContainer};