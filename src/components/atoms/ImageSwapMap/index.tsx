import React, { useState } from "react";
import {
  ImageSwapMapWrapper,
  MapSwapButton,
  ImageSwapMapContainer
} from "./styled";
import { ReactComponent as MapIcon } from "./map-location.svg";
import { ReactComponent as PictureIcon } from "./picture.svg";
import Map from "../Map";

interface iImageBoxProps {
  sourceImage: any;
  title: string;
  className?: string;
  handleDirectionDuration?: (duration: string) => void;
  destination: string;
}

const ImageSwapMap = ({
  sourceImage,
  className,
  handleDirectionDuration,
  destination
}: iImageBoxProps) => {
  const [swap, setSwap] = useState(false);

  const handleSwap = () => {
    setSwap(!swap);
  };
  return (
    <ImageSwapMapContainer className={className}>
      <ImageSwapMapWrapper sourceImage={sourceImage} swap={swap}>
        <MapSwapButton onClick={handleSwap} swap={swap}>
          <MapIcon />
        </MapSwapButton>
      </ImageSwapMapWrapper>
      <ImageSwapMapWrapper swap={!swap}>
        <Map
          handleDirectionDuration={handleDirectionDuration}
          destination={destination}
        />
        <MapSwapButton onClick={handleSwap} swap={swap}>
          <PictureIcon />
        </MapSwapButton>
      </ImageSwapMapWrapper>
    </ImageSwapMapContainer>
  );
};

export default ImageSwapMap;
