import styled from "styled-components";
import Roles from "../../../ts/types/roles.types";

interface iMascotWrapperProps {
  type?: string;
  theme: {
    colors: any;
  };
}

const getColor = (props: iMascotWrapperProps) => {
  switch (props.type) {
    case Roles.INDIVIDUAL:
      return props.theme.colors.darkorange;
    case Roles.ASSOCIATION:
      return props.theme.colors.yellow;
    case Roles.CORPORATION:
      return props.theme.colors.blue;
    default:
      return props.theme.colors.white;
  }
};

const MascotWrapper = styled.div<iMascotWrapperProps>`
  #tablier {
    fill: ${(props: iMascotWrapperProps) => getColor(props)};
  }
`;

export default MascotWrapper;
