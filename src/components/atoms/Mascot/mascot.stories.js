import React from "react";
import Mascot from "./index";
import { ThemeProvider } from "styled-components";
import { GlobalStyle, theme } from "../../_settings/theme";
import Roles from '../../../ts/types/roles.types';

export default { title: "Atoms|Mascot" };

export const HomeMascot = () => (
  <ThemeProvider theme={theme}>
    <>
      <GlobalStyle />
      <Mascot />
    </>
  </ThemeProvider>
);

export const IndividualMascot = () => (
  <ThemeProvider theme={theme}>
    <>
      <GlobalStyle />
      <Mascot type={Roles.INDIVIDUAL} />
    </>
  </ThemeProvider>
);

export const AssociationMascot = () => (
  <ThemeProvider theme={theme}>
    <>
      <GlobalStyle />
      <Mascot type={Roles.ASSOCIATION} />
    </>
  </ThemeProvider>
);
