import React from "react";
import { ReactComponent as MascotSVG } from "./masco.svg";
import { ReactComponent as MascotSadSVG } from "./masco_sad.svg";
import MascotWrapper from "./styled";
import Roles from "../../../ts/types/roles.types";

interface iMascotProps {
  type?: Roles;
  isSad?: boolean;
}

const Mascot = ({ type, isSad }: iMascotProps) => {
  return (
    <MascotWrapper type={type}>
      {isSad ? <MascotSadSVG /> : <MascotSVG />}
    </MascotWrapper>
  );
};

export default Mascot;
