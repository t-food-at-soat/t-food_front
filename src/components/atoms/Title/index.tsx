import React from "react";
import TitleWrapper from "./styled";

export interface iTitleProps {
  font?: string;
  children: object;
  dark?: boolean;
  fontSize?: string;
  Tcolor?: string;
  Lcolor?: string;
}

const Title = ({
  font,
  children,
  dark,
  Tcolor,
  Lcolor,
  fontSize
}: iTitleProps) => {
  return (
    <TitleWrapper
      font={font}
      dark={dark}
      Tcolor={Tcolor}
      Lcolor={Lcolor}
      fontSize={fontSize}
    >
      {children}
    </TitleWrapper>
  );
};

export default Title;
