import styled from "styled-components";

interface iTitleWrapperProps {
  font?: string;
  dark?: boolean;
  fontSize?: string;
  theme: {
    colors: any;
  };
  Tcolor?: string;
  Lcolor?: string;
}

const getTColor = (props: any) => {
  if (props.dark) {
    return props.theme.colors.white;
  }

  if (props.Tcolor) {
    return props.theme.colors[props.Tcolor];
  }

  return props.theme.colors.darkgrey;
};

const getLabelColor = (props: any) => {
  if (props.dark) {
    return props.theme.colors.white;
  }

  if (props.Lcolor) {
    return props.theme.colors[props.Lcolor];
  }

  return props.theme.colors.green;
};

const TitleWrapper = styled.span<iTitleWrapperProps>`
  font-family: Milkshake, serif;
  font-size: ${props => (props.fontSize ? props.fontSize : "2.4rem")};
  color: ${props =>
    props.dark ? props.theme.colors.white : props.theme.colors.darkgrey};
  span {
    span {
      color: ${props => getTColor(props)};
    }
    font-family: ${(props: iTitleWrapperProps) =>
        props.font ? props.font : "This"},
      serif;
    color: ${props => getLabelColor(props)};
  }
`;

export default TitleWrapper;
