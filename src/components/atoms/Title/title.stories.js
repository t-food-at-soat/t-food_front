import React from 'react';
import Title from './index';
import {ThemeProvider} from 'styled-components';
import {GlobalStyle, theme} from '../../_settings/theme';

export default {title: 'Atoms|Title'};

export const cquoiTitle = () => <ThemeProvider theme={theme}>
  <>
    <GlobalStyle/>
    <Title>
      Mais <span><span>T</span>food</span>, c’est quoi ?
    </Title>
  </>
</ThemeProvider>;

export const ttrustTitle = () => <ThemeProvider theme={theme}>
  <>
    <GlobalStyle/>
    <Title>
      Les <span><span>T</span>trust</span>
    </Title>
  </>
</ThemeProvider>;

export const sloganTitle = () => <ThemeProvider theme={theme}>
  <>
    <GlobalStyle/>
    <Title font={"Milkshake"}>
      <span>Manger</span> c’est cool, <span>gâcher</span> c’est les boules
    </Title>
  </>
</ThemeProvider>;

export const tinforamtionTitle = () => <ThemeProvider theme={theme}>
  <>
    <GlobalStyle/>
    <Title>
      Mes <span><span>T</span>informations</span>
    </Title>
  </>
</ThemeProvider>;

export const thistoriqueTitle = () => <ThemeProvider theme={theme}>
  <>
    <GlobalStyle/>
    <Title>
      Mon <span><span>T</span>historique</span>
    </Title>
  </>
</ThemeProvider>;

export const tWithDifferentColorTitle = () => <ThemeProvider theme={theme}>
  <>
    <GlobalStyle/>
    <Title Tcolor={"darkorange"} Lcolor={"yellow"}>
      Mon <span><span>T</span>historique</span>
    </Title>
    <br/>
    <Title Tcolor={"yellow"} Lcolor={"green"}>
      Mon <span><span>T</span>historique</span>
    </Title>
  </>
</ThemeProvider>;

  export const darkCquoiTitle = () => <ThemeProvider theme={theme}>
  <>
    <GlobalStyle/>
    <div style={{background: "#4F4F4F", padding: "15px"}}>
      <Title dark={true}>
        Mais <span><span>T</span>food</span>, c’est quoi ?
      </Title>
    </div>
  </>
</ThemeProvider>;

