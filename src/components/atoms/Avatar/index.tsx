import React from 'react';
import {AvatarWrapper} from "./styled";

interface iAvatarProps {
    avatarUrl ?: string,
    size : string
}

const Avatar = ({avatarUrl, size} : iAvatarProps) => {
    return (
        <AvatarWrapper avatarUrl={avatarUrl} size={size} />
    );
};

export default Avatar;
