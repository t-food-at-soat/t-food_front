import React from 'react';
import Avatar from './index';
import {ThemeProvider} from 'styled-components';
import {GlobalStyle, theme} from '../../_settings/theme';

export default {title: 'Atoms|Avatar'};

export const avatarDefault = () => <ThemeProvider theme={theme}>
  <>
    <GlobalStyle/>
    <Avatar size={"15rem"}/>
    <Avatar size={"30rem"}/>
  </>
</ThemeProvider>;

export const ExampleDefault = () => <ThemeProvider theme={theme}>
  <>
    <GlobalStyle/>
    <Avatar size={"15rem"} avatarUrl={"https://sonet.soat.fr/Trombinoscope/1671"}/>
  </>
</ThemeProvider>;