import styled from 'styled-components';


interface iLogoWrapperProps {
    avatarUrl ?: string,
    size : string
}

const AvatarWrapper = styled.div<iLogoWrapperProps>`
    background: ${props => props.avatarUrl ? `url(${props.avatarUrl}) no-repeat center center` : props.theme.colors.darkgrey};
    background-size: cover;
    width: ${props => props.size};
    height: ${props => props.size};
    border-radius: 50%;
`;



export { AvatarWrapper };