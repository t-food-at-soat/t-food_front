import styled from "styled-components";

const CardTitleWrapper = styled.div<any>`
  font-family: hiragino, serif;
  width: 100%;
  margin-top: 0.8rem;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 3rem;
  padding: 0.3rem 0;
  background: ${props =>
    props.isOutlined
      ? `url(${props.imgSrc}) no-repeat center`
      : props.isPassed
      ? props.theme.colors.blue
      : props.isCanceled
      ? props.theme.colors.darkorange
      : props.theme.colors.green};
  background-size: cover;
  border: ${props =>
    props.isOutlined ? `0.2rem solid ${props.theme.colors.green}` : "none"};

  span {
    font-family: hiragino, serif;
    font-size: 1.6rem;
    color: ${props => props.theme.colors.white};
  }
`;

export default CardTitleWrapper;
