import React from 'react';
import CartTitle from './index';
import {ThemeProvider} from 'styled-components';
import {GlobalStyle, theme} from '../../_settings/theme';

export default {
  title: 'Atoms|CardTitle',
  includeStories: ['cardTitleWithText',
                
 
],};

export const cardTitleWithText = () => <ThemeProvider theme={theme}>
  <>
    <GlobalStyle/>
    <CartTitle>
      <span>Pizzas Meetup</span>
    </CartTitle>
  </>
</ThemeProvider>;
