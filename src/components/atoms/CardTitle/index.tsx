import React from "react";
import CardTitleWrapper from "./styled";

interface iCartTitleProps {
  children?: object;
  isOutlined?: boolean;
  imgSrc?: string;
  className?: string;
  isPassed?: boolean;
  isCanceled?: boolean;
}

const CartTitle = ({
  children,
  isOutlined = false,
  imgSrc = "",
  className,
  isPassed,
  isCanceled
}: iCartTitleProps) => {
  return (
    <CardTitleWrapper
      isOutlined={isOutlined}
      imgSrc={imgSrc}
      className={className}
      isPassed={isPassed}
      isCanceled={isCanceled}
    >
      {children}
    </CardTitleWrapper>
  );
};

export default CartTitle;
