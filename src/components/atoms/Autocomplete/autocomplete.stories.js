import React from 'react';
import Autocomplete from './index';
import {ThemeProvider} from 'styled-components';
import {GlobalStyle, theme} from '../../_settings/theme';

export default {title: 'Atoms|Autocomplete'};

export const autocompleteGoogle = () => <ThemeProvider theme={theme}>
  <>
    <GlobalStyle/>
    <Autocomplete/>
  </>
</ThemeProvider>;
