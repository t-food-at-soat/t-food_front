import React, {useState} from 'react';
import {AutocompleteWrapper} from "./styled";
import {StandaloneSearchBox} from "@react-google-maps/api";
import {theme} from "../../_settings/theme";
import {ReactComponent as AddressIcon} from "../../../assets/icons/placeholder.svg";
import Input from "../Input";
import Point from "../../../ts/types/point.types";

interface IAutocompleteProps {
    onChange: (data: any) => void;
    isRequired ?: boolean;
    placeholder ?: string;
}

const Autocomplete = ({onChange, isRequired = true, placeholder = "Adresse"}: IAutocompleteProps) => {

    const [searchBox, setSearchBox] = useState(undefined);

    return (
        <AutocompleteWrapper>
            <StandaloneSearchBox
                // @ts-ignore
                onLoad={ref => setSearchBox(ref)}
                onPlacesChanged={
                    () => {
                        searchBox
                        && onChange({
                            // @ts-ignore
                            address: searchBox.getPlaces()[0].formatted_address,
                            addressCoord: new Point(
                                // @ts-ignore
                                searchBox.getPlaces()[0].geometry.location.lat(),
                                // @ts-ignore
                                searchBox.getPlaces()[0].geometry.location.lng(),
                            )
                        })
                    }
                }
            >
                <Input {...theme.light}
                       IconComponent={AddressIcon}
                       placeholder={placeholder}
                       name={"address"}
                       isRequired={isRequired}
                />
            </StandaloneSearchBox>
        </AutocompleteWrapper>

    );
};

export default Autocomplete;
