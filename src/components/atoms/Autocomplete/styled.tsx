import styled from 'styled-components';


const AutocompleteWrapper = styled.div`
    
    width: calc(100% - 2rem);
    margin: auto;
    svg {
      margin-right: 0.5rem;
      height: 2rem;
    }
`;



export {AutocompleteWrapper};