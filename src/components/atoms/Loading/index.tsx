import React from 'react';
import {ReactComponent as MascotSVG} from './masco.svg'
import {ReactComponent as Egg} from './egg.svg'
import {LoadingWrapper, LoadingBar, SecondLoadingBar, LoadingBarWrapper, BodyAnimationWrapper, EggWrapper} from './styled'

interface iMascotProps {
    type?: 'individual' | 'association'
}

const MascotLoading = () => {
    return (
        <LoadingWrapper>
            <BodyAnimationWrapper>
                <MascotSVG/>
            </BodyAnimationWrapper>
        </LoadingWrapper>
    );
};

const BarLoading = () => {
    return (
        <LoadingBarWrapper>
            <LoadingBar/>
            <SecondLoadingBar/>
        </LoadingBarWrapper>
    )
};

const EggLoading = () => {
    return (
        <EggWrapper>
            <Egg/>
        </EggWrapper>
    )
};

export {MascotLoading, BarLoading, EggLoading};
