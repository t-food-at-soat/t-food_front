import React from 'react';
import {MascotLoading, BarLoading, EggLoading} from './index';
import {ThemeProvider} from 'styled-components';
import {GlobalStyle, theme} from '../../_settings/theme';

export default { title: 'Atoms|Loading' };

export const LoadingDefault = () =>  <ThemeProvider theme={theme}>
  <>
    <GlobalStyle />
    <MascotLoading />
    <BarLoading />
  </>
</ThemeProvider>;


export const Egg = () =>  <ThemeProvider theme={theme}>
  <>
    <GlobalStyle />
    <EggLoading />
  </>
</ThemeProvider>;


