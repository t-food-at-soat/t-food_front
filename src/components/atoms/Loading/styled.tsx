import styled, {keyframes} from 'styled-components';

interface iMascotWrapperProps {
    theme: {
        colors: any
    }
}

const armAnimation = keyframes`
  0% {
    transform: translateY(0);
  }
  50%{
    transform: translateY(-3px);
  }
  100% {
    transform: translateY(0);
  }
`;


const footAnimation = keyframes`
  0% {
    transform: translateY(0);
  }
  50%{
    transform: translateY(-10px);
  }
  100% {
    transform: translateY(0);
  }
`;

const bodyAnimation = keyframes`
  0% {
    transform: translateY(0px) rotate(-5deg) ;
  }
  50%{
    transform: translateY(0px) rotate(0deg) ;
  }
  100% {
    transform: translateY(0px) rotate(-5deg) ;
  }
`;

const barAnimation = keyframes`
  0% {
    width: 0
  }
  50% {
    width: 100%;
    transform-origin: right;
  }
  100% {
    width: 0
  }
`;

const barAnimationReverse = keyframes`
  0% {
    width: 0;
  }
  50% {
    width: calc(100%);
  }
  100% {
    width: 0;
  }
`;

const roadAnimation = keyframes`
  0%{
    transform: translateX(-100px)
  }

  50% {
    transform: translateX(100%)
  }
  51% {
    transform: translateX(20%)
  }
 
  100% {
     transform: translateX(calc(-100% - 200px))
  }
`;

const mirror = keyframes`
  0%{
    transform: scaleX(1) 
  }
  25%  {
    transform:  scaleX(1)
  }
  49%  {
    transform:  scaleX(1)
  }
  50% {
    transform:  scaleX(-1) 
  }
  51% {
    transform: scaleX(-1) 
  }
  100% {
     transform: scaleX(-1) 
  }
`;

const BodyAnimationWrapper = styled.div`    
   animation: ${mirror} 48s linear infinite;
`;

const LoadingWrapper = styled.div<iMascotWrapperProps>`
  z-index:-100;
  position: absolute;
  width: 100%;
  bottom: -8px;
   animation: ${roadAnimation} 48s linear infinite;
   svg {
       height: 75px;
       animation: ${bodyAnimation} 1s ease infinite;
       
       #lfoot {
          animation: ${footAnimation} 1s ease infinite;
       }
       
       #rfoot {
           animation: ${footAnimation} 1s ease infinite 0.5s;
       }
       
       #larm {
          animation: ${armAnimation} 1s ease infinite;
       }
       
       #rarm {
           animation: ${armAnimation} 1s ease infinite 0.5s;
       }
   }
`;

const LoadingBar = styled.div`
   height: 5px;
   background-color: white;
   width: 0;
   animation: ${barAnimation} 24s ease-in-out infinite;
`;

const SecondLoadingBar = styled.div`
   height: 5px;
   background-color: green;
    width: 0;
   animation: ${barAnimationReverse} 24s ease-in-out infinite 12s;
`;


const LoadingBarWrapper = styled.div`   
    display: flex;
`;

const wobble = keyframes`
    0% {
        transform: rotate(0deg);
    }
    25% {
        transform: rotate(-10deg);
    }
    75% {
        transform: rotate(10deg);
    }
    100% {
        transform: rotate(0deg);
    }
`;

const EggWrapper = styled.div`   
    display: flex;  
    animation: ${wobble} 3s linear infinite;
    transform-origin: bottom;
    svg {
      height: 100%;
      width: 100%;      
    }
`;




export {LoadingWrapper, LoadingBar, SecondLoadingBar, LoadingBarWrapper, BodyAnimationWrapper, EggWrapper};