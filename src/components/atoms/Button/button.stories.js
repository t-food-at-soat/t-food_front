import React from 'react';
import Button from './index';
import {ThemeProvider} from 'styled-components';
import {GlobalStyle, theme} from '../../_settings/theme';
import {ReactComponent as FilterIcon} from '../../../assets/icons/filter.svg'
import Title from '../Title'


export default {
  title: 'Atoms|Button',
  includeStories: ['withLightTheme',
                  'withDarkTheme',
                  'withLightThemeSmall',
                  'withLightThemeBig',
                  'withLightThemeBigNoBoxShadow',
                  'withLightThemeNotClickable',
                  'withLightThemeColoredIcon',
  ],
};


export const withLightThemeSmall = () =>  <ThemeProvider theme={theme}>
<div style={{padding:"10px", backgroundColor:theme.light.backgroundColor}}>
  <GlobalStyle />
  <Button icon={FilterIcon} size="small" {...theme.light} onClick={ () => console.log("allo")} >
    <Title><span>Manger</span> c’est cool, <span>gâcher</span> c’est les boules</Title>
  </Button>
  <br/>
</div>
</ThemeProvider>;

export const withLightTheme = () =>  <ThemeProvider theme={theme}>
<div style={{padding:"10px", backgroundColor:theme.light.backgroundColor}}>
  <GlobalStyle />
  <Button icon={FilterIcon} {...theme.light} onClick={ () => console.log("allo")} >
    toto
  </Button>
</div>
</ThemeProvider>;

export const withLightThemeBig = () =>  <ThemeProvider theme={theme}>
<div style={{padding:"10px", backgroundColor:theme.light.backgroundColor}}>
  <GlobalStyle />
  <Button icon={FilterIcon} size="big" {...theme.light}  onClick={ () => console.log("allo")}>
    <Title><span>Manger</span> c’est cool, <span>gâcher</span> c’est les boules</Title>
  </Button>
</div>
</ThemeProvider>;

export const withLightThemeBigNoBoxShadow = () =>  <ThemeProvider theme={theme}>
<div style={{padding:"10px", backgroundColor:theme.light.backgroundColor}}>
  <GlobalStyle />
  <Button icon={FilterIcon} isBoxShadowDisplayed={false} size="big" {...theme.light} onClick={ () => console.log("allo")} >
    <Title><span>Manger</span> c’est cool, <span>gâcher</span> c’est les boules</Title>
  </Button>
</div>
</ThemeProvider>;

export const withDarkTheme = () =>  <ThemeProvider theme={theme}>
  <div style={{padding:"10px", backgroundColor:theme.dark.backgroundColor}}>
    <GlobalStyle />
    <Button {...theme.dark} onClick={() => console.log("pipou")}>
      <Title dark={true}><span>Manger</span> c’est cool, <span>gâcher</span> c’est les boules</Title>
    </Button>
  </div>
</ThemeProvider>;

export const withLightThemeNotClickable = () =>  <ThemeProvider theme={theme}>
<div style={{padding:"10px", backgroundColor:theme.light.backgroundColor}}>
  <GlobalStyle />
  <Button icon={FilterIcon} isNotClickable={true} {...theme.light} >
    <Title><span>Manger</span> c’est cool, <span>gâcher</span> c’est les boules</Title>
  </Button>
</div>
</ThemeProvider>;

export const withLightThemeColoredIcon = () =>  <ThemeProvider theme={theme}>
<div style={{padding:"10px", backgroundColor:theme.light.backgroundColor}}>
  <GlobalStyle />
  <Button icon={FilterIcon} iconColor="red" {...theme.light} onClick={ () => console.log("allo")} >
    <Title><span>Manger</span> c’est cool, <span>gâcher</span> c’est les boules</Title>
  </Button>
  <br/>
</div>
</ThemeProvider>;

