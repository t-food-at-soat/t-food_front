import styled from 'styled-components';


const getHoverBoxShadow = (props) => {
  if (props.isBoxShadowDisplayed === false)
    return "none"
  return props.isNotClickable ? props.theme.boxShadow : props.hoverBoxShadow
}
const StyledButton = styled.button`
  ${props => props.theme.roundBox(props)};
  justify-content: center;
  align-items: center;
  svg {
    fill: ${props => props.iconColor ? props.iconColor : "currentColor"};
  }

  &:hover {
    background-color:${props => props.isNotClickable ? props.theme.backgroundColor : props.hoverBackgroundColor};
    box-shadow:   ${props => getHoverBoxShadow(props)};
    transition: all 0.5s ease;
  }
  &:active {
    background-color:${props => props.isNotClickable ? props.theme.backgroundColor : props.hoverBackgroundColor};
    box-shadow: ${props => props.isNotClickable ? props.theme.boxShadow : "none"};
    position: relative;
    top: ${props => props.isNotClickable ? "0rem" : "0.1rem"};
    transition: all 0.150s ease;
  }
`;

export { StyledButton };

