import React from "react";
import { StyledButton } from "./styled";

interface IButtonProps {
  color: string;
  backgroundColor: string;
  boxShadow: string;
  isBoxShadowDisplayed?: boolean;
  border: string;
  hoverBackgroundColor: string;
  hoverBoxShadow: string;
  children: JSX.Element | string;
  isNotClickable?: boolean;
  icon?: React.FunctionComponent<React.SVGProps<SVGSVGElement>>;
  iconColor?: string;
  size?: "small" | "medium" | "big";
  onClick?: () => void;
}

const Button = ({ children, onClick = () => {}, icon, ...props }: IButtonProps) => {
  const handleClick = (
    _event: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    onClick();
  };
  const Icon = icon;
  return (
    <StyledButton
      {...props}
      onClick={event =>
        props.isNotClickable ? event.preventDefault : handleClick(event)
      }
    >
      {children}
      {Icon ? (
        <Icon
          style={{ width: "1rem", paddingLeft: "1rem", verticalAlign: "middle" }}
        />
      ) : null}
    </StyledButton>
  );
};

export default Button;
