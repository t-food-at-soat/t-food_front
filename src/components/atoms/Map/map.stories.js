import React from 'react';
import Map from './index';
import {ThemeProvider} from 'styled-components';
import {GlobalStyle, theme} from '../../_settings/theme';

export default {title: 'Atoms|Map'};

export const DefaultMap = () => <ThemeProvider theme={theme}>
  <>
    <GlobalStyle/>
    <div style={{height:"500px"}}>
      <Map/>
    </div>
  </>
</ThemeProvider>;