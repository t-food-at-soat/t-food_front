import React, { useEffect, useRef, useState } from "react";
import {
  DirectionsRenderer,
  DirectionsService,
  GoogleMap
} from "@react-google-maps/api";
import { ILoggedInPayload } from "../../../store/authentication/types";
import { IRootState } from "../../../store/combiners";
import { connect } from "react-redux";

interface IMap {
  handleDirectionDuration?: (duration: any) => void;
  destination: string;
}

function usePrevious(value: any) {
  // The ref object is a generic container whose current property is mutable ...
  // ... and can hold any value, similar to an instance property on a class
  const ref = useRef();

  // Store current value in ref
  useEffect(() => {
    ref.current = value;
  }, [value]); // Only re-run if value changes

  // Return previous value (happens before update in useEffect above)
  return ref.current;
}

const Map = ({
  userConnected,
  handleDirectionDuration,
  destination
}: IMap & IPropsState) => {
  const directionsCallback = (response: any) => {
    setResponseDirection(response);
    if (response && response.routes[0]) {
      handleDirectionDuration &&
        handleDirectionDuration({
          text: response.routes[0].legs[0].duration.text,
          value: response.routes[0].legs[0].duration.value
        });
    } else {
      handleDirectionDuration && handleDirectionDuration("Bon Voyage !");
    }
  };

  const [responseDirection, setResponseDirection] = useState(undefined);
  const [isDirectionReady, setIsDirectionReady] = useState(false);

  const prevAddress = usePrevious(userConnected ? userConnected.address : "");

  useEffect(() => {
    if (responseDirection) {
      setIsDirectionReady(true);
    }

    if (userConnected && prevAddress !== userConnected.address) {
      setResponseDirection(undefined);
      setIsDirectionReady(false);
    }
    return;
  }, [responseDirection, userConnected]);

  return (
    <GoogleMap
      // required
      id="direction-example"
      // required
      mapContainerStyle={{
        height: "100%",
        width: "100%"
      }}
      // required
      zoom={2}
      // required
      center={{
        lat: 0,
        lng: -180
      }}
    >
      {!responseDirection && (
        <DirectionsService
          options={{
            destination: destination,
            origin: userConnected ? userConnected.address : "",
            // @ts-ignore
            travelMode: "TRANSIT"
          }}
          callback={directionsCallback}
        />
      )}

      {isDirectionReady && (
        <DirectionsRenderer
          options={{
            directions: responseDirection
          }}
        />
      )}
    </GoogleMap>
  );
};

interface IPropsState {
  userConnected: ILoggedInPayload | undefined;
}

const mapStateToProps = ({ authentication }: IRootState): IPropsState => ({
  userConnected: authentication.userConnected
});

export default connect(mapStateToProps)(Map);
