import styled from 'styled-components';

const TextWrapper = styled.div`
width:100%;
font-family: hiragino,serif;
font-size: 1.6rem;
color:#4F4F4F;
line-height: ${props => props.lineHeigh ? props.lineHeigh : "2rem"};
  
`;
    


export default TextWrapper;