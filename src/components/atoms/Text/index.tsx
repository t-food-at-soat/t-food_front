import React from 'react';
import TextWrapper from './styled'

interface iTextProps {
    lineHeigh?: string;
    children: JSX.Element | string;
}

const Text = ({children, ...props} : iTextProps) => {
    return (
        <TextWrapper {...props}>
            {children}
        </TextWrapper>
    );
};

export default Text;
