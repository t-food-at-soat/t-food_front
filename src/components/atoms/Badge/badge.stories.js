import React from 'react';
import Badge from './index';
import {ThemeProvider} from 'styled-components';
import {GlobalStyle, theme} from '../../_settings/theme';

export default {title: 'Atoms|Badge'};

export const badgeWithText = () => <ThemeProvider theme={theme}>
  <>
    <GlobalStyle/>
    <Badge>
      <span>Pizza</span>
    </Badge>
    <br/>
    <br/>
    <Badge>
      <span>21: 00</span>
    </Badge>
    <br/>
    <br/>
    <Badge>
      <span>à&nbsp;&nbsp;20 min</span>
    </Badge>
  </>
</ThemeProvider>;

export const badgeWithLogo = () => <ThemeProvider theme={theme}>
  <>
    <GlobalStyle/>
    <Badge isOutlined imgSrc={"https://pbs.twimg.com/profile_images/1169992166762004480/hyUmIVN__400x400.jpg"}/>
  </>
</ThemeProvider>;