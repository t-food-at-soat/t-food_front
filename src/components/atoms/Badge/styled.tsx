import styled from 'styled-components';

interface iBadgeWrapperProps {
    isOutlined ?: boolean,
    imgSrc ?: string
}



const BadgeWrapper = styled.label<iBadgeWrapperProps>`
  font-family: Milkshake,serif;
  width: 10rem;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 3rem;
  padding: 0.3rem 0;
  background: ${props => props.isOutlined ? `${props.theme.colors.lightgrey} url(${props.imgSrc}) no-repeat center` : props.theme.colors.lightgrey};
  background-size: contain;
  border: ${props => props.isOutlined ? `0.2rem solid ${props.theme.colors.lightgrey}` : `0.2rem solid  ${props.theme.colors.lightgrey}`};
  
  span {
    font-family: Milkshake,serif;
    font-size: 1.6rem;
    color: ${props => props.theme.colors.darkgrey};
  }
`;


export default BadgeWrapper;