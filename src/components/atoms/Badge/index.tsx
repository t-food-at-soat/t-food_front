import React from 'react';
import BadgeWrapper from './styled'

interface iBadgeProps {
    children?: object,
    isOutlined ?: boolean,
    imgSrc ?: string
}

const Badge = ({children, isOutlined = false, imgSrc = ""} : iBadgeProps) => {
    return (
        <BadgeWrapper isOutlined={isOutlined} imgSrc={imgSrc}>
            {children}
        </BadgeWrapper>
    );
};

export default Badge;
