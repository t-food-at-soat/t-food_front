import React from "react";
import { SelectStyled, OptionStyled } from "./styled.js";

export interface IOption {
  id: number;
  value: string;
}

interface ISelectProps {
  label: string;
  name?: string;
  value: string;
  options: IOption[];
  onChange: (data: any) => void;
}

const Select = ({
  label,
  options,
  name,
  value,
  onChange = () => {},
  ...props
}: ISelectProps) => {
  const handleChange = (selected: any) => {
    onChange(selected);
  };
  return (
    <SelectStyled
      onChange={e => handleChange(e.target.value)}
      {...props}
      name={name}
      defaultValue={value}
    >
      <OptionStyled disabled>{label}</OptionStyled>
      {options.map((option: IOption) => (
        <OptionStyled key={option.id} value={option.value}>
          {option.value}
        </OptionStyled>
      ))}
    </SelectStyled>
  );
};

export default Select;
