import styled from "styled-components";

const SelectStyled = styled.select`
  ${props => props.theme.roundBox(props)};
  height: 3.5rem;
  padding-left: 1rem;
`;

const OptionStyled = styled.option``;

export { SelectStyled, OptionStyled };
