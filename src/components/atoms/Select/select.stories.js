import React from "react";
import Select from "./index";
import { ThemeProvider } from "styled-components";
import { GlobalStyle, theme } from "../../_settings/theme";

export default { title: "Atoms|Select" };

const options = [
  { id: 1, value: "option1" },
  { id: 2, value: "option2" },
  { id: 3, value: "option3" }
];

const handleChange = data => {
  console.log(data);
};

export const mySelect = () => (
  <ThemeProvider theme={theme}>
    <>
      <GlobalStyle />
      <Select
        label={"catégorie"}
        options={options}
        onChange={handleChange}
        {...theme.light}
      />
    </>
  </ThemeProvider>
);
