import styled from 'styled-components';


interface iLogoWrapperProps {
    dark ?: boolean
    theme: {
        colors : any
    }
}

const LogoWrapper = styled.span<iLogoWrapperProps>`
  font-family: This,serif;
  user-select: none;
  cursor:pointer;
  font-size : 4.9rem;
  color: ${props => props.dark ? props.theme.colors.white : props.theme.colors.green};
  span {
    color: ${props => props.dark ? props.theme.colors.white : props.theme.colors.darkgrey};;
  }
`;



export { LogoWrapper };