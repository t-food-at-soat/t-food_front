import React from 'react';
import Logo from './index';
import {ThemeProvider} from 'styled-components';
import {GlobalStyle, theme} from '../../_settings/theme';

export default {title: 'Atoms|Logo'};

export const HeaderLogo = () => <ThemeProvider theme={theme}>
  <>
    <GlobalStyle/>
    <Logo content={"food"}/>
  </>
</ThemeProvider>;

export const TrustLogo = () => <ThemeProvider theme={theme}>
  <>
    <GlobalStyle/>
    <Logo content={"trust"}/>
  </>
</ThemeProvider>;

export const TrustSample = () => <ThemeProvider theme={theme}>
  <>
    <GlobalStyle/>
    <Logo content={"sample"}/>
  </>
</ThemeProvider>;

export const DarkLogo = () => <ThemeProvider theme={theme}>
  <>
    <GlobalStyle/>
    <div style={{background: "#4F4F4F", padding: "15px"}}>
      <Logo dark={true} content={"food"}/>
    </div>
  </>
</ThemeProvider>;