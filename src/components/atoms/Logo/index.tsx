import React from 'react';
import {LogoWrapper} from "./styled";

interface iLogoProps {
    content : string,
    dark ?: boolean
}

const Logo = ({content, dark} : iLogoProps) => {
    return (
        <LogoWrapper dark={dark}>
            <span>T</span>
            {content}
        </LogoWrapper>
    );
};

export default Logo;
