import styled from 'styled-components';
import React from "react";

interface iInputWrapperProps {
    border: string;
    backgroundColor: string;
    isBoxShadowDisplayed?: boolean;
    boxShadow: string;
    size: string;
    placeholder?: string;
    type?: string;
    label?: string;
    value?: string;
    onChange?: (event: React.FormEvent<HTMLInputElement>) => void;
    onClick?: (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
    isInvalid ?: boolean
}

const InputWrapper = styled.div<any>`
  ${props => props.theme.roundBox(props)};
  padding-left: 1.5rem;
  display: flex;
  align-items: center;
  box-shadow: ${props =>
    props.isBoxShadowDisplayed
        ? `none`
        : `.1rem .1rem`};
  
  input {
    display: flex;
    outline: none;
    border: none;
    font-family: hiragino,serif;
    font-size: 1.5rem;
    margin-left: 1.5rem;
    flex: 1;
    color: ${props => props.theme.colors.darkgrey};
  }
  input[type=checkbox]{
    flex: 0;
  }
  input::placeholder{
    color: ${props => props.theme.colors.lightgrey};
  }
  label {
    padding: 0 1.5rem;
    font-size: 1.5rem;
    color: #f44336;
  }
  svg {
    height: 1.5rem;
    fill : ${props => props.theme.colors.darkgrey}
  }
`;


export {InputWrapper};