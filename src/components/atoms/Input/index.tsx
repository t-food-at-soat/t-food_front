import React, { useEffect, useState } from "react";
import { InputWrapper } from "./styled";
import { ReactComponent as ErrorIcon } from "../../../assets/icons/error.svg";
import { ReactComponent as RequiredIcon } from "../../../assets/icons/asterisk.svg";
import Offer from "../../../ts/interfaces/offer.interfaces";
import { IRootState } from "../../../store/combiners";
import { Dispatch } from "redux";
import { OfferAction } from "../../../store/offer/types";
import { getUserOffer, setOfferIsLoad } from "../../../store/offer/actions";
import { connect } from "react-redux";
import {
  uploadPicture,
  uploadPictureSetLoad
} from "../../../store/uploadPicture/actions";
import { UploadPictureAction } from "../../../store/uploadPicture/types";
import { setProfileIsLoad } from "../../../store/profile/actions";
import { serverURL } from "../../../store/config/constant";

interface iInputProps {
  IconComponent?: React.FunctionComponent<React.SVGProps<SVGSVGElement>>;
  placeholder?: string;
  type?: string;
  label?: string;
  value?: string;
  name: string;
  isBoxShadowDisplayed?: boolean;
  onChange?: (data: any) => void;
  onClick?: (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
  isInvalid?: boolean;
  validation?: string;
  isRequired?: boolean;
  disabled?: boolean;
  onKeyPress?: any;
  defaultValue?: string;
  checked?: boolean;
}

const Input = ({
  IconComponent,
  placeholder,
  type = "text",
  label,
  value,
  name,
  onChange = () => {},
  onClick,
  isInvalid,
  validation,
  isRequired,
  onKeyPress,
  disabled,
  defaultValue,
  checked,
  uploadPicture,
  isPictureLoad,
  loadFilePath,
  setPictureUpload,
  ...props
}: iInputProps & IPropsDispatch & IPropsState) => {
  const [isValid, setIsValid] = useState(!isRequired);
  const [validError, setValidError] = useState("");

  const handleOnChange = (
    event: React.ChangeEvent<HTMLInputElement>,
    type: string
  ) => {
    switch (type) {
      case "checkbox":
        onChange({
          [event.target.name]: event.target.value,
          checked: event.target.checked
        });
        break;
      case "file":
        if (event.target.files) {
          uploadPicture(event.target.files[0]);
        }
        break;
      default:
        onChange({ [event.target.name]: event.target.value });
        break;
    }
  };

  const validateInput = (value: string) => {
    let isValid = true;
    let errMessage = "";

    switch (validation) {
      case "email":
        if (!value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
          isValid = false;
          errMessage = "Email invalide";
        }

        if (value.length <= 0) {
          isValid = false;
          errMessage = "Email requis";
        }

        setIsValid(isValid);
        setValidError(errMessage);

        break;
      case "password":
        if (value.length >= 6) {
          setIsValid(true);
          setValidError("");
        } else {
          setIsValid(false);
          setValidError("Mot de passe trop court");
        }
        break;
      default:
        if (isRequired && value.length <= 0) {
          isValid = false;
          errMessage = "Champs requis";
        }
        setIsValid(isValid);
        setValidError(errMessage);
        break;
    }
  };

  useEffect(() => {
    if (value && value.length > 0) {
      validateInput(value);
    }
    return () => {
      setPictureUpload(false);
    };
  }, []);

  useEffect(() => {
    if (type === "file" && isPictureLoad && loadFilePath) {
      onChange({
        picture: `${serverURL}${loadFilePath}`
      });
    }
    return () => {
      setPictureUpload(false);
    };
  }, [loadFilePath]);

  return (
    <InputWrapper {...props} onClick={onClick}>
      {IconComponent && !validError && <IconComponent />}
      {validError && <ErrorIcon />}
      <input
        checked={checked}
        data-valid={isValid}
        name={name}
        placeholder={placeholder}
        type={type}
        value={value}
        onChange={event => handleOnChange(event, type)}
        onBlur={event => validateInput(event.target.value)}
        onKeyPress={onKeyPress}
        disabled={disabled}
      />
      {type === "file" && isPictureLoad && loadFilePath === "" && "LOADING"}
      <label style={{ color: "black" }}>{label}</label>
      {validError && <label>{validError}</label>}
      {isRequired && <RequiredIcon />}
    </InputWrapper>
  );
};

interface IPropsState {
  loadFilePath: string;
  isPictureLoad: boolean;
  errorMessage: string;
}

interface IPropsDispatch {
  uploadPicture: (filePath: File) => void;
  setPictureUpload: (status: boolean) => void;
}

const mapStateToProps = ({ uploadPicture }: IRootState): IPropsState => ({
  loadFilePath: uploadPicture.loadFilePath,
  isPictureLoad: uploadPicture.isPictureLoad,
  errorMessage: uploadPicture.errorMessage
});

const mapDispatchToProps = (
  dispatch: Dispatch<UploadPictureAction>
): IPropsDispatch => ({
  uploadPicture: (filePath: File) => dispatch(uploadPicture(filePath)),
  setPictureUpload: (status: boolean) => dispatch(uploadPictureSetLoad(status))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Input);
