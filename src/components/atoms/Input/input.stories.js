import React from 'react';
import Input from './index';
import {ThemeProvider} from 'styled-components';
import {GlobalStyle, theme} from '../../_settings/theme';
import {ReactComponent as MailIcon} from '../../../assets/icons/email.svg'
import {ReactComponent as LockIcon} from '../../../assets/icons/lock.svg'

export default {title: 'Atoms|Input'};

export const TextInput = () => <ThemeProvider theme={theme}>
  <>
    <GlobalStyle/>
    <Input {...theme.light} IconComponent={MailIcon} placeholder={"email"}/>
    <Input {...theme.light} IconComponent={LockIcon} placeholder={"mot de passe"} />
  </>
</ThemeProvider>;

  export const CheckboxInput = () => <ThemeProvider theme={theme}>
  <>
    <GlobalStyle/>
    <Input {...theme.light} type={"checkbox"} label={"Je suis un super label de checkbox"}/>
  </>
</ThemeProvider>;

