import React, { useState } from "react";
import { ReactComponent as PinIcon } from "../../../assets/icons/003-pin.svg";
import { AddressInputWrapper } from "./styled";
import { IRootState } from "../../../store/combiners";
import {
  AuthenticationAction,
  ILoggedInPayload
} from "../../../store/authentication/types";
import { connect } from "react-redux";
import { StandaloneSearchBox } from "@react-google-maps/api";
import { Dispatch } from "redux";
import { loggedIn } from "../../../store/authentication/actions";

const AddressInput = ({
  userConnected,
  setUserConnected
}: IPropsState & IPropsDispatch) => {
  const [searchBox, setSearchBox] = useState(undefined);

  const handleOnBlur = () => {
    //TODO
    // const firstSearchBoxResult = document.getElementsByClassName('pac-item')[0];
    // console.log(firstSearchBoxResult);
    // const spanArray = firstSearchBoxResult.getElementsByTagName("span");
    // console.log(`${spanArray[1].textContent} ${spanArray[spanArray.length - 1].textContent}`);
  };

  return (
    <AddressInputWrapper>
      <PinIcon />
      <StandaloneSearchBox
        // @ts-ignore
        onLoad={ref => setSearchBox(ref)}
        onPlacesChanged={() => {
          console.log(searchBox);
          userConnected &&
            searchBox &&
            setUserConnected({
              id: userConnected.id,
              userName: userConnected.userName,
              jwt: userConnected.jwt,
              picture: userConnected.picture,
              roles: userConnected.roles,
              // @ts-ignore
              address: searchBox.getPlaces()[0].formatted_address
            });
        }}
      >
        <input
          type={"text"}
          defaultValue={
            userConnected ? userConnected.address : "Aucune adresse"
          }
          onBlur={handleOnBlur}
        />
      </StandaloneSearchBox>
    </AddressInputWrapper>
  );
};

interface IPropsState {
  userConnected: ILoggedInPayload | undefined;
}

const mapStateToProps = ({ authentication }: IRootState): IPropsState => ({
  userConnected: authentication.userConnected
});

interface IPropsDispatch {
  setUserConnected: (userConnectedPayload: ILoggedInPayload) => void;
}

const mapDispatchToProps = (
  dispatch: Dispatch<AuthenticationAction>
): IPropsDispatch => ({
  setUserConnected: (userConnectedPayload: ILoggedInPayload) =>
    dispatch(loggedIn(userConnectedPayload))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddressInput);
