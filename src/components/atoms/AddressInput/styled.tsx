import styled from "styled-components";

export const AddressInputWrapper = styled.div`
      display: flex;
      justify-content: center;
      align-items: center;
    
    svg {
      margin-left: 2.5rem;
      margin-right: 0.5rem;
    }
    
    input {
      border: none;
      font-family: "Hiragino",serif;
      font-size: 1.5rem;
      color: ${props => props.theme.colors.darkgrey};
      text-overflow: ellipsis;
      white-space: nowrap;
      overflow: hidden;
      min-width: 28rem;
      outline: none;
    }
`;