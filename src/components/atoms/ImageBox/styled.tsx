import styled from 'styled-components';

interface ImageBoxWrapper {
    sourceImage ?: string,
    shadowOff ?: boolean
}

const ImageBoxWrapper = styled.div<ImageBoxWrapper>`
  // background: url(${props => props.sourceImage}) no-repeat;
  // background-size : cover;
  box-shadow:  ${props => props.shadowOff ? "none" : "5px 5px 0px 0px" + props.theme.colors.green};
  height: 17rem;
  width: 100%;
  
  img {
    height: 17rem;
    width: 100%;
    object-fit: cover;
  }
`;



export { ImageBoxWrapper };