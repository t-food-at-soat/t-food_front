import React from 'react';
import {ImageBoxWrapper} from "./styled";

interface iImageBoxProps {
    sourceImage : any,
    title: string,
    shadowOff ?: boolean
    className ?: string
}

const ImageBox = ({sourceImage, shadowOff, className} : iImageBoxProps) => {
    return (
        <ImageBoxWrapper>
          <img src={sourceImage} alt={"alt"}/>
        </ImageBoxWrapper>
    );
};

//<ImageBoxWrapper sourceImage={sourceImage} shadowOff={shadowOff} className={className}/>

export default ImageBox;
