import styled from "styled-components";

const ProfilEditorOrganismWrapper = styled.div`
  display: flex;
  justify-content: space-evenly;
  background: url(${props => props.pictureUrl}) no-repeat;
  background-size: cover;
  align-items: center;
  
  @media (max-width: 768px) {
    svg {
      display: none;
    }
  }
`;

export { ProfilEditorOrganismWrapper };
