import React from "react";
import { ProfilEditorOrganismWrapper } from "./styled";
import Mascot from "../../atoms/Mascot";
import ProfileEditor from "../../molecules/ProfilEditor";

interface ProfilEditorOrganismProps {
  onClick: (profil: any) => void;
  pictureUrl: string;
}

const ProfilEditorOrganism = ({ ...props }: ProfilEditorOrganismProps) => {
  return (
    <ProfilEditorOrganismWrapper {...props}>
      <Mascot />
      <ProfileEditor/>
    </ProfilEditorOrganismWrapper>
  );
};

export default ProfilEditorOrganism;
