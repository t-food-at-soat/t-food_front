import React from "react";
import { ThemeProvider } from "styled-components";
import { GlobalStyle, theme } from "../../_settings/theme";
import ProfilEditorOrganism from "./index";

export default {
  title: "Organisms|ProfilEditor",
  includeStories: ["standard"]
};

export const standard = () => (
  <ThemeProvider theme={theme}>
    <div>
      <GlobalStyle />
      <ProfilEditorOrganism
        onClick={profile => {
          console.log(profile);
        }}
        pictureUrl="https://cdn.pixabay.com/photo/2016/06/17/06/04/background-1462755_960_720.jpg"
      ></ProfilEditorOrganism>
    </div>
  </ThemeProvider>
);
