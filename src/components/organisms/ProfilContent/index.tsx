import React, { useEffect } from "react";
import { ProfilContentWrapper, TitleWrapper, CardListWrapper } from "./styled";
import CardOfferList from "../CardOfferList";
import { theme } from "../../_settings/theme";
import Title from "../../atoms/Title";
import ProfilEditorOrganism from "../ProfilEditorOrganism";
import Offer from "../../../ts/interfaces/offer.interfaces";
import { IRootState } from "../../../store/combiners";
import { Dispatch } from "redux";
import { OfferAction } from "../../../store/offer/types";
import {
  getAllReservation,
  setOfferIsLoad
} from "../../../store/offer/actions";
import { connect } from "react-redux";
import { LoadingWrapper } from "../userHome/styled";
import { EggLoading } from "../../atoms/Loading";

const ProfilContent = ({
  offerList,
  getAllReservation,
  isOfferListLoad,
  setOfferListLoad
}: IPropsState & IPropsDispatch) => {
  useEffect(() => {
    getAllReservation();
    return () => {
      setOfferListLoad(false);
    };
  }, [getAllReservation]);

  return (
    <ProfilContentWrapper>
      <TitleWrapper>
        <Title {...theme.light} fontSize={"4.9rem"}>
          Mes{" "}
          <span>
            <span>T</span>informations
          </span>
        </Title>
      </TitleWrapper>

      <ProfilEditorOrganism
        onClick={profile => {
          console.log(profile);
        }}
        pictureUrl="https://cdn.pixabay.com/photo/2016/06/17/06/04/background-1462755_960_720.jpg"
      />
      <TitleWrapper>
        <Title {...theme.light} fontSize={"4.9rem"}>
          Mon{" "}
          <span>
            <span>T</span>historique
          </span>
        </Title>
      </TitleWrapper>
      <CardListWrapper>
        {isOfferListLoad ? (
          <CardOfferList offerList={offerList} onClick={data => {}} />
        ) : (
          <LoadingWrapper>
            <EggLoading />
            <Title font={"Milkshake"} fontSize={"3rem"}>
              <span>Chargement ...</span>
            </Title>
          </LoadingWrapper>
        )}
      </CardListWrapper>
    </ProfilContentWrapper>
  );
};

interface IPropsState {
  offerList: Array<Offer>;
  isOfferListLoad: boolean;
}

interface IPropsDispatch {
  getAllReservation: () => void;
  setOfferListLoad: (state: boolean) => void;
}

const mapStateToProps = ({ offer }: IRootState): IPropsState => ({
  offerList: offer.offerList,
  isOfferListLoad: offer.isLoad
});

const mapDispatchToProps = (
  dispatch: Dispatch<OfferAction>
): IPropsDispatch => ({
  getAllReservation: () => dispatch(getAllReservation()),
  setOfferListLoad: (payload: boolean) => dispatch(setOfferIsLoad(payload))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfilContent);
