import styled from "styled-components";

const ProfilContentWrapper = styled.div`
  display: grid;
  text-align: center;
`;

const TitleWrapper = styled.div`
  justify-self: center;
`;

const CardListWrapper = styled.div`
    text-align: center;
    width: 100%;
    margin: auto auto 5rem;
    max-width: 960px;
`;

export { ProfilContentWrapper, TitleWrapper, CardListWrapper };
