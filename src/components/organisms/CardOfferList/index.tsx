import React, { useState, useEffect } from "react";
import { CardOfferListWrapper, CardWrapper } from "./styled";
import CardOffer from "../../molecules/CardOffer";
import FilterBar, { IFilterData } from "../../molecules/FilterBar";
import { theme } from "../../_settings/theme";
import Offer from "../../../ts/interfaces/offer.interfaces";
import OrganismNoOffer from "../organismNoOffer";
import { navigate } from "hookrouter";

interface CardOfferListProps {
  onClick: (offer: Offer) => void;
  offerList: Array<Offer>;
}

const CardOfferList = ({ onClick, offerList }: CardOfferListProps) => {
  const [filter, setFilter] = useState<IFilterData>({
    types: [],
    quantities: [],
    distances: []
  });
  const [filteredList, setFilteredList] = useState(offerList);

  const handleFilterChange = (data: any) => {
    setFilter(data);
  };

  const filterOfferList = () => {
    let offerListFiltred: Array<Offer> = offerList;
    if (filter.types.length) {
      offerListFiltred = offerListFiltred.filter(offer =>
        filter.types.includes(offer.type)
      );
    }
    if (filter.quantities.length) {
      offerListFiltred = offerListFiltred.filter(
        offer =>
          filter.quantities.includes(offer.quantity.toString()) ||
          (filter.quantities.includes((4).toString()) && offer.quantity >= 4)
      );
    }

    if (filter.distances.length && filter.distances.includes((1).toString())) {
      offerListFiltred = offerListFiltred.filter(
        offer => offer.duration || offer.duration === 0
      );

      offerListFiltred.sort((offerA, offerB) => {
        const durationA = offerA.duration ? offerA.duration : 0;
        const durationB = offerB.duration ? offerB.duration : 0;
        let comparaison = 0;

        if (durationA > durationB) {
          comparaison = 1;
        } else if (durationA < durationB) {
          comparaison = -1;
        }

        return comparaison;
      });
    }
    return offerListFiltred;
  };

  useEffect(() => {
    setFilteredList(filterOfferList());
    return;
  }, [filter, offerList]);
  return (
    <CardOfferListWrapper>
      <FilterBar
        {...theme.light}
        onFilterChange={handleFilterChange}
        filter={filter}
      />
      {filteredList.length > 0 ? (
        <CardWrapper>
          {filteredList.map(offer => (
            <CardOffer
              key={offer.id}
              offer={offer}
              onClick={() => navigate(`/offer/${offer.id}`)}
            />
          ))}
        </CardWrapper>
      ) : (
        <OrganismNoOffer />
      )}
    </CardOfferListWrapper>
  );
};

export default CardOfferList;
