import styled from "styled-components";

const CardOfferListWrapper = styled.div`
  display: grid;
  width: 100%;
`;
const CardWrapper = styled.div`
  margin-top: 1rem;
  display: grid;
  grid-template-columns: repeat(3, 1fr);
  @media (max-width: 768px) {
    grid-template-columns: repeat(2, 1fr);
  }
  @media (max-width: 400px) {
    grid-template-columns: repeat(1, 1fr);
  }
  grid-gap: 1rem;
  
  
  @media (max-width: 768px) {    
    margin-top: 10rem;
  }
`;

export { CardOfferListWrapper, CardWrapper };
