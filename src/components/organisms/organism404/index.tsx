import React from "react";
import { Organism404Wrapper } from "./styled";
import { ReactComponent as TfoodNest } from "./404.svg";
import Title from "../../atoms/Title";
interface IOrganism404Props {}

const Organism404 = ({ ...props }: IOrganism404Props) => {
  return (
    <Organism404Wrapper>
      <Title fontSize="3.5rem">
        Ici c'est le <span style={{ fontFamily: "milkshake" }}>nid</span> de{" "}
        <span>
          <span>T</span>food
        </span>{" "}
        le{" "}
        <span>
          <span>T</span>rex
        </span>
        ,<br />
        il n'y a rien à <span style={{ fontFamily: "milkshake" }}>
          manger
        </span>{" "}
        ...
      </Title>
      <TfoodNest></TfoodNest>
    </Organism404Wrapper>
  );
};

export default Organism404;
