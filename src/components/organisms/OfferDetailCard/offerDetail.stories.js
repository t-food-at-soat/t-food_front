import React from 'react';
import OfferDetail from './index';
import {ThemeProvider} from 'styled-components';
import {GlobalStyle, theme} from '../../_settings/theme';

export default {title: 'Organisms|OfferDetail'};

export const OfferDetailDefault = () => <ThemeProvider theme={theme}>
  <>
    <GlobalStyle/>
    {/*<div style={{width:"900px",height:"400px"}}>*/}
      <OfferDetail />
    {/*</div>*/}
  </>
</ThemeProvider>;
