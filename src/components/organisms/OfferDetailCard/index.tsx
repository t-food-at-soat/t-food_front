import React, { useState } from "react";
import {
  OfferDetailWrapper,
  BadgeContainer,
  DetailCtrlWrapper,
  DetailCtrlContainer,
  DetailCtrlDivider
} from "./styled";
import ImageSwapMap from "../../atoms/ImageSwapMap";
import CartTitle from "../../atoms/CardTitle";
import ReservationBox from "../../molecules/ReservationBox";
import Badge from "../../atoms/Badge";
import Offer from "../../../ts/interfaces/offer.interfaces";
import moment from "moment-timezone";

interface IOfferDetailCardProps {
  offer: Offer;
  currentIndex: number;
  maxIndex: number;
  handleCtrl: (index: number) => void;
}

const OfferDetailCard = ({
  offer,
  currentIndex,
  maxIndex,
  handleCtrl
}: IOfferDetailCardProps) => {
  const [travelDuration, setTravelDuration] = useState("Calcul ...");
  const handleDirectionDuration = (duration: any) => {
    setTravelDuration(`${duration.text ? duration.value > 3600 ? "+ 1 heure" : duration.text : "Bon voyage !"}`);

  };

  return (
    <OfferDetailWrapper>
      <ImageSwapMap
        className={"imgBox"}
        destination={offer.location}
        sourceImage={offer.picture}
        title={"food"}
        handleDirectionDuration={handleDirectionDuration}
      />
      <CartTitle
        className={"title"}
        isPassed={moment(offer.datetime).isBefore(Date.now())}
      >
        <span>{offer.description}</span>
      </CartTitle>
      <div className={"reserveBox"}>
        <ReservationBox
          offer={offer}
          isPassed={moment(offer.datetime).isBefore(Date.now())}
        />
      </div>

      <div className={"badgeRone"}>
        <BadgeContainer>
          <Badge>
            <span>{offer.type}</span>
          </Badge>
          <Badge>
            <span>{moment(offer.datetime).format("DD MMMM")}</span>
          </Badge>
          <Badge>
            <span>{travelDuration}</span>
          </Badge>
        </BadgeContainer>
      </div>
      <div className={"badgeRtwo"}>
        <BadgeContainer>
          <Badge>
            <span>{offer.quantity} personnes</span>
          </Badge>

          <Badge>
            <span>{moment(offer.datetime).format("hh:mm")}</span>
          </Badge>
          <Badge isOutlined imgSrc={offer.author.picture} />
        </BadgeContainer>
      </div>
      <DetailCtrlContainer className={"ctrl"}>
        {currentIndex !== 0 && (
          <DetailCtrlWrapper
            className={"ctrlLeft"}
            onClick={() => handleCtrl(currentIndex - 1)}
          >
            <span>Précédent</span>
          </DetailCtrlWrapper>
        )}
        {currentIndex !== 0 && currentIndex !== maxIndex && (
          <DetailCtrlDivider />
        )}
        {currentIndex !== maxIndex && (
          <DetailCtrlWrapper
            className={"ctrlRight"}
            onClick={() => handleCtrl(currentIndex + 1)}
          >
            <span>Suivant</span>
          </DetailCtrlWrapper>
        )}
      </DetailCtrlContainer>
    </OfferDetailWrapper>
  );
};

export default OfferDetailCard;
