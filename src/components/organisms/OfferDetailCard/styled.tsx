import styled from 'styled-components';


const OfferDetailWrapper = styled.div`
  width: 70%;
  height: 100%;
  display: grid;
  grid-template-columns: 2.8fr 1.5fr;
  grid-template-rows: 3.5fr 0.5fr 0.5fr;
  grid-template-areas:    
    'img reservebox'
    'title badgeRone'
    'title badgeRtwo'
    'ctrl ctrl';
  grid-gap: 10px;
  
  .imgBox {    
    grid-area: img;
  }
  
  .title {    
    grid-area: title;
    height: 100%;
    margin: 0;
    padding: 0;
  }
  
  .reserveBox {    
    grid-area: reservebox;
  }
  
  .badgeRone {
    grid-area: badgeRone;
  }
  
  .badgeRtwo {
    grid-area: badgeRtwo;
  }
  
  .ctrl {
    grid-area: ctrl;
  }
  
  @media (max-width: 768px) {
    grid-template-areas:    
    'img'
    'reservebox'
    'title'
    'badgeRone'
    'badgeRtwo'
    'ctrl';
    
    grid-auto-columns: max-content;
    grid-auto-rows: auto;
    
    grid-template-columns: auto auto;
    grid-template-rows: 1fr 0.5fr 0.5fr;    
    width: auto;
  }
`;

const BadgeContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

const DetailCtrlWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  background: ${props => props.theme.colors.blue};  
  flex: 1;
  padding: 2.5rem 0;
  cursor: pointer;
  position: relative;
  box-shadow: 2px 2px ${props => props.theme.colors.darkgrey};
  
  span {
    font-size: 1.6rem;
    font-family: Milkshake,serif;
    color: #FFF;
  }
  div:nth-child(2) {
    margin-left: 10px;
  }
`;

const DetailCtrlContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;
const DetailCtrlDivider = styled.div`
  width: 10px;
`;


export {OfferDetailWrapper, BadgeContainer,DetailCtrlWrapper, DetailCtrlContainer, DetailCtrlDivider};