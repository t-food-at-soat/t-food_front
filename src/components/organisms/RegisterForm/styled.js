import styled from "styled-components";

const RegisterFormStyled = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  
   svg {
    padding: 0;
    margin-right: 1rem;
    }

  button {
    margin: 0.5rem 0;
    flex-direction: row-reverse;

    svg {
      width: 5rem !important;
      height: 5rem !important;
      padding-right: 1.5rem;      
    }
  }
  
  div[color="#4F4F4F"] {
    margin: 0.5rem 0;
  }
  
  @media (max-width: 768px) {
     min-width: 80%;
  }
  
`;

export { RegisterFormStyled };
