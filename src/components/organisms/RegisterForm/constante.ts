import {ReactComponent as IndividualIcon} from "../../../assets/icons/particulier.svg";
import {ReactComponent as AssociationIcon} from "../../../assets/icons/association.svg";
import {ReactComponent as CorporationIcon} from "../../../assets/icons/entreprise.svg";
import Roles from "../../../ts/types/roles.types";
import Point from "../../../ts/types/point.types";

export const initialFormState = {
  roles: Roles.PUBLIC,
  basis: {
    name: "",
    email: "",
    password: "",
    picture: "",
    phone: ""
  },
  address: "",
  addressCoord: new Point(0, 0),
  infoAssoEntreprise: {
    numSiret: ""
  },
  picture: ""
};

export const icon = (type: Roles) => {
  if (type === Roles.ASSOCIATION) return AssociationIcon;
  if (type === Roles.INDIVIDUAL) return IndividualIcon;
  if (type === Roles.CORPORATION) return CorporationIcon;
};

export const color = (type: Roles) => {
  if (type === Roles.ASSOCIATION) return "#efc364";
  if (type === Roles.INDIVIDUAL) return "#f15e1b";
  if (type === Roles.CORPORATION) return "#52a3dd";
};
