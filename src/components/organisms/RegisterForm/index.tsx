import React, {useEffect, useState} from "react";
import {RegisterFormStyled} from "./styled";
import RegisterBasis from "../../molecules/Forms/RegisterBasics/index";
import RegisterAddress from "../../molecules/Forms/RegisterAddress";
import NavigateFormButtons from "../../molecules/navigateFormButtons";
import Button from "../../atoms/Button";
import Title from "../../atoms/Title";
import {theme} from "../../_settings/theme";
import RegisterAssoEntreprise from "../../molecules/Forms/RegisterAssoEntreprise";
import {initialFormState, color, icon} from "./constante";
import {IRootState} from "../../../store/combiners";
import {RegisterAction} from "../../../store/register/types";
import {Dispatch} from "redux";
import {register} from "../../../store/register/actions";
import {connect} from "react-redux";
import User from "../../../ts/interfaces/user.interfaces";
import Roles from "../../../ts/types/roles.types";
import Point from "../../../ts/types/point.types";
import {node} from "prop-types";
import {toast} from "react-toastify";

interface IRegisterFormProps {
  onSubmit: (registerInfo: any) => void;
  onBack: () => void;
  roles: Roles;
}

const RegisterForm = ({
                        register,
                        roles,
                        onBack
                      }: IRegisterFormProps & IPropsDispatch & IPropsState) => {
  const [count, setCount] = useState(0);
  const [registerInfo, setRegisterInfo] = useState(initialFormState);

  const handleClick = () => {
    const user: User = new User(
      registerInfo.basis.name,
      registerInfo.basis.email,
      registerInfo.basis.password,
      registerInfo.basis.phone,
      registerInfo.address,
      roles,
      registerInfo.addressCoord,
      registerInfo.basis.picture
    );
    register(user);
  };

  const handleChange = (data: any) => {
    console.log(data)
    setRegisterInfo({
      ...registerInfo,
      ...data
    });
  };

  const isPanelValid = (nodes: NodeListOf<HTMLElement>) => {
    let output = true;
    nodes.forEach(node => {
      if (node.dataset.valid === "false") {
        output = false
      }
    });
    return output
  };

  const max = roles === Roles.INDIVIDUAL ? 1 : 2;
  return (
    <RegisterFormStyled>
      <Button
        icon={icon(roles)}
        iconColor={color(roles)}
        {...theme.light}
        onClick={() => {
        }}
      >
        <Title font={"Milkshake"}>
          <span style={{color: `${color(roles)}`}}>{roles}</span>
        </Title>
      </Button>
      {count === 0 && (
        <RegisterBasis
          onChange={data =>
            handleChange({basis: {...registerInfo.basis, ...data}})
          }
          basis={registerInfo.basis}
        />
      )}
      {count === 1 && (
        <RegisterAddress
          onChange={data =>
            handleChange({
              address: data.address,
              addressCoord: data.addressCoord
            })
          }
          address={registerInfo.address}
        />
      )}
      {count === 2 && (
        <RegisterAssoEntreprise
          onChange={data =>
            handleChange({
              infoAssoEntreprise: {
                ...registerInfo.infoAssoEntreprise,
                ...data
              }
            })
          }
          infoAssoEntreprise={registerInfo.infoAssoEntreprise}
        />
      )}
      {count !== max && (
        <NavigateFormButtons
          left={<Title font={"Milkshake"}>
            <span>Précédent</span>
          </Title>}
          right={<Title font={"Milkshake"}>
            <span>Suivant</span>
          </Title>}
          onClickL={() => (count === 0 ? onBack() : setCount(count - 1))}
          onClickR={() => {
            isPanelValid(document.querySelectorAll('[data-valid]'))
              ? count === max ? setCount(count) : setCount(count + 1)
              : toast.error("Le formulaire n'est pas valide")
          }
          }
        />
      )}
      {count === max && (
        <NavigateFormButtons
          left={<Title font={"Milkshake"}>
            <span>Précédent</span>
          </Title>}
          right={<Title font={"Milkshake"}>
            <span>Valider</span>
          </Title>}
          onClickL={() => setCount(count - 1)}
          onClickR={() => {
            isPanelValid(document.querySelectorAll('[data-valid]'))
              ? handleClick()
              : toast.error("Le formulaire n'est pas valide")
          }
          }
        />
      )}
    </RegisterFormStyled>
  );
};

interface IPropsDispatch {
  register: (user: User) => void;
}

interface IPropsState {
  isRegistered: boolean;
  errorMessage: string;
}

const mapStateToProps = ({register}: IRootState): IPropsState => ({
  isRegistered: register.isRegistered,
  errorMessage: register.errorMessage
});

const mapDispatchToProps = (
  dispatch: Dispatch<RegisterAction>
): IPropsDispatch => ({
  register: (user: User) => dispatch(register(user))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegisterForm);
