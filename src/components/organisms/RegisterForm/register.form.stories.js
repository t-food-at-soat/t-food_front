import React from "react";
import { ThemeProvider } from "styled-components";
import { GlobalStyle, theme } from "../../_settings/theme";
import RegisterForm from "./index";
import Roles from '../../../ts/types/roles.types';

export default { title: "Organisms|Register Form" };

export const IndividualForm = () => {
  return (
    <ThemeProvider theme={theme}>
      <>
        <GlobalStyle />
        <RegisterForm
          type={Roles.INDIVIDUAL}
          onClick={element => console.log(element)}
        />
      </>
    </ThemeProvider>
  );
};

export const AssociaitionForm = () => {
  return (
    <ThemeProvider theme={theme}>
      <>
        <GlobalStyle />
        <RegisterForm
          type={Roles.ASSOCIATION}
          onClick={element => console.log(element)}
        />
      </>
    </ThemeProvider>
  );
};

export const CorporationForm = () => {
  return (
    <ThemeProvider theme={theme}>
      <>
        <GlobalStyle />
        <RegisterForm
          type={Roles.CORPORATION}
          onClick={element => console.log(element)}
        />
      </>
    </ThemeProvider>
  );
};
