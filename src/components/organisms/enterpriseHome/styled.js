import styled from "styled-components";
import ImgBG from './../../organisms/HeroHeader/bgImage.jpg'

const EnterpriseHomeWrapper = styled.div`
  text-align: center;
  width: 100%;
`;

const OfferAddFormWrapper = styled.div`
  background: url(${ImgBG})
    no-repeat;
  background-size: cover;
  display: flex;
  justify-content: center;
  width: 100%;
  padding: 2.5rem 0;
`;

const CardListWrapper = styled.div`
  display: flex;
  justify-content: center;
  margin: 2.5rem auto;
  max-width: 960px;
`;


export { EnterpriseHomeWrapper, OfferAddFormWrapper, CardListWrapper };
