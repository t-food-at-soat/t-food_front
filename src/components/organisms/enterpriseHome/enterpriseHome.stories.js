import React from "react";
import EnterpriseHome from "./index";
import { ThemeProvider } from "styled-components";
import { GlobalStyle, theme } from "../../_settings/theme";

export default { title: "Organisms|EnterpriseHome" };

export const OfferFormDefault = () => (
  <ThemeProvider theme={theme}>
    <>
      <GlobalStyle />
      <EnterpriseHome />
    </>
  </ThemeProvider>
);
