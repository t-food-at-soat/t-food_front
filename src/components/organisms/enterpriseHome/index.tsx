import React, { useEffect } from "react";
import {
  EnterpriseHomeWrapper,
  OfferAddFormWrapper,
  CardListWrapper
} from "./styled";
import { IRootState } from "../../../store/combiners";
import { Dispatch } from "redux";
import { OfferAction } from "../../../store/offer/types";
import { getUserOffer, setOfferIsLoad } from "../../../store/offer/actions";
import { connect } from "react-redux";
import Offer from "../../../ts/interfaces/offer.interfaces";
import Mascot from "../../atoms/Mascot";
import OfferForm from "../OfferForm";
import Title from "../../atoms/Title";
import CardOfferList from "../CardOfferList";
import { LoadingWrapper } from "../userHome/styled";
import { EggLoading } from "../../atoms/Loading";

interface EnterpriseHomeProps {}

const EnterpriseHome = ({
  enterpriseHistory,
  getUserOffer,
  isOfferListLoad,
  setOfferListLoad
}: EnterpriseHomeProps & IPropsState & IPropsDispatch) => {
  useEffect(() => {
    getUserOffer();
    return () => {
      setOfferListLoad(false);
    };
  }, [getUserOffer]);
  return (
    <EnterpriseHomeWrapper>
      <Title fontSize={"4.9rem"}>
        Nouvelle{" "}
        <span>
          <span>T</span>Offre
        </span>
      </Title>
      <OfferAddFormWrapper>
        {/*<Mascot />*/}
        <OfferForm />
      </OfferAddFormWrapper>
      <Title fontSize={"4.9rem"}>
        Mes{" "}
        <span>
          <span>T</span>Offres
        </span>
      </Title>
      {isOfferListLoad ? (
        <CardListWrapper>
          <CardOfferList onClick={data => {}} offerList={enterpriseHistory} />
        </CardListWrapper>
      ) : (
        <LoadingWrapper>
          <EggLoading />
          <Title font={"Milkshake"} fontSize={"3rem"}>
            <span>Chargement ...</span>
          </Title>
        </LoadingWrapper>
      )}
    </EnterpriseHomeWrapper>
  );
};

interface IPropsState {
  enterpriseHistory: Array<Offer>;
  isOfferListLoad: boolean;
}

interface IPropsDispatch {
  getUserOffer: () => void;
  setOfferListLoad: (state: boolean) => void;
}

const mapStateToProps = ({ offer }: IRootState): IPropsState => ({
  enterpriseHistory: offer.offerList,
  isOfferListLoad: offer.isLoad
});

const mapDispatchToProps = (
  dispatch: Dispatch<OfferAction>
): IPropsDispatch => ({
  getUserOffer: () => dispatch(getUserOffer()),
  setOfferListLoad: (payload: boolean) => dispatch(setOfferIsLoad(payload))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EnterpriseHome);
