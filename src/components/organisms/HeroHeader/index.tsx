import React from "react";
import { HeroHeaderWrapper, Divider } from "./styled";
import NewUser from "../../molecules/NewUser";
import ConnexionForm from "../../molecules/Forms/Connexion";

const HeroHeader = () => {
  return (
    <HeroHeaderWrapper>
      <NewUser />
      <Divider className={"divider"} />
      <ConnexionForm classNameConnexion={"form-connexion"} />
    </HeroHeaderWrapper>
  );
};

export default HeroHeader;
