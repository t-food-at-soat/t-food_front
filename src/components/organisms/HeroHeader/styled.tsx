import styled from 'styled-components';
import ImgBG from './bgImage.jpg'


const HeroHeaderWrapper = styled.div`
    display: flex;
    justify-content: space-around;
    align-items: center;
    background: url(${ImgBG});
    background-size: cover;
    padding: 2.5rem 0;
    
    .form-connexion {    
      flex: 1;
      display: flex;
      justify-content: center;
      align-items: center;
      div{
        min-width: 300px;
      }
    }
    
    .new-user {
      flex: 1;
    }
    
    @media (max-width: 758px) {
        .form-connexion, .divider {    
          display: none;
         }
         
         .new-user {
          width: 80%;
        }
  }
`;

const Divider = styled.div`
    width: 3px;
    height: 70px;
    background-color: white;
`;


export {HeroHeaderWrapper, Divider};