import React from 'react';
import HeroHeader from './index';
import {ThemeProvider} from 'styled-components';
import {GlobalStyle, theme} from '../../_settings/theme';
import {Provider} from 'react-redux';
import store from '../../../store/configureStore';

export default {title: 'Organisms|HeroHeader'};

export const badgeWithText = () =>
    <Provider store={store}><ThemeProvider theme={theme}>
      <>
        <GlobalStyle/>
        <HeroHeader/>
      </>
    </ThemeProvider></Provider>;