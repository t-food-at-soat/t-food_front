import React from "react";
import { Provider } from "react-redux";
import store from "../../../store/configureStore";
import OfferForm from "./index";
import { ThemeProvider } from "styled-components";
import { GlobalStyle, theme } from "../../_settings/theme";

export default { title: "Organisms|Offer Add" };

export const OfferFormDefault = () => (
  <Provider store={store}>
    <ThemeProvider theme={theme}>
      <>
        <GlobalStyle />
        <OfferForm />
      </>
    </ThemeProvider>
  </Provider>
);
