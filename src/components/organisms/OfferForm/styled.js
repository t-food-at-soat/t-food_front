import styled from "styled-components";

const OfferFormStyled = styled.div`
  width: 100%;
  margin: 1.5rem;
  flex-basis: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: row;

  @media (max-width: 768px) {
    justify-content: center;
    flex-direction: column;
  }
`;

const ImageContainerStyled = styled.div`
  width: 100%;
  height: auto;
  position: relative;
  margin-right: 2rem;
  @media (max-width: 768px) {
    margin : auto;
  }
`;

const TitleStyled = styled.div`
  width: calc(100% - 2rem);
  margin: 1rem auto;
  @media (max-width: 768px) {
    width: calc(100% - 2rem);
    margin: 1rem auto;
  }
`;

const DataContainerStyled = styled.div`
  width: 100%;
  button {
    margin: auto;
    width: calc(100% - 2rem);
  }
`;

const AddImageStyled = styled.div`
  width: 0;
  height: 0;
  font-size: 40px;
  text-align: center;
  position: absolute;
  top: 10rem;
  border-style: solid;
  border-width: 7rem 0 0 7rem;
  border-color: transparent transparent transparent white;
  
  * {
    padding: 0;
  }
  &:hover {
    box-shadow: -32px 33px 65px -37px rgba(0, 0, 0, 0.75);
    cursor: pointer;
  }

  p {
    z-index: 10;
    top: -8.5rem;
    left: -6rem;
    position: absolute;
    &:hover {
      text-shadow: 4px -1px 41px rgba(0, 0, 0, 0.83);
      cursor: pointer;
    }
  }

  input {
    z-index: 12;
    top: -6.2rem;
    opacity: 0;
    left: -7rem;
    position: absolute;
    &:hover {
      text-shadow: 4px -1px 41px rgba(0, 0, 0, 0.83);
      cursor: pointer;
    }
    width: 6rem;
    height: 6rem;
    border: 1px solid #ccc;
    display: inline-block;
  }
`;

const DateQuantityStyled = styled.div`
  
  div {
    margin: 1rem 1rem 0;
    width: calc(100% - 2rem);
  }
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;
const CategorieDestinataireStyled = styled.div`
  display: flex;
  flex-direction: row;
  @media (max-width: 768px) {
    flex-direction: column;
    select {
      width: auto;
    }
  }
  div,
  select {
    margin: 1rem;
  }
`;

const InputMomentWrapper = styled.div`
  background: rgba(0, 0, 0, 0.5);
  top: 0;
  left: 0;
  width: 100vw !important;
  height: 100vh;
  margin: 0 !important;
  position: fixed;
  z-index: 1000;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  button {
    margin: 0;
    width: 70%;
  }
  
  .im-big-input-moment {
    div {
     margin: 0;
    }
    max-width: 70%;
    max-height: 70%;
    background: #FFF;
    font-family: Hiragino,serif;
    .toolbar, .im-date-picker thead td, .im-time-slider .separator {
      color: ${props => props.theme.colors.green};
    }
    
    .im-date-picker td.current, .im-time-slider .time, .im-slider .value{
      background-color: ${props => props.theme.colors.green};
    }
    .current-date {
      text-decoration: none !important;
    }
    .im-slider .handle:after {
      border: 3px solid ${props => props.theme.colors.green};
    }
    
    @media (max-width: 768px) {    
      max-width: 100%;
    }
  }
`;

export {
  OfferFormStyled,
  AddImageStyled,
  ImageContainerStyled,
  DataContainerStyled,
  DateQuantityStyled,
  CategorieDestinataireStyled,
  TitleStyled,
  InputMomentWrapper
};
