import React, { useState } from "react";
import {
  AddImageStyled,
  CategorieDestinataireStyled,
  DataContainerStyled,
  DateQuantityStyled,
  ImageContainerStyled,
  InputMomentWrapper,
  OfferFormStyled,
  TitleStyled
} from "./styled";
import Input from "../../atoms/Input";
import { theme } from "../../_settings/theme";
import ImageBox from "../../atoms/ImageBox";
import Select, { IOption } from "../../atoms/Select";
import Button from "../../atoms/Button";
import Title from "../../atoms/Title";
import Offer from "../../../ts/interfaces/offer.interfaces";
import { OfferAction } from "../../../store/offer/types";
import { addOffer, editOffer } from "../../../store/offer/actions";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { IRootState } from "../../../store/combiners";
import OfferTarget from "../../../ts/types/offerTarget.types";
import OfferType from "../../../ts/types/offerType.types";
import User from "../../../ts/interfaces/user.interfaces";
import { ILoggedInPayload } from "../../../store/authentication/types";
import Autocomplete from "../../atoms/Autocomplete";
import moment, { Moment } from "moment-timezone";
// @ts-ignore
import { BigInputMoment as InputMoment } from "react-input-moment";
import "../../../../node_modules/react-input-moment/css/input-moment.min.css";
import { ReactComponent as CalendarIcon } from "../../../assets/icons/calendar.svg";
import { initialFormState } from "../RegisterForm/constante";

interface IPropsOfferForm {
  offerToUpdate?: Offer;
}

interface IOfferState {
  description: string;
  datetime: Date;
  type: OfferType;
  target: OfferTarget;
  quantity: number;
  location: string;
  imageUrl: ArrayBuffer | string | null;
  picture: string;
}

const types: IOption[] = [
  { id: 1, value: OfferType.Pizza },
  { id: 2, value: OfferType.BuffetFroid },
  { id: 3, value: OfferType.BuffetChaud },
  { id: 4, value: OfferType.Salades }
];

const targets: IOption[] = [
  { id: 1, value: OfferTarget.association },
  { id: 2, value: OfferTarget.individual }
];

const OfferForm = ({
  addOffer,
  editOffer,
  connectedUser,
  offerToUpdate
}: IPropsDispatch & IPropsState & IPropsOfferForm) => {
  const setDefaultPicture = (type: OfferType): string => {
    console.log(type);
    switch (type) {
      case OfferType.Salades:
        return "https://assets.afcdn.com/recipe/20161010/64090_w1024h768c1cx3680cy2456.jpg";
      case OfferType.BuffetChaud:
        return "https://media-cdn.tripadvisor.com/media/photo-s/0d/6f/39/2d/buffet-chaud.jpg";
      case OfferType.BuffetFroid:
        return "https://www.flunch-traiteur.fr/cms/wp-content/uploads/2018/12/grand-buffet-froid-min.jpg";
      default:
        return "https://www.atelierdeschefs.com/media/recette-e30299-pizza-pepperoni-tomate-mozza.jpg";
    }
  };

  const initialState = {
    description: offerToUpdate ? offerToUpdate.description : "",
    quantity: offerToUpdate ? offerToUpdate.quantity : 1,
    datetime: offerToUpdate ? offerToUpdate.datetime : new Date(),
    type: offerToUpdate ? offerToUpdate.type : OfferType.Pizza,
    target: offerToUpdate ? offerToUpdate.target : OfferTarget.association,
    location: offerToUpdate ? offerToUpdate.location : "",
    picture: offerToUpdate ? offerToUpdate.picture : "",
    imageUrl: setDefaultPicture(
      offerToUpdate ? offerToUpdate["type"] : OfferType.Pizza
    )
  };

  const [offerState, setOfferState] = useState<IOfferState>(initialState);

  const [mom, setMom] = useState(moment());
  const [isCalendarOpen, setCalendarOpen] = useState(false);

  //TODO: update picture url
  const handleImageChange = (imageFile: any) => {
    setOfferState(imageFile[0]);
    let fr = new FileReader();
    fr.onload = () => {
      setOfferState({ ...offerState, imageUrl: fr.result });
    };
    fr.readAsDataURL(imageFile[0]);
  };

  const handleChange = (data: any) => {
    if (data.type) {
      setOfferState({
        ...offerState,
        ...data,
        imageUrl: setDefaultPicture(data.type)
      });
    } else {
      setOfferState({ ...offerState, ...data });
    }
  };

  const handleCalendar = () => {
    setCalendarOpen(!isCalendarOpen);
  };

  const onSubmit = () => {
    const user: User = new User();
    user.picture = connectedUser ? connectedUser.picture : "";
    user.id = connectedUser ? parseInt(connectedUser.id) : -1;
    const offer: Offer = new Offer(
      offerState.description,
      offerState.datetime,
      offerState.type,
      offerState.target,
      offerState.quantity,
      offerState.location,
      // @ts-ignore
      offerState.picture ? offerState.picture : offerState.imageUrl,
      { ...user },
      offerToUpdate ? offerToUpdate.id : 1
    );
    offerToUpdate ? editOffer(offer) : addOffer(offer);

    setOfferState(initialState);
  };

  return (
    <OfferFormStyled>
      <ImageContainerStyled>
        <ImageBox
          shadowOff={true}
          sourceImage={
            offerState.picture ? offerState.picture : offerState.imageUrl
          }
          title={"Offer Image"}
        />
        <AddImageStyled>
          <Input
            placeholder={"Photo"}
            name={"picture"}
            value={""}
            type={"file"}
            onChange={handleChange}
          />
          <p>+</p>
        </AddImageStyled>
      </ImageContainerStyled>
      <DataContainerStyled>
        <TitleStyled>
          <Input
            {...theme.light}
            placeholder={"Titre"}
            name={"description"}
            type={"text"}
            onChange={handleChange}
            value={offerState.description}
          />
        </TitleStyled>
        <Autocomplete
          onChange={({ address }) => handleChange({ location: address })}
        />
        <DateQuantityStyled>
          <Input
            {...theme.light}
            IconComponent={CalendarIcon}
            placeholder={"Quantité"}
            type={"text"}
            name={"datetime"}
            disabled={true}
            value={mom.format("[le] DD MMMM [à] HH:mm")}
            onClick={handleCalendar}
          />
          {isCalendarOpen && (
            <InputMomentWrapper>
              <InputMoment
                moment={mom}
                onChange={(m: Moment) => {
                  setMom(m);
                  handleChange({
                    datetime: m.toDate()
                  });
                }}
                minStep={1}
                hourStep={1}
                locale="fr"
              />
              <Button size="big" {...theme.light} onClick={handleCalendar}>
                <Title>
                  <span>Sauvegarder</span>
                </Title>
              </Button>
            </InputMomentWrapper>
          )}
          <Input
            {...theme.light}
            placeholder={"Quantité"}
            type={"quantity"}
            name={"quantity"}
            onChange={handleChange}
            value={offerState.quantity.toString()}
            label={"personnes"}
          />
        </DateQuantityStyled>
        <CategorieDestinataireStyled>
          <Select
            label={"Catégorie"}
            options={types}
            name={"type"}
            value={offerState.type}
            onChange={data => handleChange({ type: data })}
          />
          <Select
            label={"Destinataire"}
            name={"target"}
            value={offerState.target}
            options={targets}
            onChange={data => handleChange({ target: data })}
          />
        </CategorieDestinataireStyled>
        <Button size="big" {...theme.light} onClick={onSubmit}>
          <Title>
            <span>{offerToUpdate ? "Modifier" : "Ajouter"}</span>
          </Title>
        </Button>
      </DataContainerStyled>
    </OfferFormStyled>
  );
};

interface IPropsDispatch {
  addOffer: (offer: Offer) => void;
  editOffer: (offer: Offer) => void;
}

interface IPropsState {
  connectedUser: ILoggedInPayload | undefined;
  errorMessage: string;
}

const mapStateToProps = ({
  offer,
  authentication
}: IRootState): IPropsState => ({
  connectedUser: authentication.userConnected,
  errorMessage: offer.errorMessage
});

const mapDispatchToProps = (
  dispatch: Dispatch<OfferAction>
): IPropsDispatch => ({
  addOffer: (offer: Offer) => dispatch(addOffer(offer)),
  editOffer: (offer: Offer) => dispatch(editOffer(offer))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(OfferForm);
