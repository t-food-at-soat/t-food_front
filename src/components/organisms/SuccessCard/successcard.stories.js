import React from "react";
import {ThemeProvider} from "styled-components";
import {GlobalStyle, theme} from "../../_settings/theme";
import SuccessCard from "./index";

export default {
  title: "Organisms|SuccessCard",
  includeStories: ["standard"]
};

export const standard = () => (
    <ThemeProvider theme={theme}>
      <GlobalStyle/>
      <SuccessCard message={"Votre réservation à bien été prise en compte !"}/>
    </ThemeProvider>
);
