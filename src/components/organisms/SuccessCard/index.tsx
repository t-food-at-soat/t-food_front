import React from 'react';
import {SuccessCardWrapper} from './styled'
import Title from "../../atoms/Title";
import {theme} from "../../_settings/theme";
import Button from "../../atoms/Button";
import Mascot from "../../atoms/Mascot";
import {navigate} from "hookrouter";

interface ISuccessCardProp {
    message : string
}

const SuccessCard = ({message}:ISuccessCardProp) => {
    return (
        <SuccessCardWrapper>
            <div>
                <Title font={"Milkshake"} fontSize={"3rem"}>
                    <span>{message}</span>
                </Title>
                <Button size="big" {...theme.light} onClick={ () => navigate("/")} >
                    <Title font={"Milkshake"}><span><span>Accueil</span></span></Title>
                </Button>
            </div>
            <Mascot/>
        </SuccessCardWrapper>
    );
};

export default SuccessCard;
