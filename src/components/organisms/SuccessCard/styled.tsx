import styled from "styled-components";

export const SuccessCardWrapper = styled.div`
  background-color: ${props => props.theme.colors.darkgrey};
  border-radius: 7px;
  display: flex;
  justify-content: center;
  align-items: center;
  
  padding: 6rem;
  
  svg {
    transform: scaleX(-1);
    margin-left: 1.5rem;
  }
  
  @media (max-width: 768px) {
     flex-direction: column-reverse;
   }
`;