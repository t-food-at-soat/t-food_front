import React from "react";
import { ThemeProvider } from "styled-components";
import { GlobalStyle, theme } from "../../_settings/theme";
import Organism401 from "./index";

export default {
  title: "Organisms|Organism401",
  includeStories: ["standard"]
};

export const standard = () => (
  <ThemeProvider theme={theme}>
    <div style={{ background: "white" }}>
      <GlobalStyle />
      <Organism401 />
    </div>
  </ThemeProvider>
);
