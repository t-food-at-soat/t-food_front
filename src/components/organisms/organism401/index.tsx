import React from "react";
import { Organism401Wrapper } from "./styled";
import { ReactComponent as TfoodNest } from "./401.svg";
import Title from "../../atoms/Title";
interface IOrganism404Props {}

const Organism401 = ({ ...props }: IOrganism404Props) => {
  return (
    <Organism401Wrapper>
      <Title fontSize="3.5rem">
        <span>
          <span>T</span>food
        </span>{" "}
        le{" "}
        <span>
          <span>T</span>rex
        </span>
        ,<br />
        partage sa <span style={{ fontFamily: "milkshake" }}>nourriture </span>
        uniquement avec
        <br />
        les utilisateurs{" "}
        <span style={{ fontFamily: "milkshake" }}>connectés</span>
      </Title>
      <TfoodNest></TfoodNest>
    </Organism401Wrapper>
  );
};

export default Organism401;
