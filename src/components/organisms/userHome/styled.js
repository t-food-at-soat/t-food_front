import styled from "styled-components";

const UserHomeWrapper = styled.div`
  text-align: center;
  width: 70%;
`;

const LoadingWrapper = styled.div`
  display: flex;
  flex: 1;
  justify-content: center;
  align-items: center;
  flex-direction: column;  
  margin-top: 25vh;
  svg {
    height: 7rem;
  }
`;

export { UserHomeWrapper, LoadingWrapper };
