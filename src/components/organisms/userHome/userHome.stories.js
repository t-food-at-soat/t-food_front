import React from "react";
import EnterpriseHome from "./index";
import { ThemeProvider } from "styled-components";
import { GlobalStyle, theme } from "../../_settings/theme";

export default { title: "Organisms|UserHome" };

export const Default = () => (
  <ThemeProvider theme={theme}>
    <>
      <GlobalStyle />
      <EnterpriseHome />
    </>
  </ThemeProvider>
);
