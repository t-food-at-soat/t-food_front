import React, { useEffect } from "react";
import { UserHomeWrapper, LoadingWrapper } from "./styled";
import { IRootState } from "../../../store/combiners";
import { Dispatch } from "redux";
import { OfferAction } from "../../../store/offer/types";
import { getAllOffer, setOfferIsLoad } from "../../../store/offer/actions";
import { connect } from "react-redux";
import Offer from "../../../ts/interfaces/offer.interfaces";
import CardOfferList from "../CardOfferList";
import { EggLoading } from "../../atoms/Loading";
import Title from "../../atoms/Title";

interface UserHomeProps {}

const UserHome = ({
  offerList,
  getAllOffer,
  setOfferListLoad,
  isOfferListLoad
}: UserHomeProps & IPropsState & IPropsDispatch) => {
  useEffect(() => {
    getAllOffer();
    return () => {
      setOfferListLoad(false);
    };
  }, [getAllOffer]);
  return (
    <UserHomeWrapper>
      {isOfferListLoad ? (
        <CardOfferList onClick={() => {}} offerList={offerList} />
      ) : (
        <LoadingWrapper>
          <EggLoading />
          <Title font={"Milkshake"} fontSize={"3rem"}>
            <span>Chargement ...</span>
          </Title>
        </LoadingWrapper>
      )}
    </UserHomeWrapper>
  );
};

interface IPropsState {
  offerList: Array<Offer>;
  isOfferListLoad: boolean;
}

interface IPropsDispatch {
  getAllOffer: () => void;
  setOfferListLoad: (state: boolean) => void;
}

const mapStateToProps = ({ offer }: IRootState): IPropsState => ({
  offerList: offer.offerList,
  isOfferListLoad: offer.isLoad
});

const mapDispatchToProps = (
  dispatch: Dispatch<OfferAction>
): IPropsDispatch => ({
  getAllOffer: () => dispatch(getAllOffer()),
  setOfferListLoad: (payload: boolean) => dispatch(setOfferIsLoad(payload))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserHome);
