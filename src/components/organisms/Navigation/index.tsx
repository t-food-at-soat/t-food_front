import React, {useEffect} from "react";
import {NavigationContainer, LeftNavWrapper} from "./styled";
import Logo from "../../atoms/Logo";
import Profile from "../../molecules/Profile";
import {A, navigate, usePath} from "hookrouter";
import {IRootState} from "../../../store/combiners";
import {Dispatch} from "redux";
import {
    AuthenticationAction,
    ILoggedInPayload
} from "../../../store/authentication/types";
import {loggedIn, logout} from "../../../store/authentication/actions";
import {connect} from "react-redux";
import Cookies from "universal-cookie";
import {MascotLoading} from "../../atoms/Loading";
import Roles from "../../../ts/types/roles.types";
import AddressInput from "../../atoms/AddressInput";

interface INavigationProps {
}

const Navigation = ({
                        isConnected,
                        logout,
                        userConnected = {
                            id: "",
                            userName: "",
                            jwt: "",
                            picture: "",
                            roles: Roles.INDIVIDUAL,
                            address: ""
                        },
                        setUserConnected
                    }: INavigationProps & IPropsState & IPropsDispatch) => {

    const handleLogoutClick = () => {
        logout();
        deleteCookieJwt();
        navigate("/");
    };

    const path = usePath();

    const deleteCookieJwt = () => {
        const cookies = new Cookies();
        cookies.remove("tfood-jwt");
    };

    useEffect(() => {
        const cookies = new Cookies();
        let userResponse = cookies.get("tfood-jwt");
        if (userResponse) {
            setUserConnected({
                id: userResponse.id,
                userName: userResponse.userName,
                jwt: userResponse.jwt,
                picture: userResponse.picture,
                roles: userResponse.roles,
                address: userResponse.address
            });
        }
        return;
    }, [setUserConnected]);

    return (
        <NavigationContainer isConnected={isConnected}>
            <LeftNavWrapper>
                <A href="/">
                    <Logo content={"food"}/>
                </A>
                {isConnected && (<AddressInput/>)}
            </LeftNavWrapper>
            {isConnected && (
                <Profile
                    imageUrl={userConnected.picture}
                    username={userConnected.userName}
                    onClick={() => navigate("/profile")}
                    onLogoutClick={handleLogoutClick}
                />
            )}
            {path === "/" || path === "/register" ? <MascotLoading/> : null}
        </NavigationContainer>
    );
};

interface IPropsDispatch {
    logout: () => void;
    setUserConnected: (userConnectedPayload: ILoggedInPayload) => void;
}

interface IPropsState {
    isConnected: boolean;
    userConnected: ILoggedInPayload | undefined;
}

const mapStateToProps = ({authentication}: IRootState): IPropsState => ({
    isConnected: authentication.isConnected,
    userConnected: authentication.userConnected
});

const mapDispatchToProps = (
    dispatch: Dispatch<AuthenticationAction>
): IPropsDispatch => ({
    logout: () => dispatch(logout()),
    setUserConnected: (userConnectedPayload: ILoggedInPayload) =>
        dispatch(loggedIn(userConnectedPayload))
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Navigation);
