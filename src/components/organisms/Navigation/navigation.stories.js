import React from 'react';
import Navigation from './index';
import {ThemeProvider} from 'styled-components';
import {GlobalStyle, theme} from '../../_settings/theme';

export default {title: 'Organisms|Navigation'};

export const NavigationDefault = () => <ThemeProvider theme={theme}>
  <>
    <GlobalStyle/>
    <Navigation/>
  </>
</ThemeProvider>;
