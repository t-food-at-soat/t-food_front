import styled from "styled-components";

interface INavigationContainerProps {
  isConnected: boolean
}

const NavigationContainer = styled.section<INavigationContainerProps>`
  position: relative;
  display: flex;
  justify-content: ${props => props.isConnected ? "space-around" : "center"};
  align-items: center;
  padding: 1.5rem 0;
  
  @media (max-width: 768px) {
     flex-direction: column;
   }
`;

const LeftNavWrapper = styled.div`
  display: flex;
  @media (max-width: 768px) {
     text-align: center;
    flex-direction: column;
  }    
`;
export {NavigationContainer, LeftNavWrapper};
