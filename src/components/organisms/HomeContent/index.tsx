import React from "react";
import {HomeContentContainer, ArticleWrapper} from "./styled";
import Title from "../../atoms/Title";
import ImageBox from "../../atoms/ImageBox";
import Text from "../../atoms/Text";
import ImgSoat from './soat_apero.jpg'
import ImgMeetup from './soat_meetup.jpeg'

const HomeContent = () => {
  return (
    <HomeContentContainer>
      <Title fontSize={"4.9rem"}>
        Mais{" "}
        <span>
          <span>T</span>food
        </span>
        , c’est quoi ?
      </Title>
      <ArticleWrapper>
        <ImageBox
          sourceImage={
            ImgSoat

          }
          title={"Test"}
        />
        <Text>
          TFood est une application révolutionnaire dont le but principal est
          d'éviter le gaspillage de nourriture. Pour ce faire, nous essayons de
          mettre en relation les entreprises organisant des événements (meetup,
          after work), avec des particuliers ou des associations afin qu'ils
          viennent récupérer les potentiels restes de nourriture de ces
          événement.
        </Text>
      </ArticleWrapper>
      <Title fontSize={"4.9rem"}>
        Les{" "}
        <span>
          <span>T</span>trust
        </span>
      </Title>
      <ArticleWrapper>
        <ImageBox
          sourceImage={
            ImgMeetup
          }
          title={"Test"}
        />
        <Text>
          <>
            <Title fontSize={"2rem"}>
              En utilisant{" "}
              <span>
                <span>T</span>food
              </span>
              , vous vous <span><span style={{fontFamily:"Milkshake", color:"#98B92E"}}>engagez</span></span> à :
            </Title>
            <ul>
              <li>ne pas vendre la nourriture récupérée</li>
              <li>ne pas congeler les produits</li>
              <li>honorer vos offres/réservations</li>
              <li>sourire</li>
              <li>être de bonne humeur</li>
            </ul>
          </>
        </Text>
      </ArticleWrapper>
      <Title font={"Milkshake"} fontSize={"3.5rem"}>
        <span>Manger</span> c’est cool, <span>gâcher</span> c’est les boules
      </Title>
    </HomeContentContainer>
  );
};

export default HomeContent;
