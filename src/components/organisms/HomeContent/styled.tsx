import styled from 'styled-components';

interface IHomeContentWrapperProps {
}


export const HomeContentContainer = styled.section<IHomeContentWrapperProps>`
    display: flex;
    flex-direction: column;
    align-items: center;
    max-width: 960px;
    margin: auto;
    span {
      margin: 2.5rem 0;
    }
    
    @media (max-width: 768px) {
        span {
          text-align: center;
        }
    }
`;

export const ArticleWrapper = styled.section<IHomeContentWrapperProps>`
    display: flex;
    justify-content: space-around;
    align-items: center;
    
    div, p {
      width: 45%;
    }
    
    p{
      text-align: justify;
    }
    
    @media (max-width: 768px) {
        flex-direction: column;
        div, p {
          width: 90%;
        }
    }
`;
