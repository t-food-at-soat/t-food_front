import React from 'react';
import HomeContent from './index';
import {ThemeProvider} from 'styled-components';
import {GlobalStyle, theme} from '../../_settings/theme';

export default {title: 'Organisms|HomeContent'};

export const HomeContentDefault = () => <ThemeProvider theme={theme}>
  <>
    <GlobalStyle/>
    <HomeContent/>
  </>
</ThemeProvider>;

