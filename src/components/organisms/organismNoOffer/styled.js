import styled from "styled-components";

const Organism404Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center;
  flex: 1;
  svg {
    margin-top: 1.5rem;
    padding-top: 1.5rem;
    padding-bottom: 1.5rem;
    width: 100%;
    background-color: ${props => props.theme.colors.darkgrey};
  }
  @media (max-width: 768px) {    
    margin-top: 10rem;
  }
`;
const TitleWrapper = styled.div``;
export { Organism404Wrapper, TitleWrapper };
