import React from "react";
import { Organism404Wrapper } from "./styled";
import { ReactComponent as NoOfferSVG } from "./nooffer.svg";
import Title from "../../atoms/Title";
interface IOrganism404Props {}

const OrganismNoOffer = ({ ...props }: IOrganism404Props) => {
  return (
    <Organism404Wrapper>
      <Title fontSize="3.5rem">
        Il n'y a pas d'<span style={{ fontFamily: "milkshake" }}>annonces</span> pour le moment
      </Title>
      <NoOfferSVG/>
    </Organism404Wrapper>
  );
};

export default OrganismNoOffer;
