import React from "react";
import { ThemeProvider } from "styled-components";
import { GlobalStyle, theme } from "../../_settings/theme";
import Organism404 from "./index";

export default {
  title: "Organisms|Organism40re4",
  includeStories: ["standard"]
};

export const standard = () => (
  <ThemeProvider theme={theme}>
    <div style={{ background: "white" }}>
      <GlobalStyle />
      <Organism404 />
    </div>
  </ThemeProvider>
);
