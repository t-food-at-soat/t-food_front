import React from "react";
import { ThemeProvider } from "styled-components";
import { Provider } from "react-redux";
import { theme, GlobalStyle } from "./components/_settings/theme";
import Home from "./components/pages/Home";
import store from "./store/configureStore";
import { useRoutes } from "hookrouter";
import Navigation from "./components/organisms/Navigation";
import Footer from "./components/molecules/Footer";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import RegisterUser from "./components/pages/Register/index";
import Organism404 from "./components/organisms/organism404";
import Offer from "./components/pages/Offer";
import Profile from "./components/pages/Profile";
import OfferDetail from "./components/pages/OfferDetail";
import SuccessRegister from "./components/pages/SuccessRegister";
import SuccessReservation from "./components/pages/SuccessReservation";
import { LoadScript } from "@react-google-maps/api";
import "./components/_settings/moment-config";
import PasswordReset from "./components/pages/PasswordReset";
import UpdateOffer from "./components/pages/UpdateOffer";

//toast.configure();

const routes = {
  "/": () => <Home />,
  "/register": () => <RegisterUser />,
  "/register/success": () => <SuccessRegister />,
  "/reservation/success": () => <SuccessReservation />,
  "/offer": () => <Offer />,
  "/offer/:id": ({ id }: any) => <OfferDetail id={id} />,
  "/profile": () => <Profile />,
  "/resetPwd/:jwt": ({ jwt }: any) => <PasswordReset jwt={jwt} />,
  "/offer/update/:id": ({ id }: any) => <UpdateOffer offerId={id} />
};

const App: React.FC = () => {
  const routeResult = useRoutes(routes);
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <LoadScript
          googleMapsApiKey="AIzaSyBOeBriCeuw0BETvQRlKloB4KoooPYzu4g"
          libraries={["places"]}
        >
          <GlobalStyle />
          <Navigation />
          <ToastContainer closeOnClick />
          {routeResult || <Organism404 />}
          <Footer />
        </LoadScript>
      </ThemeProvider>
    </Provider>
  );
};

export default App;
