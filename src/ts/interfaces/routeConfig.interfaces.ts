import HTTP_METHODS from "../types/httpMethod.types";
import Roles from "../types/roles.types";
export default interface RouteConfig {
    route: string,
    execute: string,
    method: HTTP_METHODS,
    schema: any,
    roles: Array<Roles>
}