import Status from "../types/status.types";
import OfferType from "../types/offerType.types";
import OfferTarget from "../types/offerTarget.types";
import User from "./user.interfaces";

export default class Offer {
  id: number | undefined;
  description: string;
  status?: Status;
  datetime: Date;
  type: OfferType;
  target: OfferTarget;
  quantity: number;
  location: string;
  picture: string;
  duration?: number;
  author: User;

  constructor(
    description: string,
    datetime: Date,
    type: OfferType,
    target: OfferTarget,
    quantity: number,
    location: string,
    picture: string,
    author: User,
    id?: number,
    status?: Status,
    duration?: number | undefined
  ) {
    this.description = description;
    this.datetime = datetime;
    this.type = type;
    this.target = target;
    this.quantity = quantity;
    this.location = location;
    this.picture = picture;
    this.author = author;
    this.id = id;
    this.status = status;
    this.duration = duration;
  }

  public setStatus(status: Status): void {
    this.status = status;
  }
}
