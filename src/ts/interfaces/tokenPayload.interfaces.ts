import Roles from "../types/roles.types";

export default interface TokenPayload{
    userId : number
    roles : Roles
}