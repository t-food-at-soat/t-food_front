import User from "./user.interfaces";
import TypeMessage from "../types/typeMessage.types";

export default interface Notification {
    datetime : Date,
    destinataire : User,
    message : string,
    type : TypeMessage
}