import Offer from "./offer.interfaces";
import Point from "../types/point.types";
import Roles from "../types/roles.types";

export default class User {
  id: number | undefined;
  name: string | undefined;
  email: string | undefined;
  phone: string | undefined;
  address: string | undefined;
  addressCoord: Point | undefined;
  picture: string | undefined;
  history: [Offer] | undefined;
  password: string | undefined;
  roles: Roles | undefined;

  constructor(
    name?: string,
    email?: string,
    password?: string,
    phone?: string,
    address?: string,
    roles?: Roles,
    addressCoord?: Point,
    picture?: string,
    history?: [Offer]
  ) {
    this.name = name;
    this.email = email;
    this.phone = phone;
    this.password = password;
    this.address = address;
    this.addressCoord = addressCoord;
    this.picture = picture;
    this.history = history;
    this.roles = roles;
  }
}
