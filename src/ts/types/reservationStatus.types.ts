enum ReservationStatus {
    Passed = "Passed",
    Confirmed = "Confirmed",
    Canceled = "Canceled"
}

export default ReservationStatus;