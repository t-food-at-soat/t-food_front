export default class Point {
  private _latitude: number;
  private _longitude: number;

  constructor(lat: number, long: number) {
    this._latitude = lat;
    this._longitude = long;
  }

  get latitude() {
    return this._latitude;
  }

  get longitude() {
    return this._longitude;
  }
}
