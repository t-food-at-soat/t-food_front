 enum Status {
    Opened = "Opened",
    Closed = "Closed",
    Reserved = "Reserved",
    Canceled = "Canceled"
}

export default Status;