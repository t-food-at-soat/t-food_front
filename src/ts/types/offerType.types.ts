enum OfferType {
  Pizza = "Pizza",
  BuffetFroid = "Buffet froid",
  BuffetChaud = "Buffet chaud",
  Salades = "Salades"
}

export default OfferType;
