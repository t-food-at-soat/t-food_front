import { Action } from "redux";
import Roles from "../../ts/types/roles.types";

export interface IAuthenticationState {
  isConnected: boolean;
  isInvalid: boolean;
  errorMessage: string;
  userConnected: ILoggedInPayload | undefined;
}

export enum AuthenticationActionsKind {
  LOGIN = "AUTHENTICATION_LOGIN",
  LOGOUT = "AUTHENTICATION_LOGOUT",
  LOGGED_IN = "AUTHENTICATION_LOGGED_IN",
  LOGGED_OUT = "AUTHENTICATION_LOGGED_OUT",
  LOGIN_FAIL = "AUTHENTICATION_LOGIN_FAIL"
}

export interface ILogin extends Action {
  readonly type: AuthenticationActionsKind.LOGIN;
  readonly meta: {
    email: string;
    password: string;
  };
}

export interface ILogout extends Action {
  readonly type: AuthenticationActionsKind.LOGOUT;
}

export interface ILoggedIn extends Action {
  readonly type: AuthenticationActionsKind.LOGGED_IN;
  readonly payload: ILoggedInPayload;
}

export interface ICoordType {
  lat: number;
  lng: number;
}

export interface ILoggedInPayload {
  id: string;
  userName: string;
  jwt: string;
  picture: string;
  roles: Roles;
  address: string;
}

export interface ILoggedOut extends Action {
  readonly type: AuthenticationActionsKind.LOGGED_OUT;
}

export interface ILoginFail extends Action {
  readonly type: AuthenticationActionsKind.LOGIN_FAIL;
  readonly payload: Error;
}

export type AuthenticationAction =
  | ILogin
  | ILogout
  | ILoggedIn
  | ILoggedOut
  | ILoginFail;
