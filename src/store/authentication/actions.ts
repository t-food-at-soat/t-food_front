import {
  AuthenticationActionsKind,
  ILoggedIn,
  ILoggedInPayload,
  ILoggedOut,
  ILogin,
  ILoginFail,
  ILogout
} from "./types";

export const login = (email: string, password: string): ILogin => ({
  type: AuthenticationActionsKind.LOGIN,
  meta: {
    email,
    password
  }
});

export const logout = (): ILogout => ({
  type: AuthenticationActionsKind.LOGOUT
});

export const loggedIn = (payload: ILoggedInPayload): ILoggedIn => ({
  type: AuthenticationActionsKind.LOGGED_IN,
  payload
});

export const loggedOut = (): ILoggedOut => ({
  type: AuthenticationActionsKind.LOGGED_OUT
});

export const loginFail = (payload: Error): ILoginFail => ({
  type: AuthenticationActionsKind.LOGIN_FAIL,
  payload
});
