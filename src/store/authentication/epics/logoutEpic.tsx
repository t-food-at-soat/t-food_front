import { Epic, ofType } from "redux-observable";
import { of } from "rxjs";
import {AuthenticationActionsKind, ILogout, ILoggedOut} from "../types";
import { IRootState } from "../../combiners";
import {switchMap} from "rxjs/operators";
import {loggedOut} from "../actions";

export const logoutEpic: Epic<ILogout, any, IRootState, ILoggedOut> = (
  action$
) =>
  action$.pipe(
    ofType(AuthenticationActionsKind.LOGOUT),
      switchMap(_ => {
          return of(loggedOut());
      }
    )
  );
