import { loginEpic } from "./loginEpic";
import { logoutEpic } from "./logoutEpic";
// import { setUserConnectedEpic } from "./setUserConnectedEpic";
import { combineEpics } from "redux-observable";

export const authenticationEpics = combineEpics<any>(loginEpic, logoutEpic);
