import { Epic, ofType } from "redux-observable";
import { of } from "rxjs";
import {
  ILogin,
  ILoggedIn,
  AuthenticationActionsKind,
  ILoginFail
} from "../types";
import { IRootState } from "../../combiners";
import { switchMap, flatMap, catchError } from "rxjs/operators";
import { ajax } from "rxjs/ajax";
import { loggedIn, loginFail } from "../actions";
import { toast } from "react-toastify";
import * as React from "react";
import Cookies from "universal-cookie";

import { serverURL, errorMessages } from "../../config/constant";

export const loginEpic: Epic<
  ILogin,
  any,
  IRootState,
  ILoggedIn | ILoginFail
> = action$ =>
  action$.pipe(
    ofType(AuthenticationActionsKind.LOGIN),
    switchMap(action => {
      return ajax({
        url: serverURL + "login",
        method: "POST",
        responseType: "json",
        headers: {
          "Content-Type": "application/json"
        },
        body: action.meta
      }).pipe(
        flatMap(res => {
          const cookies = new Cookies();
          cookies.set("tfood-jwt", JSON.stringify(res.response), { path: "/" });
          return of(
            loggedIn({
              id: res.response.id,
              userName: res.response.userName,
              picture: res.response.picture,
              jwt: res.response.jwt,
              roles: res.response.roles,
              address: res.response.address
            })
          );
        }),
        catchError(error => {
          // @ts-ignore
          toast.error(errorMessages[error.status]);
          return of(loginFail(error));
        })
      );
    })
  );
