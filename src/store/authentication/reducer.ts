import {AuthenticationAction, AuthenticationActionsKind, IAuthenticationState} from './types'

export const defaultAuthenticationState: IAuthenticationState = {
    isConnected: false,
    isInvalid: false,
    errorMessage: "",
    userConnected: undefined
};


const authenticationReducer = (
    state: IAuthenticationState = defaultAuthenticationState,
    action: AuthenticationAction
): IAuthenticationState => {
    switch (action.type) {
        case AuthenticationActionsKind.LOGGED_IN :
            return {
                ...state,
                isConnected: true,
                isInvalid: false,
                userConnected: action.payload
            };
        case AuthenticationActionsKind.LOGGED_OUT :
            return {
                ...state,
                isConnected: false
            };
        case AuthenticationActionsKind.LOGIN_FAIL :
            return {
                ...state,
                isInvalid: true,
                errorMessage: action.payload.message
            };
        default:
            return state;
    }
};

export default authenticationReducer;