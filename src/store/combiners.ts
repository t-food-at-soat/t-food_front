import { combineReducers, Reducer } from "redux";

import authenticationReducer, {
  defaultAuthenticationState
} from "./authentication/reducer";
import { IAuthenticationState } from "./authentication/types";
import { combineEpics } from "redux-observable";
import { authenticationEpics } from "./authentication/epics";
import { registerEpics } from "./register/epics";
import offerReducer, { defaultOfferState } from "./offer/reducer";
import { IOfferState } from "./offer/types";
import { offerEpics } from "./offer/epics";
import registerReducer from "./register/reducer";
import { IRegisterState } from "./register/types";
import { defaultRegisterState } from "./register/reducer";
import { resetPasswordEpics } from "./resetPassword/epics";
import {
  resetPasswordActionsKind,
  IGetResetURLState
} from "./resetPassword/types";
import resetPasswordReducer, {
  defaultGetResetURLState
} from "./resetPassword/reducer";
import profileReducer, { defaultProfileState } from "./profile/reducer";
import { IProfileState } from "./profile/types";
import { profileEpics } from "./profile/epics";
import { defaultReservationState } from "./reservation/reducer";
import { IReservationState } from "./reservation/types";
import reservationReducer from "./reservation/reducer";
import { reservationEpics } from "./reservation/epics/index";
import uploadPictureReducer, {
  defaultUploadPictureState
} from "./uploadPicture/reducer";
import { IUploadPictureState } from "./uploadPicture/types";
import { uploadPictureEpics } from "./uploadPicture/epics";

export interface IRootState {
  authentication: IAuthenticationState;
  register: IRegisterState;
  offer: IOfferState;
  resetPassword: IGetResetURLState;
  profile: IProfileState;
  reservation: IReservationState;
  uploadPicture: IUploadPictureState;
}

export const defaultRootState: IRootState = {
  authentication: defaultAuthenticationState,
  register: defaultRegisterState,
  offer: defaultOfferState,
  resetPassword: defaultGetResetURLState,
  profile: defaultProfileState,
  reservation: defaultReservationState,
  uploadPicture: defaultUploadPictureState
};

const rootReducer: Reducer<IRootState> = combineReducers<IRootState>({
  authentication: authenticationReducer,
  register: registerReducer,
  offer: offerReducer,
  resetPassword: resetPasswordReducer,
  profile: profileReducer,
  reservation: reservationReducer,
  uploadPicture: uploadPictureReducer
});

export const rootEpics = combineEpics(
  authenticationEpics,
  registerEpics,
  offerEpics,
  resetPasswordEpics,
  profileEpics,
  reservationEpics,
  uploadPictureEpics
);
export default rootReducer;
