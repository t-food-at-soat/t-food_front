import { getResetPasswordURL } from "./getResetPasswordURL";
import { combineEpics } from "redux-observable";
import { resetPassword } from "./resetPassword";

export const resetPasswordEpics = combineEpics<any>(
  getResetPasswordURL,
  resetPassword
);
