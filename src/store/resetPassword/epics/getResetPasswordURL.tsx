import { Epic, ofType } from "redux-observable";
import { of } from "rxjs";
import { IRootState } from "../../combiners";
import { switchMap, flatMap, catchError } from "rxjs/operators";
import { ajax, AjaxResponse } from "rxjs/ajax";
import { getResetURLSuccess, getResetURLFail } from "../actions";
import { toast } from "react-toastify";
import * as React from "react";

import { serverURL, errorMessages } from "../../config/constant";
import {
  IGetResetURL,
  IGetResetURLFailed,
  IGetResetURLSuccess,
  resetPasswordActionsKind,
  IGetResetURLResponse
} from "../types";

export const getResetPasswordURL: Epic<
  IGetResetURL,
  any,
  IRootState,
  IGetResetURLSuccess | IGetResetURLFailed
> = (action$, store) => {
  return action$.pipe(
    ofType(resetPasswordActionsKind.GET_RESET_URL),
    switchMap(action => {
      return ajax({
        url: serverURL + "getPasswordResetUrl",
        method: "POST",
        responseType: "json",
        headers: {
          "Content-Type": "application/json"
        },
        body: {
          email: action.meta.email
        }
      }).pipe(
        flatMap((res: AjaxResponse) => {
          toast.success("Un email vous a été envoyé");
          return of(getResetURLSuccess(res.response as IGetResetURLResponse));
        }),
        catchError(error => {
          // @ts-ignore
          toast.error(errorMessages[error.status]);
          return of(getResetURLFail(error));
        })
      );
    })
  );
};
