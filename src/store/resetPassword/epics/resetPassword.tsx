import { Epic, ofType } from "redux-observable";
import { of } from "rxjs";
import { IRootState } from "../../combiners";
import { switchMap, flatMap, catchError } from "rxjs/operators";
import { ajax, AjaxResponse } from "rxjs/ajax";
import { getResetURLSuccess, getResetURLFail } from "../actions";
import { toast } from "react-toastify";
import * as React from "react";

import { serverURL, errorMessages } from "../../config/constant";
import {
  IResetPassword,
  IResetPasswordFail,
  IResetPasswordSuccess,
  resetPasswordActionsKind,
  IGetResetURLResponse
} from "../types";

export const resetPassword: Epic<
  IResetPassword,
  any,
  IRootState,
  IResetPasswordSuccess | IResetPasswordFail
> = (action$, store) => {
  return action$.pipe(
    ofType(resetPasswordActionsKind.RESET_PASSWORD),
    switchMap(action => {
      return ajax({
        url: serverURL + "changePwd/" + action.meta.jwt,
        method: "POST",
        responseType: "json",
        headers: {
          "Content-Type": "application/json"
        },
        body: {
          password: action.meta.password
        }
      }).pipe(
        flatMap((res: AjaxResponse) => {
          toast.success("Votre mot de passe a été modifié");
          return of(getResetURLSuccess(res.response as IGetResetURLResponse));
        }),
        catchError(error => {
          // @ts-ignore
          toast.error(errorMessages[error.status]);
          return of(getResetURLFail(error));
        })
      );
    })
  );
};
