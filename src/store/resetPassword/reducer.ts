import {
  ResetPasswordAction,
  resetPasswordActionsKind,
  IGetResetURLState,
  DefaultIGetResetURLResponse
} from "./types";

export const defaultGetResetURLState: IGetResetURLState = {
  response: DefaultIGetResetURLResponse
};

const resetPasswordReducer = (
  state: IGetResetURLState = defaultGetResetURLState,
  action: ResetPasswordAction
): IGetResetURLState => {
  switch (action.type) {
    case resetPasswordActionsKind.GET_RESET_URL:
      return {
        ...state
      };
    case resetPasswordActionsKind.GET_RESET_URL_FAIL:
      return {
        ...state
      };

    case resetPasswordActionsKind.GET_RESET_URL_SUCCESS:
      return {
        ...state
      };
    case resetPasswordActionsKind.RESET_PASSWORD:
      return {
        ...state
      };
    case resetPasswordActionsKind.RESET_PASSWORD_SUCCESS:
      return {
        ...state
      };
    case resetPasswordActionsKind.RESET_PASSWORD_FAIL:
      return {
        ...state
      };
    default:
      return state;
  }
};

export default resetPasswordReducer;
