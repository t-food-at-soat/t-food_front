import {
  resetPasswordActionsKind,
  IGetResetURL,
  IGetResetURLFailed,
  IGetResetURLSuccess,
  IGetResetURLResponse,
  IResetPassword,
  IResetPasswordSuccess,
  IResetPasswordFail
} from "./types";

export const resetPassword = (
  jwt: string,
  password: string
): IResetPassword => ({
  type: resetPasswordActionsKind.RESET_PASSWORD,
  meta: {
    jwt,
    password
  }
});

export const resetPasswordSuccess = (
  payload: IGetResetURLResponse
): IResetPasswordSuccess => ({
  type: resetPasswordActionsKind.RESET_PASSWORD_SUCCESS,
  payload
});

export const resetPasswordFail = (payload: Error): IResetPasswordFail => ({
  type: resetPasswordActionsKind.RESET_PASSWORD_FAIL,
  payload
});
export const getResetURL = (email: string): IGetResetURL => ({
  type: resetPasswordActionsKind.GET_RESET_URL,
  meta: {
    email
  }
});

export const getResetURLSuccess = (
  payload: IGetResetURLResponse
): IGetResetURLSuccess => ({
  type: resetPasswordActionsKind.GET_RESET_URL_SUCCESS,
  payload
});

export const getResetURLFail = (payload: Error): IGetResetURLFailed => ({
  type: resetPasswordActionsKind.GET_RESET_URL_FAIL,
  payload
});
