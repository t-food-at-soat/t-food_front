import { Action } from "redux";
import Offer from "../../ts/interfaces/offer.interfaces";

export interface IGetResetURLResponse {
  status: string;
  statusCode: number;
  message: string;
  data: {
    jwt: string;
  };
}
export const DefaultIGetResetURLResponse: IGetResetURLResponse = {
  status: "default",
  statusCode: 200,
  message: "default",
  data: {
    jwt: "default"
  }
};
export interface IGetResetURLState {
  response: IGetResetURLResponse;
}

export enum resetPasswordActionsKind {
  GET_RESET_URL = "GET_RESET_URL",
  GET_RESET_URL_SUCCESS = "GET_RESET_URL_SUCCESS",
  GET_RESET_URL_FAIL = "GET_RESET_URL_SUCCESS",
  RESET_PASSWORD = "RESET_PASSWORD",
  RESET_PASSWORD_SUCCESS = "RESET_PASSWORD_SUCCESS",
  RESET_PASSWORD_FAIL = "RESET_PASSWORD_FAIL"
}

export interface IResetPassword extends Action {
  readonly type: resetPasswordActionsKind.RESET_PASSWORD;
  readonly meta: {
    jwt: string;
    password: string;
  };
}
export interface IResetPasswordSuccess extends Action {
  readonly type: resetPasswordActionsKind.RESET_PASSWORD_SUCCESS;
  readonly payload: IGetResetURLResponse;
}
export interface IResetPasswordFail extends Action {
  readonly type: resetPasswordActionsKind.RESET_PASSWORD_FAIL;
  readonly payload: Error;
}

export interface IGetResetURL extends Action {
  readonly type: resetPasswordActionsKind.GET_RESET_URL;
  readonly meta: {
    email: string;
  };
}

export interface IGetResetURLSuccess extends Action {
  readonly type: resetPasswordActionsKind.GET_RESET_URL_SUCCESS;
  readonly payload: IGetResetURLResponse;
}

export interface IGetResetURLFailed extends Action {
  readonly type: resetPasswordActionsKind.GET_RESET_URL_FAIL;
  readonly payload: Error;
}

export type ResetPasswordAction =
  | IGetResetURL
  | IGetResetURLSuccess
  | IGetResetURLFailed
  | IResetPassword
  | IResetPasswordSuccess
  | IResetPasswordFail;
