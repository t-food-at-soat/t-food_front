export const serverURL =
  "http://tfood-server.hd3midcur3.eu-west-1.elasticbeanstalk.com/";
//export const serverURL = "http://localhost:3000/";

export const errorMessages = {
  400: "Utilisateur incorrect",
  401: "Vous devez être connecté pour afficher ce contenu",
  403: "Utilisateur incorrect",
  500: "TFood est parti en vacances",
  404: "TFood est introuvable ici"
};
