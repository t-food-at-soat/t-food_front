import {
  OfferActionsKind,
  IGetAllOfferSuccess,
  IGetAllOffer,
  IGetAllOfferSuccessPayload,
  IGetAllOfferFailed,
  IAddOffer,
  IAddOfferFailed,
  IAddOfferSuccess,
  IEditOffer,
  IEditOfferSuccess,
  IEditOfferFailed,
  ICancelOffer,
  ICancelOfferFailed,
  ICancelOfferSuccess,
  IGetUserOffer,
  IGetUserOfferSuccessPayload,
  IGetUserOfferSuccess,
  IGetUserOfferFailed,
  IReserveOffer,
  IReserveOfferSuccess,
  IReserveOfferFailed,
  IOfferSetLoad,
  IGetAllReservation,
  IGetAllReservationSuccess,
  IGetAllReservationFailed
} from "./types";
import Offer from "../../ts/interfaces/offer.interfaces";

export const addOffer = (offer: Offer): IAddOffer => ({
  type: OfferActionsKind.ADD_OFFER,
  meta: {
    offer
  }
});

export const addOfferSuccess = (payload: Offer): IAddOfferSuccess => ({
  type: OfferActionsKind.ADD_OFFER_SUCCESS,
  payload
});

export const addOfferFail = (payload: Error): IAddOfferFailed => ({
  type: OfferActionsKind.ADD_OFFER_FAIL,
  payload
});

export const editOffer = (offer: Offer): IEditOffer => ({
  type: OfferActionsKind.EDIT_OFFER,
  meta: {
    offer
  }
});

export const editOfferSuccess = (payload: Offer): IEditOfferSuccess => ({
  type: OfferActionsKind.EDIT_OFFER_SUCCESS,
  payload
});

export const editOfferFail = (payload: Error): IEditOfferFailed => ({
  type: OfferActionsKind.EDIT_OFFER_FAIL,
  payload
});

export const cancelOffer = (offerId: number): ICancelOffer => ({
  type: OfferActionsKind.CANCEL_OFFER,
  meta: {
    offerId
  }
});

export const cancelOfferSuccess = (payload: Offer): ICancelOfferSuccess => ({
  type: OfferActionsKind.CANCEL_OFFER_SUCCESS,
  payload
});

export const cancelOfferFail = (payload: Error): ICancelOfferFailed => ({
  type: OfferActionsKind.CANCEL_OFFER_FAIL,
  payload
});

export const getAllOffer = (): IGetAllOffer => ({
  type: OfferActionsKind.GET_ALL_OFFER
});

export const getAllOfferSuccess = (
  payload: IGetAllOfferSuccessPayload
): IGetAllOfferSuccess => ({
  type: OfferActionsKind.GET_ALL_OFFER_SUCCESS,
  payload
});

export const getAllOfferFailed = (payload: Error): IGetAllOfferFailed => ({
  type: OfferActionsKind.GET_ALL_OFFER_FAILED,
  payload
});

export const getUserOffer = (): IGetUserOffer => ({
  type: OfferActionsKind.GET_USER_OFFER
});

export const getUserOfferSuccess = (
  payload: IGetUserOfferSuccessPayload
): IGetUserOfferSuccess => ({
  type: OfferActionsKind.GET_USER_OFFER_SUCCESS,
  payload
});

export const getUserOfferFailed = (payload: Error): IGetUserOfferFailed => ({
  type: OfferActionsKind.GET_USER_OFFER_FAILED,
  payload
});

export const reserveOffer = (id: number): IReserveOffer => ({
  type: OfferActionsKind.RESERVE_OFFER,
  meta: {
    id
  }
});

export const reserveOfferSuccess = (id: number): IReserveOfferSuccess => ({
  type: OfferActionsKind.RESERVE_OFFER_SUCCESS,
  payload: {
    id
  }
});

export const reserveOfferFail = (payload: Error): IReserveOfferFailed => ({
  type: OfferActionsKind.RESERVE_OFFER_FAIL,
  payload
});

export const setOfferIsLoad = (payload: boolean): IOfferSetLoad => ({
  type: OfferActionsKind.SET_LOAD,
  payload
});

export const getAllReservation = (): IGetAllReservation => ({
  type: OfferActionsKind.GET_ALL_RESERVATION
});

export const getAllReservationSuccess = (
  payload: IGetAllOfferSuccessPayload
): IGetAllReservationSuccess => ({
  type: OfferActionsKind.GET_ALL_RESERVATION_SUCCESS,
  payload
});

export const getAllReservationFailed = (
  payload: Error
): IGetAllReservationFailed => ({
  type: OfferActionsKind.GET_ALL_RESERVATION_FAILED,
  payload
});
