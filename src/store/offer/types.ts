import { Action } from "redux";
import Offer from "../../ts/interfaces/offer.interfaces";

export interface IOfferState {
  offerList: Array<Offer>;
  isLoad: boolean;
  errorMessage: string;
}

export enum OfferActionsKind {
  ADD_OFFER = "OFFER_ADD_OFFER",
  ADD_OFFER_SUCCESS = "OFFER_ADD_OFFER_SUCCESS",
  ADD_OFFER_FAIL = "OFFER_ADD_OFFER_FAIL",
  EDIT_OFFER = "OFFER_EDIT_OFFER",
  EDIT_OFFER_SUCCESS = "OFFER_EDIT_OFFER_SUCCESS",
  EDIT_OFFER_FAIL = "OFFER_EDIT_OFFER_FAIL",
  CANCEL_OFFER = "OFFER_CANCEL_OFFER",
  CANCEL_OFFER_SUCCESS = "OFFER_CANCEL_OFFER_SUCCESS",
  CANCEL_OFFER_FAIL = "OFFER_CANCEL_OFFER_FAIL",
  GET_ALL_OFFER = "OFFER_GET_ALL_OFFER",
  GET_ALL_OFFER_SUCCESS = "OFFER_GET_ALL_OFFER_SUCCESS",
  GET_ALL_OFFER_FAILED = "OFFER_GET_ALL_OFFER_FAILED",
  GET_USER_OFFER = "OFFER_GET_USER_OFFER",
  GET_USER_OFFER_SUCCESS = "OFFER_GET_USER_OFFER_SUCCESS",
  GET_USER_OFFER_FAILED = "OFFER_GET_USER_OFFER_FAILED",
  RESERVE_OFFER = "OFFER_RESERVE_OFFER",
  RESERVE_OFFER_SUCCESS = "OFFER_RESERVE_OFFER_SUCCESS",
  RESERVE_OFFER_FAIL = "OFFER_RESERVE_OFFER_FAIL",
  SET_LOAD = "OFFER_SET_ISLOAD",
  GET_ALL_RESERVATION = "OFFER_GET_ALL_RESERVATION",
  GET_ALL_RESERVATION_SUCCESS = "OFFER_GET_ALL_RESERVATION_SUCCESS",
  GET_ALL_RESERVATION_FAILED = "OFFER_GET_ALL_RESERVATION_FAILED"
}

export interface IAddOffer extends Action {
  readonly type: OfferActionsKind.ADD_OFFER;
  readonly meta: {
    offer: Offer;
  };
}

export interface IAddOfferSuccess extends Action {
  readonly type: OfferActionsKind.ADD_OFFER_SUCCESS;
  readonly payload: Offer;
}

export interface IAddOfferFailed extends Action {
  readonly type: OfferActionsKind.ADD_OFFER_FAIL;
  readonly payload: Error;
}

export interface IEditOffer extends Action {
  readonly type: OfferActionsKind.EDIT_OFFER;
  readonly meta: {
    offer: Offer;
  };
}

export interface IEditOfferSuccess extends Action {
  readonly type: OfferActionsKind.EDIT_OFFER_SUCCESS;
  readonly payload: Offer;
}

export interface IEditOfferFailed extends Action {
  readonly type: OfferActionsKind.EDIT_OFFER_FAIL;
  readonly payload: Error;
}

export interface ICancelOffer extends Action {
  readonly type: OfferActionsKind.CANCEL_OFFER;
  readonly meta: {
    offerId: number;
  };
}

export interface ICancelOfferSuccess extends Action {
  readonly type: OfferActionsKind.CANCEL_OFFER_SUCCESS;
  readonly payload: Offer;
}

export interface ICancelOfferFailed extends Action {
  readonly type: OfferActionsKind.CANCEL_OFFER_FAIL;
  readonly payload: Error;
}

export interface IGetAllOffer extends Action {
  readonly type: OfferActionsKind.GET_ALL_OFFER;
}

export interface IGetAllOfferSuccessPayload {
  data: Array<Offer>;
}

export interface IGetAllOfferSuccess extends Action {
  readonly type: OfferActionsKind.GET_ALL_OFFER_SUCCESS;
  readonly payload: IGetAllOfferSuccessPayload;
}

export interface IGetAllOfferFailed extends Action {
  readonly type: OfferActionsKind.GET_ALL_OFFER_FAILED;
  readonly payload: Error;
}

export interface IGetUserOffer extends Action {
  readonly type: OfferActionsKind.GET_USER_OFFER;
}

export interface IGetUserOfferSuccessPayload {
  data: Array<Offer>;
}

export interface IGetUserOfferSuccess extends Action {
  readonly type: OfferActionsKind.GET_USER_OFFER_SUCCESS;
  readonly payload: IGetUserOfferSuccessPayload;
}

export interface IGetUserOfferFailed extends Action {
  readonly type: OfferActionsKind.GET_USER_OFFER_FAILED;
  readonly payload: Error;
}

export interface IReserveOffer extends Action {
  readonly type: OfferActionsKind.RESERVE_OFFER;
  readonly meta: {
    id: number;
  };
}

export interface IReserveOfferSuccessPayload {
  id: number;
}

export interface IReserveOfferSuccess extends Action {
  readonly type: OfferActionsKind.RESERVE_OFFER_SUCCESS;
  readonly payload: IReserveOfferSuccessPayload;
}

export interface IReserveOfferFailed extends Action {
  readonly type: OfferActionsKind.RESERVE_OFFER_FAIL;
  readonly payload: Error;
}

export interface IOfferSetLoad extends Action {
  readonly type: OfferActionsKind.SET_LOAD;
  readonly payload: boolean;
}

export interface IGetAllReservation extends Action {
  readonly type: OfferActionsKind.GET_ALL_RESERVATION;
}

export interface IGetAllReservationSuccess extends Action {
  readonly type: OfferActionsKind.GET_ALL_RESERVATION_SUCCESS;
  readonly payload: IGetUserOfferSuccessPayload;
}

export interface IGetAllReservationFailed extends Action {
  readonly type: OfferActionsKind.GET_ALL_RESERVATION_FAILED;
  readonly payload: Error;
}

export type OfferAction =
  | IAddOffer
  | IAddOfferSuccess
  | IAddOfferFailed
  | IEditOffer
  | IEditOfferSuccess
  | IEditOfferFailed
  | ICancelOffer
  | ICancelOfferSuccess
  | ICancelOfferFailed
  | IGetAllOffer
  | IGetAllOfferSuccess
  | IGetAllOfferFailed
  | IGetUserOffer
  | IGetUserOfferSuccess
  | IGetUserOfferFailed
  | IReserveOffer
  | IReserveOfferSuccess
  | IReserveOfferFailed
  | IOfferSetLoad
  | IGetAllReservation
  | IGetAllReservationSuccess
  | IGetAllReservationFailed;
