import { getAllOfferEpic } from "./getAllOfferEpic";
import { getUserOfferEpic } from "./getUserOfferEpic";
import { combineEpics } from "redux-observable";
import { addOfferEpic } from "./addOfferEpic";
import { reserveOfferEpic } from "./reserveOfferEpic";
import { getAllReservationEpic } from "./getAllReservationEpic";
import { editOfferEpic } from "./editOfferEpic";

export const offerEpics = combineEpics<any>(
  addOfferEpic,
  editOfferEpic,
  getUserOfferEpic,
  getAllOfferEpic,
  reserveOfferEpic,
  getAllReservationEpic
);
