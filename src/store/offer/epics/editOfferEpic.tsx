import { Epic, ofType } from "redux-observable";
import { of } from "rxjs";
import { IRootState } from "../../combiners";
import { switchMap, flatMap, catchError } from "rxjs/operators";
import { ajax } from "rxjs/ajax";
import { editOfferSuccess, editOfferFail } from "../actions";
import { toast } from "react-toastify";

import { serverURL, errorMessages } from "../../config/constant";
import {
  IEditOffer,
  IEditOfferFailed,
  IEditOfferSuccess,
  OfferActionsKind
} from "../types";
import { navigate } from "hookrouter";

export const editOfferEpic: Epic<
  IEditOffer,
  any,
  IRootState,
  IEditOfferSuccess | IEditOfferFailed
> = (action$, store) => {
  return action$.pipe(
    ofType(OfferActionsKind.EDIT_OFFER),
    switchMap(action => {
      const jwt = store.value.authentication.userConnected
        ? store.value.authentication.userConnected.jwt
        : "noJWT";
      return ajax({
        url: serverURL + "updateOffer",
        method: "PUT",
        responseType: "json",
        headers: {
          "Content-Type": "application/json",
          Authorization: jwt
        },
        body: {
          ...action.meta.offer
        }
      }).pipe(
        flatMap(res => {
          toast.success("Offre modifiée !");
          navigate("/offer");
          return of(editOfferSuccess(action.meta.offer));
        }),
        catchError(error => {
          // @ts-ignore
          toast.error(errorMessages[error.status]);
          return of(editOfferFail(error));
        })
      );
    })
  );
};
