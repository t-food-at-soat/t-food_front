import { Epic, ofType } from "redux-observable";
import { concat, of } from "rxjs";
import {
  IGetUserOffer,
  IGetUserOfferSuccess,
  OfferActionsKind
} from "../types";
import { IRootState } from "../../combiners";
import { switchMap, flatMap, catchError } from "rxjs/operators";
import { ajax } from "rxjs/ajax";
import {
  getUserOfferSuccess,
  getUserOfferFailed,
  setOfferIsLoad
} from "../actions";
import { toast } from "react-toastify";
import { serverURL, errorMessages } from "../../config/constant";

export const getUserOfferEpic: Epic<
  IGetUserOffer,
  any,
  IRootState,
  IGetUserOfferSuccess
> = (action$, store) =>
  action$.pipe(
    ofType(OfferActionsKind.GET_USER_OFFER),
    switchMap(_ => {
      const storeValueAuthUserConnected =
        store.value.authentication.userConnected;
      const jwt = storeValueAuthUserConnected
        ? storeValueAuthUserConnected.jwt
        : "noJWT";
      const userName = storeValueAuthUserConnected
        ? storeValueAuthUserConnected.userName
        : "";
      return ajax({
        url: `${serverURL}findAllOrdersByAuthor/${userName}`,
        method: "GET",
        responseType: "json",
        headers: {
          "Content-Type": "application/json",
          Authorization: jwt
        }
      }).pipe(
        flatMap(res => {
          return concat(
            of(getUserOfferSuccess(res.response)),
            of(setOfferIsLoad(true))
          );
        }),
        catchError(error => {
          // @ts-ignore
          toast.error(errorMessages[error.status]);
          return of(getUserOfferFailed(error));
        })
      );
    })
  );
