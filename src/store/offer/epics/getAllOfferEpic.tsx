import { Epic, ofType } from "redux-observable";
import { of, concat } from "rxjs";
import { IGetAllOffer, IGetAllOfferSuccess, OfferActionsKind } from "../types";
import { IRootState } from "../../combiners";
import { switchMap, flatMap, catchError } from "rxjs/operators";
import { ajax } from "rxjs/ajax";
import {
  getAllOfferSuccess,
  getAllOfferFailed,
  setOfferIsLoad
} from "../actions";
import { toast } from "react-toastify";
import { serverURL, errorMessages } from "../../config/constant";

export const getAllOfferEpic: Epic<
  IGetAllOffer,
  any,
  IRootState,
  IGetAllOfferSuccess
> = (action$, store) =>
  action$.pipe(
    ofType(OfferActionsKind.GET_ALL_OFFER),
    switchMap(_ => {
      const jwt = store.value.authentication.userConnected
        ? store.value.authentication.userConnected.jwt
        : "noJWT";
      return ajax({
        url: serverURL + "offers",
        method: "GET",
        responseType: "json",
        headers: {
          "Content-Type": "application/json",
          Authorization: jwt
        }
      }).pipe(
        flatMap(res => {
          return concat(
            of(getAllOfferSuccess(res.response)),
            of(setOfferIsLoad(true))
          );
        }),
        catchError(error => {
          // @ts-ignore
          toast.error(errorMessages[error.status]);
          return of(getAllOfferFailed(error));
        })
      );
    })
  );
