import { Epic, ofType } from "redux-observable";
import { of, concat } from "rxjs";
import {
  IGetAllReservation,
  IGetAllReservationFailed,
  IGetAllReservationSuccess,
  OfferActionsKind
} from "../types";
import { IRootState } from "../../combiners";
import { switchMap, flatMap, catchError } from "rxjs/operators";
import { ajax } from "rxjs/ajax";
import {
  getAllReservationSuccess,
  getAllReservationFailed,
  setOfferIsLoad
} from "../actions";
import { toast } from "react-toastify";
import * as React from "react";
import { serverURL, errorMessages } from "../../config/constant";

export const getAllReservationEpic: Epic<
  IGetAllReservation,
  any,
  IRootState,
  IGetAllReservationSuccess | IGetAllReservationFailed
> = (action$, store) =>
  action$.pipe(
    ofType(OfferActionsKind.GET_ALL_RESERVATION),
    switchMap(_ => {
      const jwt = store.value.authentication.userConnected
        ? store.value.authentication.userConnected.jwt
        : "noJWT";
      return ajax({
        url: serverURL + "reservations",
        method: "GET",
        responseType: "json",
        headers: {
          "Content-Type": "application/json",
          Authorization: jwt
        }
      }).pipe(
        flatMap(res => {
          return concat(
            of(getAllReservationSuccess(res.response)),
            of(setOfferIsLoad(true))
          );
        }),
        catchError(error => {
          // @ts-ignore
          toast.error(errorMessages[error.status]);
          return of(getAllReservationFailed(error));
        })
      );
    })
  );
