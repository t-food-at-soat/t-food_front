import { Epic, ofType } from "redux-observable";
import { of } from "rxjs";
import { IRootState } from "../../combiners";
import { switchMap, flatMap, catchError } from "rxjs/operators";
import { ajax } from "rxjs/ajax";
import { reserveOfferSuccess, reserveOfferFail } from "../actions";
import { toast } from "react-toastify";

import { serverURL, errorMessages } from "../../config/constant";
import {
  IReserveOffer,
  IReserveOfferFailed,
  IReserveOfferSuccess,
  OfferActionsKind
} from "../types";
import { navigate } from "hookrouter";

export const reserveOfferEpic: Epic<
  IReserveOffer,
  any,
  IRootState,
  IReserveOfferSuccess | IReserveOfferFailed
> = (action$, store) => {
  return action$.pipe(
    ofType(OfferActionsKind.RESERVE_OFFER),
    switchMap(action => {
      const jwt = store.value.authentication.userConnected
        ? store.value.authentication.userConnected.jwt
        : "noJWT";
      return ajax({
        url: serverURL + "reserveOffer",
        method: "POST",
        responseType: "json",
        headers: {
          "Content-Type": "application/json",
          Authorization: jwt
        },
        body: {
          id: action.meta.id
        }
      }).pipe(
        flatMap(() => {
          navigate("/reservation/success");
          return of(reserveOfferSuccess(action.meta.id));
        }),
        catchError(error => {
          // @ts-ignore
          toast.error(errorMessages[error.status]);
          return of(reserveOfferFail(error));
        })
      );
    })
  );
};
