import { Epic, ofType } from "redux-observable";
import { of } from "rxjs";
import { IRootState } from "../../combiners";
import { switchMap, flatMap, catchError } from "rxjs/operators";
import { ajax } from "rxjs/ajax";
import { addOfferSuccess, addOfferFail } from "../actions";
import { toast } from "react-toastify";
import { errorMessages } from "../../config/constant";

import { serverURL } from "../../config/constant";
import {
  IAddOffer,
  IAddOfferFailed,
  IAddOfferSuccess,
  OfferActionsKind
} from "../types";
import Offer from "../../../ts/interfaces/offer.interfaces";

export const addOfferEpic: Epic<
  IAddOffer,
  any,
  IRootState,
  IAddOfferSuccess | IAddOfferFailed
> = (action$, store) => {
  return action$.pipe(
    ofType(OfferActionsKind.ADD_OFFER),
    switchMap(action => {
      console.log(action.meta.offer);
      const jwt = store.value.authentication.userConnected
        ? store.value.authentication.userConnected.jwt
        : "noJWT";
      return ajax({
        url: serverURL + "createOffer",
        method: "POST",
        responseType: "json",
        headers: {
          "Content-Type": "application/json",
          Authorization: jwt
        },
        body: {
          ...action.meta.offer
        }
      }).pipe(
        flatMap(res => {
          toast.success("Offre ajoutée !");
          return of(
            addOfferSuccess({
              ...action.meta.offer,
              id: res.response.insertedId
            } as Offer)
          );
        }),
        catchError(error => {
          // @ts-ignore
          toast.error(errorMessages[error.status]);
          return of(addOfferFail(error));
        })
      );
    })
  );
};
