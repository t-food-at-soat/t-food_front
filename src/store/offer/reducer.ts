import { OfferAction, OfferActionsKind, IOfferState } from "./types";

export const defaultOfferState: IOfferState = {
  offerList: [],
  isLoad: false,
  errorMessage: ""
};

const offerReducer = (
  state: IOfferState = defaultOfferState,
  action: OfferAction
): IOfferState => {
  switch (action.type) {
    case OfferActionsKind.ADD_OFFER_SUCCESS:
      return {
        ...state,
        offerList: [...state.offerList, action.payload]
      };
    case OfferActionsKind.ADD_OFFER_FAIL:
      return {
        ...state,
        errorMessage: action.payload.message
      };
    case OfferActionsKind.EDIT_OFFER_SUCCESS:
      return {
        ...state,
        offerList: [...state.offerList, action.payload]
      };
    case OfferActionsKind.EDIT_OFFER_FAIL:
      return {
        ...state,
        errorMessage: action.payload.message
      };
    case OfferActionsKind.CANCEL_OFFER_SUCCESS:
      return {
        ...state
      };
    case OfferActionsKind.CANCEL_OFFER_FAIL:
      return {
        ...state,
        errorMessage: action.payload.message
      };
    case OfferActionsKind.GET_ALL_OFFER_SUCCESS:
      return {
        ...state,
        offerList: action.payload.data
      };
    case OfferActionsKind.GET_ALL_RESERVATION_SUCCESS:
      return {
        ...state,
        offerList: action.payload.data
      };
    case OfferActionsKind.GET_USER_OFFER_SUCCESS:
      return {
        ...state,
        offerList: action.payload.data
      };
    case OfferActionsKind.RESERVE_OFFER_SUCCESS:
      return {
        ...state,
        offerList: state.offerList.filter(
          offer => offer.id !== action.payload.id
        )
      };
    case OfferActionsKind.SET_LOAD:
      return {
        ...state,
        isLoad: action.payload
      };
    default:
      return state;
  }
};

export default offerReducer;
