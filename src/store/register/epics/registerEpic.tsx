import { Epic, ofType } from "redux-observable";
import { of } from "rxjs";
import {
  IRegister,
  IRegistered,
  IRegisterFail,
  RegisterActionsKind
} from "../types";
import { IRootState } from "../../combiners";
import { switchMap, flatMap, catchError } from "rxjs/operators";
import { ajax } from "rxjs/ajax";
import { registered, registerFail } from "../actions";
import { toast } from "react-toastify";
import * as React from "react";

import { serverURL, errorMessages } from "../../config/constant";
import { navigate } from "hookrouter";

export const registerEpic: Epic<
  IRegister,
  any,
  IRootState,
  IRegistered | IRegisterFail
> = action$ => {
  return action$.pipe(
    ofType(RegisterActionsKind.REGISTER),
    switchMap(action => {
      console.log(action.meta.user);
      return ajax({
        url: serverURL + "register",
        method: "POST",
        responseType: "json",
        headers: {
          "Content-Type": "application/json"
        },
        body: action.meta.user
      }).pipe(
        flatMap(res => {
          navigate("/register/success");
          return of(registered());
        }),
        catchError(error => {
          // @ts-ignore
          toast.error(errorMessages[error.status]);
          return of(registerFail(error));
        })
      );
    })
  );
};
