import { combineEpics } from "redux-observable";
import { registerEpic } from "./registerEpic";

export const registerEpics = combineEpics<any>(registerEpic);
