import { Action } from "redux";
import User from "../../ts/interfaces/user.interfaces";

export interface IRegisterState {
  isRegistered: boolean;
  errorMessage: string;
}

export enum RegisterActionsKind {
  REGISTER = "REGISTER",
  REGISTERED = "REGISTERED",
  REGISTER_FAIL = "REGISTER_FAIL"
}

export interface IRegister extends Action {
  readonly type: RegisterActionsKind.REGISTER;
  readonly meta: {
    user: User;
  };
}

export interface IRegistered extends Action {
  readonly type: RegisterActionsKind.REGISTERED;
}

export interface IRegisterFail extends Action {
  readonly type: RegisterActionsKind.REGISTER_FAIL;
  readonly payload: Error;
}

export type RegisterAction = IRegister | IRegistered | IRegisterFail;
