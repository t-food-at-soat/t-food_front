import {
  RegisterActionsKind,
  IRegistered,
  IRegister,
  IRegisterFail
} from "./types";
import User from "../../ts/interfaces/user.interfaces";

export const register = (user: User): IRegister => {
  return {
    type: RegisterActionsKind.REGISTER,
    meta: {
      user
    }
  };
};

export const registered = (): IRegistered => ({
  type: RegisterActionsKind.REGISTERED
});

export const registerFail = (payload: Error): IRegisterFail => ({
  type: RegisterActionsKind.REGISTER_FAIL,
  payload
});
