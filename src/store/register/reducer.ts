import { IRegisterState, RegisterActionsKind, RegisterAction } from "./types";

export const defaultRegisterState: IRegisterState = {
  isRegistered: false,
  errorMessage: ""
};

const registerReducer = (
  state: IRegisterState = defaultRegisterState,
  action: RegisterAction
): IRegisterState => {
  switch (action.type) {
    case RegisterActionsKind.REGISTERED:
      return {
        ...state,
        isRegistered: true
      };
    case RegisterActionsKind.REGISTER_FAIL:
      return {
        ...state,
        errorMessage: action.payload.message
      };
    default:
      return state;
  }
};

export default registerReducer;
