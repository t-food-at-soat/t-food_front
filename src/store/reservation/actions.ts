import {
  ReservationActionsKind,
  ICancelReservation,
  ICancelReservationSuccess,
  ICancelReservationFailed
} from "./types";

export const cancelReservation = (
  reservationId: number
): ICancelReservation => ({
  type: ReservationActionsKind.CANCEL_RESERVATION,
  meta: {
    reservationId
  }
});

export const cancelReservationSuccess = (): ICancelReservationSuccess => ({
  type: ReservationActionsKind.CANCEL_RESERVATION_SUCCESS
});

export const cancelReservationFail = (
  payload: Error
): ICancelReservationFailed => ({
  type: ReservationActionsKind.CANCEL_RESERVATION_FAIL,
  payload
});
