import { combineEpics } from "redux-observable";
import { cancelReservationEpic } from "./cancelReservationEpic";

export const reservationEpics = combineEpics<any>(cancelReservationEpic);
