import { Epic, ofType } from "redux-observable";
import { of } from "rxjs";
import { IRootState } from "../../combiners";
import { switchMap, flatMap, catchError } from "rxjs/operators";
import { ajax } from "rxjs/ajax";
import { toast } from "react-toastify";

import { serverURL, errorMessages } from "../../config/constant";

import { navigate } from "hookrouter";
import {
  ICancelReservation,
  ICancelReservationSuccess,
  ICancelReservationFailed,
  ReservationActionsKind
} from "../types";
import { cancelReservationSuccess, cancelReservationFail } from "../actions";

export const cancelReservationEpic: Epic<
  ICancelReservation,
  any,
  IRootState,
  ICancelReservationSuccess | ICancelReservationFailed
> = (action$, store) => {
  return action$.pipe(
    ofType(ReservationActionsKind.CANCEL_RESERVATION),
    switchMap(action => {
      const jwt = store.value.authentication.userConnected
        ? store.value.authentication.userConnected.jwt
        : "noJWT";
      return ajax({
        url: serverURL + "cancelReservation",
        method: "PUT",
        responseType: "json",
        headers: {
          "Content-Type": "application/json",
          Authorization: jwt
        },
        body: {
          id: action.meta.reservationId
        }
      }).pipe(
        flatMap(res => {
          toast.success("Réservation annulée !");
          navigate("/profile");
          return of(cancelReservationSuccess());
        }),
        catchError(error => {
          // @ts-ignore
          toast.error(errorMessages[error.status]);
          return of(cancelReservationFail(error));
        })
      );
    })
  );
};
