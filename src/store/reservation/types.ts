import { Action } from "redux";

export interface IReservationState {
  errorMessage: string;
}

export enum ReservationActionsKind {
  CANCEL_RESERVATION = "RESERVATION_CANCEL_RESERVATION",
  CANCEL_RESERVATION_SUCCESS = "RESERVATION_CANCEL_RESERVATION_SUCCESS",
  CANCEL_RESERVATION_FAIL = "RESERVATION_CANCEL_RESERVATION_FAIL",
  CANCEL_OFFER = "CANCEL_OFFER"
}

export interface ICancelReservation extends Action {
  readonly type: ReservationActionsKind.CANCEL_RESERVATION;
  readonly meta: {
    reservationId: number;
  };
}

export interface ICancelReservationSuccess extends Action {
  readonly type: ReservationActionsKind.CANCEL_RESERVATION_SUCCESS;
}

export interface ICancelReservationFailed extends Action {
  readonly type: ReservationActionsKind.CANCEL_RESERVATION_FAIL;
  readonly payload: Error;
}

export type ReservationActions =
  | ICancelReservation
  | ICancelReservationSuccess
  | ICancelReservationFailed;
