import {
  IReservationState,
  ReservationActionsKind,
  ReservationActions
} from "./types";

export const defaultReservationState: IReservationState = {
  errorMessage: ""
};

const reservationReducer = (
  state: IReservationState = defaultReservationState,
  action: ReservationActions
): IReservationState => {
  switch (action.type) {
    case ReservationActionsKind.CANCEL_RESERVATION_SUCCESS:
      return {
        ...state
      };
    case ReservationActionsKind.CANCEL_RESERVATION_FAIL:
      return {
        ...state,
        errorMessage: action.payload.message
      };

    default:
      return state;
  }
};

export default reservationReducer;
