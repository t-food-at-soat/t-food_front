import {
  IProfileGetProfileById,
  ProfileActionsKind,
  IProfileGetProfileByIdSuccess,
  IProfileGetProfileByIdFailed,
  IProfileSetLoad,
  IProfileGetProfileByIdSuccessPayload,
  IProfileUpdateProfile,
  IProfileUpdateProfileSuccess, IProfileUpdateProfileFailed
} from "./types";
import {} from "./types";
import User from "../../ts/interfaces/user.interfaces";

export const getProfileById = (id: number): IProfileGetProfileById => ({
  type: ProfileActionsKind.GET_PROFILE_BY_ID,
  meta: {
    id
  }
});

export const getProfileByIdSuccess = (payload : IProfileGetProfileByIdSuccessPayload): IProfileGetProfileByIdSuccess => ({
  type: ProfileActionsKind.GET_PROFILE_BY_ID_SUCCESS,
  payload
});

export const getProfileByIdFailed = (payload: Error): IProfileGetProfileByIdFailed => ({
  type: ProfileActionsKind.GET_PROFILE_BY_ID_FAILED,
  payload
});

export const setProfileIsLoad = (payload: boolean): IProfileSetLoad => ({
  type: ProfileActionsKind.SET_LOAD,
  payload
});

export const updateProfile = (user: User): IProfileUpdateProfile => ({
  type: ProfileActionsKind.UPDATE_PROFILE,
  meta: user
});

export const updateProfileSuccess = (): IProfileUpdateProfileSuccess => ({
  type: ProfileActionsKind.UPDATE_PROFILE_SUCCESS,
});

export const updateProfiledFailed = (payload: Error): IProfileUpdateProfileFailed => ({
  type: ProfileActionsKind.UPDATE_PROFILE_FAILED,
  payload
});