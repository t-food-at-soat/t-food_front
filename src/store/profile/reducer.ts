import {IProfileState, ProfileAction, ProfileActionsKind} from "./types";
import User from "../../ts/interfaces/user.interfaces";


export const defaultProfileState: IProfileState = {
  currentProfile: new User(),
  isLoad: false,
  errorMessage: ""
};

const profileReducer = (
  state: IProfileState = defaultProfileState,
  action: ProfileAction
): IProfileState => {
  switch (action.type) {
    case ProfileActionsKind.GET_PROFILE_BY_ID_SUCCESS:
      return {
        ...state,
        currentProfile: action.payload.data
      };
    case ProfileActionsKind.GET_PROFILE_BY_ID_FAILED:
      return {
        ...state,
        errorMessage: action.payload.message
      };
    case ProfileActionsKind.SET_LOAD:
      return {
        ...state,
        isLoad: action.payload
      };
    default:
      return state;
  }
};

export default profileReducer;
