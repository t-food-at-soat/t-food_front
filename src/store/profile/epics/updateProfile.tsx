import { Epic, ofType } from "redux-observable";
import { concat, of } from "rxjs";
import { IRootState } from "../../combiners";
import { catchError, flatMap, switchMap } from "rxjs/operators";
import { ajax } from "rxjs/ajax";
import { toast } from "react-toastify";
import { serverURL, errorMessages } from "../../config/constant";
import {
  IProfileUpdateProfile,
  IProfileUpdateProfileFailed,
  IProfileUpdateProfileSuccess,
  ProfileActionsKind
} from "../types";
import { updateProfiledFailed, updateProfileSuccess } from "../actions";
import { loggedIn } from "../../authentication/actions";
import Roles from "../../../ts/types/roles.types";
import Cookies from "universal-cookie/es6";

export const updateProfileEpic: Epic<
  IProfileUpdateProfile,
  any,
  IRootState,
  IProfileUpdateProfileSuccess | IProfileUpdateProfileFailed
> = (action$, store) =>
  action$.pipe(
    ofType(ProfileActionsKind.UPDATE_PROFILE),
    switchMap(action => {
      const jwt = store.value.authentication.userConnected
        ? store.value.authentication.userConnected.jwt
        : "noJWT";
      const userConnected = store.value.authentication.userConnected;
      return ajax({
        url: serverURL + "editProfileInfo",
        method: "PUT",
        responseType: "json",
        headers: {
          "Content-Type": "application/json",
          Authorization: jwt
        },
        body: {
          ...action.meta
        }
      }).pipe(
        flatMap(res => {
          toast.success("Profil modifié !");
          const updatedUser = {
            id: userConnected && userConnected.id ? userConnected.id : "",
            userName:
              userConnected && userConnected.userName
                ? userConnected.userName
                : "",
            picture:
              (userConnected && userConnected.picture) !== action.meta.picture
                ? action.meta.picture
                  ? action.meta.picture
                  : ""
                : userConnected && userConnected.picture
                ? userConnected.picture
                : "",
            jwt: jwt,
            roles:
              userConnected && userConnected.roles
                ? userConnected.roles
                : Roles.INDIVIDUAL,
            address:
              (userConnected && userConnected.address) !== action.meta.address
                ? action.meta.address
                  ? action.meta.address
                  : ""
                : userConnected && userConnected.address
                ? userConnected.address
                : ""
          };
          const cookies = new Cookies();
          cookies.set("tfood-jwt", JSON.stringify(updatedUser), { path: "/" });
          return concat(of(loggedIn(updatedUser)), of(updateProfileSuccess()));
        }),
        catchError(error => {
          // @ts-ignore
          toast.error(errorMessages[error.status]);
          return of(updateProfiledFailed(error));
        })
      );
    })
  );
