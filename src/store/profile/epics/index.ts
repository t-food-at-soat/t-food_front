import {getProfileByIdEpic} from "./getProfileById";
import {updateProfileEpic} from "./updateProfile";
import {combineEpics} from "redux-observable";

export const profileEpics = combineEpics<any>(
  getProfileByIdEpic,
  updateProfileEpic
);
