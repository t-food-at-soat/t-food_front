import { Epic, ofType } from "redux-observable";
import { of, concat } from "rxjs";
import { IRootState } from "../../combiners";
import { switchMap, flatMap, catchError } from "rxjs/operators";
import { ajax } from "rxjs/ajax";
import { toast } from "react-toastify";
import { serverURL, errorMessages } from "../../config/constant";
import {
  IProfileGetProfileById,
  IProfileGetProfileByIdFailed,
  IProfileGetProfileByIdSuccess,
  ProfileActionsKind
} from "../types";
import {
  getProfileByIdFailed,
  getProfileByIdSuccess,
  setProfileIsLoad
} from "../actions";

export const getProfileByIdEpic: Epic<
  IProfileGetProfileById,
  any,
  IRootState,
  IProfileGetProfileByIdSuccess | IProfileGetProfileByIdFailed
> = (action$, store) =>
  action$.pipe(
    ofType(ProfileActionsKind.GET_PROFILE_BY_ID),
    switchMap(action => {
      const jwt = store.value.authentication.userConnected
        ? store.value.authentication.userConnected.jwt
        : "noJWT";
      return ajax({
        url: serverURL + "profileInfo/" + action.meta.id,
        method: "GET",
        responseType: "json",
        headers: {
          "Content-Type": "application/json",
          Authorization: jwt
        }
      }).pipe(
        flatMap(res => {
          return concat(
            of(getProfileByIdSuccess(res.response)),
            of(setProfileIsLoad(true))
          );
        }),
        catchError(error => {
          // @ts-ignore
          toast.error(errorMessages[error.status]);
          return of(getProfileByIdFailed(error));
        })
      );
    })
  );
