import { Action } from "redux";
import User from "../../ts/interfaces/user.interfaces";

export interface IProfileState {
  currentProfile: User;
  isLoad: boolean;
  errorMessage: string;
}

export enum ProfileActionsKind {
  GET_PROFILE_BY_ID = "PROFILE_GET_PROFILE_BY_ID",
  GET_PROFILE_BY_ID_SUCCESS = "PROFILE_GET_PROFILE_BY_ID_SUCCESS",
  GET_PROFILE_BY_ID_FAILED = "PROFILE_GET_PROFILE_BY_ID_FAILED",
  SET_LOAD = "PROFILE_SET_LOAD",
  UPDATE_PROFILE = "PROFILE_UPDATE_PROFILE",
  UPDATE_PROFILE_SUCCESS = "PROFILE_UPDATE_PROFILE_SUCCESS",
  UPDATE_PROFILE_FAILED = "PROFILE_UPDATE_PROFILE_FAILED"
}

export interface IProfileSetLoad extends Action {
  readonly type: ProfileActionsKind.SET_LOAD;
  readonly payload: boolean;
}

export interface IProfileGetProfileById extends Action {
  readonly type: ProfileActionsKind.GET_PROFILE_BY_ID;
  readonly meta: {
    id: number;
  };
}

export interface IProfileGetProfileByIdSuccessPayload {
  data: User;
}

export interface IProfileGetProfileByIdSuccess extends Action {
  readonly type: ProfileActionsKind.GET_PROFILE_BY_ID_SUCCESS;
  readonly payload: IProfileGetProfileByIdSuccessPayload;
}

export interface IProfileGetProfileByIdFailed extends Action {
  readonly type: ProfileActionsKind.GET_PROFILE_BY_ID_FAILED;
  readonly payload: Error;
}

export interface IProfileUpdateProfile extends Action {
  readonly type: ProfileActionsKind.UPDATE_PROFILE;
  readonly meta: User;
}

export interface IProfileUpdateProfileSuccess extends Action {
  readonly type: ProfileActionsKind.UPDATE_PROFILE_SUCCESS;
}
export interface IProfileUpdateProfileFailed extends Action {
  readonly type: ProfileActionsKind.UPDATE_PROFILE_FAILED;
  readonly payload: Error;
}

export type ProfileAction =
  | IProfileSetLoad
  | IProfileGetProfileById
  | IProfileGetProfileByIdSuccess
  | IProfileGetProfileByIdFailed
  | IProfileUpdateProfile
  | IProfileUpdateProfileSuccess
  | IProfileUpdateProfileFailed;
