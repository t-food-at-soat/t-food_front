import { createStore, applyMiddleware, compose } from "redux";
import rootReducer, {
  defaultRootState,
  IRootState,
  rootEpics
} from "./combiners";
import { createEpicMiddleware } from "redux-observable";
import { createLogger } from "redux-logger";

const epicMiddleware = createEpicMiddleware();

const enhancer = compose(applyMiddleware(epicMiddleware, createLogger()));

export const configureStore = (initialState: IRootState = defaultRootState) => {
  const store = createStore(rootReducer, initialState, enhancer);
  epicMiddleware.run(rootEpics);
  return store;
};

export default configureStore();
