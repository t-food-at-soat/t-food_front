import {uploadPictureEpic} from "./uploadPicture";
import {combineEpics} from "redux-observable";

export const uploadPictureEpics = combineEpics<any>(
  uploadPictureEpic,
);
