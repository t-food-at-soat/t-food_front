import { Epic, ofType } from "redux-observable";
import { concat, of } from "rxjs";
import { IRootState } from "../../combiners";
import { catchError, flatMap, switchMap } from "rxjs/operators";
import { ajax } from "rxjs/ajax";
import { toast } from "react-toastify";
import { serverURL, errorMessages } from "../../config/constant";
import {
  IUploadPictureUploadPicture,
  IUploadPictureUploadPictureFailed,
  IUploadPictureUploadPictureSuccess,
  UploadPictureActionsKind
} from "../types";
import { uploadPictureSuccess, uploadPictureFailed } from "../actions";

export const uploadPictureEpic: Epic<
  IUploadPictureUploadPicture,
  any,
  IRootState,
  IUploadPictureUploadPictureSuccess | IUploadPictureUploadPictureFailed
> = (action$, store) =>
  action$.pipe(
    ofType(UploadPictureActionsKind.UPLOAD_PROFILE_PICTURE),
    switchMap(action => {
      let fd = new FormData();
      fd.append("file", action.meta.filePath);

      return ajax({
        url: serverURL + "upload",
        body: fd,
        method: "POST",
        responseType: "json"
      }).pipe(
        flatMap(res => {
          toast.success("Upload terminé !");
          return concat(of(uploadPictureSuccess(res.response)));
        }),
        catchError(error => {
          // @ts-ignore
          toast.error(errorMessages[error.status]);
          return of(uploadPictureFailed(error));
        })
      );
    })
  );
