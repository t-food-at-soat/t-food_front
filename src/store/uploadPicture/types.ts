import {Action} from "redux";

export interface IUploadPictureState {
  loadFilePath : string;
  isPictureLoad: boolean;
  errorMessage: string
}

export enum UploadPictureActionsKind {
  UPLOAD_PROFILE_SET_LOAD = "UPLOAD_PROFILE_SET_LOAD",
  UPLOAD_PROFILE_PICTURE = "UPLOAD_PROFILE_PICTURE",
  UPLOAD_PROFILE_PICTURE_SUCCESS = "UPLOAD_PROFILE_PICTURE_SUCCESS",
  UPLOAD_PROFILE_PICTURE_FAILED = "UPLOAD_PROFILE_PICTURE_FAILED",
}

export interface IUploadPictureSetLoad extends Action {
  readonly type: UploadPictureActionsKind.UPLOAD_PROFILE_SET_LOAD;
  readonly payload: boolean;
}

export interface IUploadPictureUploadPicture extends Action {
  readonly type: UploadPictureActionsKind.UPLOAD_PROFILE_PICTURE;
  readonly meta: {
    filePath: File
  }
}

export interface IUploadPictureUploadPictureSuccessPayload {
  data: {
    filePath : string
  };
}

export interface IUploadPictureUploadPictureSuccess extends Action {
  readonly type: UploadPictureActionsKind.UPLOAD_PROFILE_PICTURE_SUCCESS;
  readonly payload: IUploadPictureUploadPictureSuccessPayload
}

export interface IUploadPictureUploadPictureFailed extends Action {
  readonly type: UploadPictureActionsKind.UPLOAD_PROFILE_PICTURE_FAILED;
  readonly payload: Error
}


export type UploadPictureAction =
  | IUploadPictureSetLoad
  | IUploadPictureUploadPicture
  | IUploadPictureUploadPictureSuccess
  | IUploadPictureUploadPictureFailed;