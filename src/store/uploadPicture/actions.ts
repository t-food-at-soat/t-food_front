import {
  IUploadPictureSetLoad,
  IUploadPictureUploadPicture, IUploadPictureUploadPictureFailed,
  IUploadPictureUploadPictureSuccess,
  IUploadPictureUploadPictureSuccessPayload,
  UploadPictureActionsKind
} from "./types";

export const uploadPicture = (filePath: File): IUploadPictureUploadPicture => ({
  type: UploadPictureActionsKind.UPLOAD_PROFILE_PICTURE,
  meta: {
    filePath
  }
});

export const uploadPictureSuccess = (payload : IUploadPictureUploadPictureSuccessPayload): IUploadPictureUploadPictureSuccess => ({
  type: UploadPictureActionsKind.UPLOAD_PROFILE_PICTURE_SUCCESS,
  payload
});

export const uploadPictureFailed = (payload: Error): IUploadPictureUploadPictureFailed => ({
  type: UploadPictureActionsKind.UPLOAD_PROFILE_PICTURE_FAILED,
  payload
});

export const uploadPictureSetLoad = (payload: boolean): IUploadPictureSetLoad => ({
  type: UploadPictureActionsKind.UPLOAD_PROFILE_SET_LOAD,
  payload
});