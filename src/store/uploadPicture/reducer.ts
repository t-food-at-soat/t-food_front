import {IUploadPictureState, UploadPictureAction, UploadPictureActionsKind} from "./types";


export const defaultUploadPictureState: IUploadPictureState = {
    loadFilePath: "",
    isPictureLoad: false,
    errorMessage: ""
  }
;

const uploadPictureReducer = (
  state: IUploadPictureState = defaultUploadPictureState,
  action: UploadPictureAction
): IUploadPictureState => {
  switch (action.type) {
    case UploadPictureActionsKind.UPLOAD_PROFILE_PICTURE_SUCCESS:
      return {
        ...state,
        loadFilePath: action.payload.data.filePath,
        isPictureLoad : true
      };
    case UploadPictureActionsKind.UPLOAD_PROFILE_PICTURE_FAILED:
      return {
        ...state,
        errorMessage: action.payload.message
      };
    case UploadPictureActionsKind.UPLOAD_PROFILE_SET_LOAD:
      return {
        ...state,
        isPictureLoad: action.payload
      };
    default:
      return state;
  }
};

export default uploadPictureReducer;
