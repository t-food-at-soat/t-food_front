import { configure } from '@storybook/react';
import { addParameters } from '@storybook/react';
import { themes } from '@storybook/theming';
import { INITIAL_VIEWPORTS } from '@storybook/addon-viewport'

// Option defaults.
addParameters({
  options: {
    theme: themes.dark,
  },
  viewport: {
    viewports: INITIAL_VIEWPORTS
  },
});

// automatically import all files ending in *.stories.js
configure(require.context('../src/', true, /\.stories\.js$/), module);
