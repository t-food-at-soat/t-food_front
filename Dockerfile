FROM node:12-alpine

WORKDIR /var/www

COPY . .

RUN npm install

RUN npm run build

RUN npm install -g serve

EXPOSE 5000

CMD serve -s build
